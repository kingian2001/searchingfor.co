﻿using Searchingfor.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities;
using Utilities.Web;

namespace Searchingfor.Core.ViewModels
{
    public class PostListVM
    {
        public List<CategoryVM> Categories { get; set; }
        public List<CountryVM> Countries { get; set; }

        public PostListVM()
        {
        }

        public PostListVM Get()
        {
            this.Categories = WebCacheHelper.CacheEveryExpirey<List<CategoryVM>>("Categories", () =>
            {
                return (new CategoryVM().GetCategoryTree());
            }, 100);

            this.Countries = WebCacheHelper.CacheEveryExpirey<List<CountryVM>>("Countries", () =>
            {
                return (new CountryVM().GetAllActive());
            }, 100);

            return this;
        }

        public PostListVM GetCountries()
        {
            this.Countries = (List<CountryVM>)WebCacheHelper.CacheEveryExpirey("Countries", () =>
            {
                return (object)(new CountryVM().GetAllActive());
            }, 100);

            return this;
        }

        public PostListVM GetCategories()
        {
            this.Categories = WebCacheHelper.CacheEveryExpirey<List<CategoryVM>>("Categories", () =>
            {
                return (new CategoryVM().GetCategoryTree());
            }, 100);

            return this;
        }
    }
}
