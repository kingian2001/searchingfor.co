﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Searchingfor.Core.ViewModels
{
    public class SMSSenderVM
    {
        public string message_type { get; set; }
        public string mobile_number { get; set; }
        public string shortcode { get; set; }
        public string message_id { get; set; }
        public string message { get; set; }
        public string client_id { get; set; }
        public string secret_key { get; set; }
    }
}
