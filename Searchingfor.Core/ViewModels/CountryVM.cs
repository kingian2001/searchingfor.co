﻿using Searchingfor.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Searchingfor.Core.ViewModels
{
    public class CountryVM : ALookupChainVM<CountryVM>
    {
        public CountryVM()
            : base("Country")
        {
        }
    }
}
