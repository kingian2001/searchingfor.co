﻿using Searchingfor.Core.Models;
using Searchingfor.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Searchingfor.Core.ViewModels
{
    public class KeywordsOfUserVM
    {
        public UserVM User { get; set; }
        public List<KeywordVM> Keywords { get; set; }

        public KeywordsOfUserVM()
        {
        }

        public bool AssignKeywords(string[] keywords, Guid userID)
        {
            if (userID.IsNull())
                throw new CustomException("User identifier is required");

            if (keywords != null && keywords.Any())
                Keywords = keywords.Select(x => new KeywordVM { Word = x }).ToList();
            else
                Keywords = new List<KeywordVM>();

            User = new UserVM { ID = userID };

            bool result = new KeywordUserVM().ProcessToCreate(this);

            return result;
        }

        public List<string> GetKeywords(Guid userID)
        {
            return new KeywordUserVM().GetKeywords(userID);
        }

        public List<KeywordsOfUserVM> GetKeywordUsers(string text)
        {
            return new KeywordUserVM().GetKeywordsUsingText(text);
        }
    }
}
