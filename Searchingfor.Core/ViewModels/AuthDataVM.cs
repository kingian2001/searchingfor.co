﻿using Searchingfor.Core.Models;
using Searchingfor.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Searchingfor.Core.ViewModels
{
    public class AuthDataVM
    {
        public string access_token { get; set; }

        public string Value { get; set; }
    }
}
