﻿using Searchingfor.Common;
using Searchingfor.Core.Models;
using Searchingfor.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Searchingfor.Core.ViewModels
{
    public class ProfileScreenVM
    {
        public UserVM Profile { get; set; }
        public int ActiveAdsCount { get; set; }
        public double AverageRating { get; set; }

        public ProfileScreenVM()
        {
        }

        public ProfileScreenVM Get()
        {
            Instance instance = new Instance();

            instance.SetSession();

            try
            {
                Profile = new UserVM(instance).GetProfile(Profile.ID);

                if (Profile == null)
                    throw new NotFoundException();

                var active = Constants.PostStatus.ACTIVE;

                ActiveAdsCount = new PostVM(instance).GetCount(x => x.Owner.ID == Profile.ID && x.Status == active);

                AverageRating = new ReviewVM(instance).GetAverageRating(x => x.Reviewee.ID == Profile.ID);

                return this;
            }
            finally { instance.Dispose(); }
        }

        public ProfileScreenVM GetViaEmail()
        {
            Instance instance = new Instance();
            try
            {
                Profile = new UserVM(instance).GetProfile(Profile.Email);

                if (Profile == null)
                    throw new NotFoundException();

                var active = Constants.PostStatus.ACTIVE;

                ActiveAdsCount = new PostVM(instance).GetCount(x => x.Owner.ID == Profile.ID && x.Status == active);

                AverageRating = new ReviewVM(instance).GetAverageRating(x => x.Reviewee.ID == Profile.ID);

                return this;
            }
            finally { instance.Dispose(); }
        }

    }
}
