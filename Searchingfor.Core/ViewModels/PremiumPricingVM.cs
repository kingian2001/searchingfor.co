﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Searchingfor.Core.ViewModels
{
    public class PremiumPricingVM
    {
        /// <summary>
        /// type of the premium
        /// </summary>
        public string Type { get; set; }
        public string Label { get; set; }
        public int DurationInDays { get; set; }
        public int CreditCost { get; set; }

        public static List<PremiumPricingVM> pricing = new List<PremiumPricingVM>
        {
              new PremiumPricingVM{
                   Type="Keyword Slot",
                    Label = "15 Days",
                    DurationInDays=15,
                    CreditCost=100
               },
               new PremiumPricingVM{
                   Type="Keyword Slot",
                    Label = "30 Days",
                    DurationInDays=30,
                    CreditCost=200
               },
               new PremiumPricingVM{
                   Type="Keyword Slot",
                    Label = "45 Days",
                    DurationInDays=45,
                    CreditCost=300
               },
               new PremiumPricingVM{
                   Type="Keyword Slot",
                    Label = "2 Months",
                    DurationInDays=60,
                    CreditCost=400
               },
               new PremiumPricingVM{
                   Type="Keyword Slot",
                    Label = "3 Months",
                    DurationInDays=90,
                    CreditCost=500
               },
               new PremiumPricingVM{
                   Type="Keyword Slot",
                    Label = "6 Months",
                    DurationInDays=180,
                    CreditCost=700
               },
                 new PremiumPricingVM{
                   Type="Feature Comment",
                    Label = "15 Days",
                    DurationInDays=15,
                    CreditCost=200
               },
               new PremiumPricingVM{
                   Type="Feature Comment",
                    Label = "30 Days",
                    DurationInDays=30,
                    CreditCost=400
               },
               new PremiumPricingVM{
                   Type="Feature Comment",
                    Label = "45 Days",
                    DurationInDays=45,
                    CreditCost=500
               },
               new PremiumPricingVM{
                   Type="Feature Comment",
                    Label = "2 Months",
                    DurationInDays=60,
                    CreditCost=600
               },
               new PremiumPricingVM{
                   Type="Feature Comment",
                    Label = "3 Months",
                    DurationInDays=90,
                    CreditCost=700
               },
               new PremiumPricingVM{
                   Type="Feature Comment",
                    Label = "6 Months",
                    DurationInDays=180,
                    CreditCost=1000
               },
               new PremiumPricingVM{
                   Type="Feature Ad",
                    Label = "15 Days",
                    DurationInDays=15,
                    CreditCost=200
               },
               new PremiumPricingVM{
                   Type="Feature Ad",
                    Label = "30 Days",
                    DurationInDays=30,
                    CreditCost=400
               },
               new PremiumPricingVM{
                   Type="Feature Ad",
                    Label = "45 Days",
                    DurationInDays=45,
                    CreditCost=500
               },
               new PremiumPricingVM{
                   Type="Feature Ad",
                    Label = "2 Months",
                    DurationInDays=60,
                    CreditCost=600
               },
               new PremiumPricingVM{
                   Type="Feature Ad",
                    Label = "3 Months",
                    DurationInDays=90,
                    CreditCost=700
               },
               new PremiumPricingVM{
                   Type="Feature Ad",
                    Label = "6 Months",
                    DurationInDays=180,
                    CreditCost=1000
               },
        };
        /// <summary>
        /// retrieves list of keyword pricings
        /// </summary>
        public List<PremiumPricingVM> GetKeywordSlotPricingList()
        {
            return pricing.Where(x => x.Type == "Keyword Slot").ToList();
        }

        /// <summary>
        /// retrieves list of comment pricings
        /// </summary>
        public List<PremiumPricingVM> GetFeatureCommentPricingList()
        {
            return pricing.Where(x => x.Type == "Feature Comment").ToList();
        }

        /// <summary>
        /// retrieves list of comment pricings
        /// </summary>
        public List<PremiumPricingVM> GetFeaturePostAdPricingList()
        {
            return pricing.Where(x => x.Type == "Feature Ad").ToList();
        }
    }
}
