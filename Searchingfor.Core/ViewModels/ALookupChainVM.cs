﻿using Searchingfor.Core.Models;
using Searchingfor.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Searchingfor.Core.ViewModels
{
    /// <summary>
    /// Use this only when you have a lookup record that has a record underneath.
    /// example: 
    /// Category lookup has subrecords with names under SubCategories per Category. 
    /// The "Group Name" is based on the "Name" of an existing lookup.
    /// </summary>
    /// <typeparam name="VM"></typeparam>
    public abstract class ALookupChainVM<VM> where VM : ALookupChainVM<VM>, new()
    {
        #region properties
        public Guid ID { get; set; }
        public string Name { get; set; }
        public VM Sub { get; set; }
        public int Sort { get; set; }
        #endregion

        public List<VM> Subs { get; set; }

        private string Type { get; set; }

        public int Count { get; set; }

        public ALookupChainVM(string Type)
        {
            this.Type = Type;
        }
        /// <summary>
        /// retrieves all active categories with subcategories
        /// </summary>
        /// <returns></returns>
        public List<VM> GetAllActive()
        {
            Instance instance = new Instance();
            try
            {
                LookupVM lookupVM = new LookupVM(instance);
                //retrieves all categories
                List<VM> vms = ConvertLookup(lookupVM.GetAll(Type));

                vms.ForEach(x =>
                {
                    x.Subs = ConvertLookup(lookupVM.GetAll(x.Name));
                    x.Subs.ForEach(y =>
                    {
                        y.Subs = ConvertLookup(lookupVM.GetAll(y.Name));
                    });
                });

                return vms;
            }
            finally { instance.Dispose(); }
        }

        /// <summary>
        /// convert lookup into categoryVM
        /// </summary>
        /// <param name="vms"></param>
        /// <returns></returns>
        private List<VM> ConvertLookup(List<LookupVM> vms)
        {
            List<VM> resultVMs = new List<VM>();

            vms.ForEach(lookup =>
            {
                var temp = new VM
                {
                    ID = lookup.ID,
                    Name = lookup.Name,
                    Sort = lookup.Sort
                };
                resultVMs.Add(temp);
            });

            return resultVMs;
        }



    }
}
