﻿using Searchingfor.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Searchingfor.Core.ViewModels
{
    public class CategoryVM : ALookupChainVM<CategoryVM>
    {
        public CategoryVM()
            : base("Category")
        {
        }

        public List<CategoryVM> GetCategoriesMetaData()
        {
            var postvm = new PostVM();
            var lookup = new LookupVM();
            List<CategoryVM> categoryVM = ConvertLookup(lookup.GetAllWithIDAndNamePropertiesOnly("Category"));

            categoryVM.ForEach(category =>
            {
                //category.Count = postvm.GetPostCountByCategory(category.ID);
                category.Subs = ConvertLookup(lookup.GetAllWithIDAndNamePropertiesOnly(category.Name));
                category.Name = "";
                category.Subs.ForEach(sub =>
                {
                    sub.Count = postvm.GetPostCountBySubCategory(sub.ID);
                    sub.Subs = ConvertLookup(lookup.GetAllWithIDAndNamePropertiesOnly(sub.Name));
                    sub.Name = "";
                    sub.Subs.ForEach(specific =>
                    {
                        specific.Count = postvm.GetPostCountBySpecificCategory(specific.ID);
                        specific.Name = "";
                    });
                });
            });

            return categoryVM;
        }

        public List<CategoryVM> GetSiblingCategoryMetaData(int type, string groupName)
        {
            var postvm = new PostVM();
            var lookup = new LookupVM();
            List<CategoryVM> CategoryVM = ConvertLookup(lookup.GetAllWithIDAndNamePropertiesOnly(groupName));

            CategoryVM.ForEach(category =>
            {
                switch (type)
                {
                    case 1:
                        category.Count = postvm.GetPostCountByCategory(category.ID);
                        break;
                    case 2:
                        category.Count = postvm.GetPostCountBySubCategory(category.ID);
                        break;
                    case 3:
                        category.Count = postvm.GetPostCountBySpecificCategory(category.ID);
                        break;
                    default:
                        break;
                }

                category.Name = "";
            });

            return CategoryVM;
        }

        /// <summary>
        /// convert lookup into categoryVM
        /// </summary>
        /// <param name="vms"></param>
        /// <returns></returns>
        private List<CategoryVM> ConvertLookup(List<LookupVM> vms)
        {
            List<CategoryVM> resultVMs = new List<CategoryVM>();

            vms.ForEach(lookup =>
            {
                var temp = new CategoryVM
                {
                    ID = lookup.ID,
                    Name = lookup.Name,
                    Sort = lookup.Sort
                };
                resultVMs.Add(temp);
            });

            return resultVMs;
        }
    }
}
