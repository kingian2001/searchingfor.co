﻿using Searchingfor.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

public class ViewModelTransformer
{
    public static TTarget Transform<TFrom, TTarget>(TFrom fromObject) where TTarget : class, new()
    {
        if (fromObject == null)
        {
            return default(TTarget);
        }

        var tTarget = new TTarget();//instanciate the target

        CopyMatchingProperties<TFrom, TTarget>(fromObject, tTarget);//copy and match fromobject to the target

        return tTarget;
    }

    public static void CopyValues<TFrom, TTarget>(TFrom fromObject, TTarget tTarget)
    {
        if (fromObject == null)
        {
            return;
        }

        CopyMatchingProperties<TFrom, TTarget>(fromObject, tTarget);
    }

    static void CopyMatchingProperties<TFrom, TTarget>(TFrom fromObject, TTarget tTarget)
    {
        foreach (var fromProperty in fromObject.GetType().GetProperties())
        {
            foreach (var targetProperty in tTarget.GetType().GetProperties())
            {
                if (targetProperty.Name == fromProperty.Name)
                {
                    if (targetProperty.PropertyType.FullName != fromProperty.PropertyType.FullName)
                    {
                        continue;
                    }

                    if (fromProperty.PropertyType.Namespace == "Searchingfor.Entity.Entities")
                    {
                        continue;
                    }

                    var fromValue = fromProperty.GetValue(fromObject, null);

                    var shouldIgnore =
                        fromObject.GetType().GetCustomAttributes(typeof(IgnoreTransformAttribute), false).Length > 0;

                    if (shouldIgnore)
                    {
                        targetProperty.SetValue(tTarget, fromValue, null);
                    }
                    else
                    {
                        targetProperty.TrySetValue(tTarget, fromValue);
                    }

                    break;
                }
            }
        }
    }
}
