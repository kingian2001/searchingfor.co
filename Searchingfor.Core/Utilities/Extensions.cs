﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Searchingfor.Core.Utilities
{
    public static class Extensions
    {
        public static Boolean IsNull(this DateTime? date)
        {
            if (date == null || date == new DateTime())
                return true;
            return false;
        }

        public static Boolean IsUndefined(this string value)
        {
            return
                value == null ||
                String.IsNullOrWhiteSpace(value) ||
                value == "undefined";
        }

        public static Boolean CanBeMatched(this IEnumerable<Attribute> attrs)
        {
            foreach (var attr in attrs)
            {
                //has no DontMatch Attribute
                if (attr is Searchingfor.Entity.DontMatch)
                { return false; }
            }

            return true;
        }

       
    }

}