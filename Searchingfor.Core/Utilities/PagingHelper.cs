﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Searchingfor.Core.Utilities
{
    /// <summary>
    /// identifier to know if the property is a search filter.
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    public class SearchFilterData : System.Attribute
    {
    }

    public class PageingHelper<TObject> where TObject : class,new()
    {
        //pageing details.
        public int Page { get; set; }
        public int PageSize { get; set; }
        public int Total { get; set; }

        /// <summary>
        /// Turns an object to dictionary to pass as parameter
        /// </summary>
        /// <param name="temp">object that contains search filters</param>
        /// <returns></returns>
        public static Dictionary<string, string> FiltersToDictionary(TObject temp)
        {
            var dic = new Dictionary<string, string>();

            var properties = temp.GetType().GetProperties(System.Reflection.BindingFlags.Public
                | System.Reflection.BindingFlags.Instance
                | System.Reflection.BindingFlags.DeclaredOnly);

            foreach (var prop in properties)
            {
                if (Attribute.IsDefined(prop, typeof(SearchFilterData)))
                {
                    var value = prop.GetValue(temp);

                    if (value != null)
                    {
                        if (!string.IsNullOrEmpty(value.ToString()))
                        {
                            if (value is bool && (bool)value == false)
                            {
                                continue;
                            }

                            dic.Add(prop.Name, value.ToString());
                        }
                    }
                }
            }

            return dic;
        }
    }
}
