﻿using Searchingfor.DataAccess;
using Searchingfor.DataAccess.Repositories;
using Searchingfor.Entity.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Searchingfor.Core.Models
{
    public class KeywordVM : ViewModelBase<KeywordRepository, Keyword>
    {
        #region entity properties
        public Guid ID { get; set; }
        [Required]
        public string Word { get; set; }
        #endregion

        #region helper

        #endregion

        internal Guid CreateOrRetrieve(bool save = true)
        {
            SetSession();
            try
            {
                Validate();

                Keyword keyword = DefaultRepository.GetSingleOrEmpty(x => x.Word == Word.Trim().ToLower());

                if (!keyword.ID.IsNull())
                    return keyword.ID;

                Entity = Transform(this);
                Entity.ID = Guid.NewGuid();
                Entity.Word = Entity.Word.ToLower();
                DefaultRepository.Add(Entity, Entity.ID);

                if (save)
                    SaveSession(true);

                return Entity.ID;
            }
            finally { if (save)Dispose(); }
        }
    }
}
