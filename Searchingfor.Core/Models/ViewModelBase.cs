﻿using Searchingfor.DataAccess;
using Searchingfor.DataAccess.Repositories;
using Searchingfor.Entity;
using Searchingfor.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Searchingfor.Core.Utilities;
using Newtonsoft.Json;
using Utilities.Container;
using System.Web.OData.Query;
using System.Diagnostics;
using Searchingfor.Interface.DA;
using Searchingfor.Common;

namespace Searchingfor.Core.Models
{
    public abstract class ViewModelBase
    {
        //public LoginViewModel Login { get; set; }
    }

    public abstract class ViewModelBase<TRepository, TEntity> : ViewModelBase
        where TEntity : class, new()
        where TRepository : class, IRepository<TEntity>, new()
    {
        protected Instance instance { get; set; }

        protected TEntity Entity;

        private TRepository defaultRepository;


        protected TRepository DefaultRepository
        {
            get
            {
                if (defaultRepository == null)
                    defaultRepository = instance.RepositoryFactory<TRepository>();

                return defaultRepository;
            }
            set { }
        }

        /// <summary>
        /// Collection of Repositories that contains repos with the current nhibernate session
        /// </summary>
        protected List<Repository> Repositories { get; set; }

        public ViewModelBase()
        {
            instance = new Instance();
        }

        public ViewModelBase(bool Distribute = false)
        {
            //retrieves a repository with an instnace
            if (instance == null)
                instance = new Instance();

            SetSession(Distribute);
        }

        public ViewModelBase(List<Repository> repo)
        {
            Repositories = repo;
        }
        
        /// <summary>
        /// Sets the session
        /// </summary>
        /// <param name="distribute">to distribute session to all viewmodels under current model</param>
        /// <param name="_instance">Uses this instance instead of creating new one</param>
        /// <param name="saveAuto">set the saveing to auto instead of on commit</param>
        public void SetSession(bool distribute = false,
            Instance _instance = null,
            bool saveAuto = false,
           bool hasTransaction = false)
        {
            if (_instance != null)
                instance = _instance;

            instance.SetSession(saveAuto, hasTransaction: hasTransaction);

            if (distribute)
                DistributeToViewModels(this);
        }

        //public SearchResult<TEntity> Search(SearchRequest<TEntity> request)
        //{
        //   return  GetRepository<TRepository>().SearchFilter(request);
        //}

        /// <summary>
        /// Distribute Repositories to all viewmodels under parent viewmodel
        /// </summary>
        /// <param name="target"></param>
        void DistributeToViewModels(object target)
        {
            //loops through all properties of the object
            foreach (var propertyInfo in target.GetType()
                    .GetProperties(
                        BindingFlags.Public |
                        BindingFlags.Instance |
                        BindingFlags.FlattenHierarchy |
                        BindingFlags.GetProperty |
                        BindingFlags.NonPublic))
            {
                //The IsAssignableFrom method is used to check whether a Type is compatible with a given type.
                if (typeof(ViewModelBase).IsAssignableFrom(propertyInfo.PropertyType))
                {//anything of type viewmodelBase will go here.
                    var child = propertyInfo.GetValue(target, null);//get value of that type

                    if (child != null)//if viewmodel is not empty
                    {
                        DistributeToViewModels(child);//recursively setContext 
                    }
                }
                else if (propertyInfo.PropertyType == typeof(List<Repository>))
                {
                    propertyInfo.SetValue(target, Repositories, null);
                }
                else if (propertyInfo.PropertyType == typeof(Instance))
                {
                    propertyInfo.SetValue(target, instance, null);
                }
            }
        }
        
        /// <summary>
        /// Saves the Transactions - also Inserts to Audit Trail
        /// </summary>
        /// <param name="dispose"></param>
        protected void SaveSession(bool dispose = false)
        {
            //InsertAuditTrail();

            instance.SaveSession();

            if (dispose)
                Dispose();
        }

        public void Audit(Guid userId, string message, string UrlOfAction, Constants.AuditLogType type)
        {
            var auditLog = instance != null ? new AuditLogVM(instance) : new AuditLogVM();
            if (!userId.IsNull())
                auditLog.User = new UserVM { ID = userId };
            auditLog.Type = (int)type;
            auditLog.Message = message;
            auditLog.Others = UrlOfAction;
            auditLog.Create(false);
        }

        /// <summary>
        /// creates a record in the database regarding the notification
        /// </summary>
        /// <param name="Type"></param>
        /// <param name="userId"></param>
        /// <param name="Title"></param>
        /// <param name="Description"></param>
        /// <param name="Link"></param>
        /// <param name="action"></param>
        public void Notify(string Type, Guid userId, string Title, string Description, string Link, Action<string, string> action)
        {
            var notification = instance != null ? new NotificationVM(instance) : new NotificationVM();
            notification.Compose(Type, userId, Title, Description, Link, action);
        }

        /// <summary>
        /// Retrieves a Repository the user choose to use.
        /// </summary>
        /// <typeparam name="TRepo"></typeparam>
        /// <returns></returns>
        protected TRepo GetRepository<TRepo>() where TRepo : class, new()
        {
            //return (Repositories.First(x => x.Name == typeof(TRepo).Name).Repo as TRepo);
            return instance.RepositoryFactory<TRepo>();
        }

        /// <summary>
        /// Create a new Entity then set ID
        /// </summary>
        protected void CreateEntity()
        {
            Entity = new TEntity();

            SetEntityID();
        }

        /// <summary>
        /// Sets ID to the Entity
        /// </summary>
        void SetEntityID()
        {
            Entity.GetType().GetProperty("ID").SetValue(Entity, Guid.NewGuid(), null);
        }

        /// <summary>
        /// transforms a viewmodel to the equivalent entity
        /// </summary>
        /// <typeparam name="TViewModel"></typeparam>
        /// <param name="viewModel"></param>
        /// <returns></returns>
        protected TEntity Transform<TViewModel>(TViewModel viewModel)
        {
            return ViewModelTransformer.Transform<TViewModel, TEntity>(viewModel);
        }

        /// <summary>
        /// Copies by value the whole object
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        protected TEntity DeepCopy(TEntity entity)
        {
            var temp = JsonConvert.SerializeObject(entity, new JsonSerializerSettings()
            {
            });

            var ent = JsonConvert.DeserializeObject<TEntity>(temp);

            return ent;
        }

        /// <summary>
        /// Turns the ViewModel Into Entity.
        /// </summary>
        /// <returns></returns>
        public TEntity ToEntity()
        {
            var vm = this;
            var Entity = new TEntity();

            //loops through all properties of the object
            foreach (var propertyInfo in vm.GetType()
                    .GetProperties(
                        BindingFlags.Public |
                        BindingFlags.Instance |
                        BindingFlags.GetProperty))
            {
                var property = Entity.GetType().GetProperties().FirstOrDefault(x => x.Name == propertyInfo.Name);
                try
                {
                    if (property != null)
                    {
                        var val = propertyInfo.GetValue(vm, null);//get value of that type
                        property.SetValue(Entity, val, null);
                    }
                }
                catch
                { }
            }

            return Entity;
        }

        /// <summary>
        /// validates the model
        /// </summary>
        protected void Validate(bool ignoreId = true)
        {
            var message = string.Join("<br>", DataAnnotationUtility.CheckErrors(this, ignoreId).Select(x => x.ErrorMessage));
            if (!string.IsNullOrEmpty(message))
                throw new CustomException(message);
        }

        /// <summary>
        /// Disposes Resources
        /// </summary>
        public void Dispose()
        {
            if (instance != null)
                instance.Dispose();
        }

        ~ViewModelBase()
        {
            Dispose(); Debug.WriteLine("ViewModelBase Deconstructor Executed");
        }

    }
}
