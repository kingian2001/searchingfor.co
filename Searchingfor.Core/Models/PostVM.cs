﻿using Searchingfor.Core.ViewModels;
using Searchingfor.DataAccess.Repositories;
using Searchingfor.Entity.Entities;
using Searchingfor.Core.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.OData.Query;
using Utilities.Container;
using System.Linq.Expressions;
using AmazonS3;
using Searchingfor.Common;
using Newtonsoft.Json;
using Searchingfor.DataAccess;

namespace Searchingfor.Core.Models
{
    public class PostVM : ViewModelBase<PostRepository, Post>
    {
        #region entity properties
        [Required]
        public Guid ID { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Description { get; set; }
        public string OtherDescription { get; set; }
        public string LinkPostfix { get; set; }
        public bool Selling { get; set; }
        [Required]
        public Urgency Urgency { get; set; }
        [Required]
        public UserVM Owner { get; set; }
        [Required]
        public CategoryVM Category { get; set; }

        public string ImageUrls { get; set; }

        public string AttachmentUrls { get; set; }

        public bool Notify { get; set; }
        public string Status { get; set; }
        public bool Featured { get; set; }
        public DateTime? FeatureExpirationDate { get; set; }

        public DateTime CreatedOn { get; set; }
        public DateTime ModifiedOn { get; set; }

        public DateTime? ExpirationDate { get; set; }

        public string Tags { get; set; }

        public string Country { get; set; }
        public string City { get; set; }
        #endregion

        #region buyable/swappable properties
        /// <summary>
        /// buyable swappable condition
        /// </summary>
        public string Condition { get; set; }

        /// <summary>
        /// buyable swappable Quantity
        /// </summary>
        public int Quantity { get; set; }

        public int BudgetMin { get; set; }
        public int BudgetMax { get; set; }
        public bool SwappableDescription { get; set; }
        public string Types { get; set; }
        #endregion

        #region services
        //public LookupVM ServiceLocation { get; set; }
        #endregion

        #region Carpool
        public string FromLocation { get; set; }
        public string ToLocation { get; set; }
        public DateTime TripTimeStart { get; set; }
        public DateTime TripTimeEnd { get; set; }
        #endregion

        #region Helper properties
        /// <summary>
        /// Primary image for the post
        /// </summary>
        public string PrimaryImage { get; set; }

        /// <summary>
        /// List of Categories
        /// </summary>
        public List<string> CategoryNames { get; set; }

        /// <summary>
        /// BSR: Shows the list of types
        /// </summary>
        public List<string> TypeList { get; set; }

        /// <summary>
        /// List of Image Files
        /// </summary>
        public List<S3FileVM> Images { get; set; }

        /// <summary>
        /// List of Attached Files
        /// </summary>
        public List<S3FileVM> Attachments { get; set; }

        /// <summary>
        /// displays urgency in a readable way
        /// </summary>
        public string DisplayUrgency { get; set; }

        /// <summary>
        /// Image url list from the database
        /// </summary>
        public List<string> ImageUrlList { get; set; }

        /// <summary>
        /// attachment url list from the database
        /// </summary>
        public List<string> AttachmentUrlList { get; set; }

        /// <summary>
        /// the premium associated with this post.
        /// </summary>
        public PremiumVM Premium { get; set; }

        public List<string> TagList { get; set; }

        public string DisplayLocation { get; set; }

        #endregion

        public PostVM()
        {
            //SetSession(true);
        }

        public PostVM(Instance instance)
        {
            SetSession(_instance: instance);
        }

        #region Core logic
        /// <summary>
        ///Creates an advertisment
        /// </summary>
        /// <returns></returns>
        public PostVM Create()
        {
            Validate();

            SetSession(hasTransaction: true);
            try
            {
                //transfer to entity
                Entity = ViewModelTransformer.Transform<PostVM, Post>(this);

                //parse
                Entity.Urgency = (int)Urgency;

                //needs to be buyable swappable rentable
                if (Category.MaterializedPath.Contains("Buyable/Swappable/Rentable"))
                {
                    //if no value
                    if (TypeList == null || TypeList.Count == 0)
                        TypeList = new List<string>() { "Buy" };
                    //throw new CustomException("Type should be of value " + String.Join(", ", Constants.BuySwapRent.ToArray()));

                    //if doesnt match with list
                    //if (TypeList.Where(x => !string.IsNullOrEmpty(x)).Except(Constants.BuySwapRent).Any())
                    //    throw new CustomException("Type should be of value " + String.Join(", ", Constants.BuySwapRent.ToArray()));

                    //assign
                    Entity.Types = String.Join(Constants.Delimiter.ToString(), TypeList.Where(x => !string.IsNullOrEmpty(x)).ToArray());

                    if (this.Quantity <= 0)
                        Entity.Quantity = this.Quantity = 1;

                    if (!Constants.ConditionTypes.Contains(this.Condition))
                        Entity.Condition = Condition = "New";
                    //throw new CustomException("Conditions should be of value " + String.Join(", ", Constants.ConditionTypes.ToArray()));
                }

                if (Category.MaterializedPath.Contains("Job."))
                {
                    //if no value
                    if (TypeList == null || TypeList.Count == 0)
                        TypeList = new List<string>() { "Full-Time" };
                    //throw new CustomException("Type should be of value " + String.Join(", ", Constants.JobTypes.ToArray()));

                    //if doesnt match with list
                    //if (TypeList.Where(x => !string.IsNullOrEmpty(x)).Except(Constants.JobTypes).Any())
                    //    throw new CustomException("Type should be of value " + String.Join(", ", Constants.JobTypes.ToArray()));

                    //assign
                    Entity.Types = TypeList.FirstOrDefault();
                }

                if (Entity.BudgetMin == 0)
                    Entity.BudgetMin = Entity.BudgetMax;

                //validation
                if (Category == null)
                    throw new CustomException("Category is required");

                //apply category
                //if (Category != null)
                Entity.Category = new Category() { ID = Category.ID };

                //apply user.
                if (Owner != null && Owner.NewUser)
                {
                    this.Owner = Owner.Create(false);
                    Entity.Owner = new User() { ID = Owner.ID };//create user
                }
                else
                    Entity.Owner = new User() { ID = Owner.ID };//use existing user.

                Entity.ExpirationDate = GetExpirationDateBasedOnUrgency(Entity.Urgency);

                Entity.Tags = TagListToString(TagList);

                Entity.Status = Constants.PostStatus.ACTIVE;

                Entity.ID = Guid.NewGuid();

                Entity.LinkPostfix = string.IsNullOrEmpty(LinkPostfix) ? Entity.ID.ToString() : LinkPostfix;

                Entity.ModifiedOn = Entity.CreatedOn = DateTime.UtcNow;

                DefaultRepository.Add(Entity, Entity.ID);

                //audit
                Audit(Entity.Owner.ID, "Added a post with title \"" + Entity.Title + "\"", "/Post/" + Entity.ID, Constants.AuditLogType.Post);

                //save images
                Entity.ImageUrls = S3FileVM.SaveAll(Images);

                //save attachments
                Entity.AttachmentUrls = S3FileVM.SaveAll(Attachments);

                //get keywords used..
                var keywordUsers = new KeywordUserVM(instance).GetKeywordsUsingText(this.Title);

                //notify user of use of the keyword
                foreach (var keywordUser in keywordUsers)
                {
                    var keywordUserLocation = LocationToReadableLocation(keywordUser.Country, keywordUser.City);

                    DisplayLocation = LocationToReadableLocation(this.Country, this.City);

                    if (DisplayLocation.StartsWith(keywordUserLocation))
                    {
                        Notify("Keywords", keywordUser.User.ID, "Someone posted with keyword '" + keywordUser.Keyword.Word + "' from " + DisplayLocation,
                            this.Owner.Name + " posted '" + this.Title + "'",
                            "Post/" + Entity.LinkPostfix,
                            BroadCast);
                        SendKeywordEmail(this.Owner.Email, this.Title, this.Owner.Name, keywordUser.Keyword.Word);

                    }
                }
                SaveSession(true);

                return
                   new PostVM
                   {
                       ID = Entity.ID,
                       Owner = this.Owner
                   };
            }
            finally { Dispose(); }
        }

        /// <summary>
        /// save as buyable Post
        /// </summary>
        /// <returns></returns>
        public PostVM Update()
        {
            Validate();

            SetSession(hasTransaction: true);

            try
            {
                //transfer to entity
                Entity = ViewModelTransformer.Transform<PostVM, Post>(this);

                //parse
                Entity.Urgency = (int)Urgency;

                //needs to be buyable swappable rentable
                if (Category.MaterializedPath.Contains("Buyable/Swappable/Rentable"))
                {
                    //if no value
                    if (TypeList == null || TypeList.Count == 0)
                        throw new CustomException("Type should be of value " + String.Join(", ", Constants.BuySwapRent.ToArray()));

                    //if doesnt match with list
                    if (TypeList.Where(x => !string.IsNullOrEmpty(x)).Except(Constants.BuySwapRent).Any())
                        throw new CustomException("Type should be of value " + String.Join(", ", Constants.BuySwapRent.ToArray()));

                    //assign
                    Entity.Types = String.Join(Constants.Delimiter.ToString(), TypeList.Where(x => !string.IsNullOrEmpty(x)).ToArray());
                }

                //apply category
                if (Category != null)
                    Entity.Category = new Category() { ID = Category.ID };

                Entity.Tags = TagListToString(TagList);

                Entity.Owner = new User() { ID = Owner.ID };//use existing user.

                Entity.ModifiedOn = DateTime.UtcNow;

                //audit
                //audit
                Audit(Entity.Owner.ID, "Modified a post with title \"" + Entity.Title + "\"", "/Post/" + Entity.ID, Constants.AuditLogType.PostModify);

                //save images
                Entity.ImageUrls = S3FileVM.SaveAll(Images);

                //save attachments
                Entity.AttachmentUrls = S3FileVM.SaveAll(Attachments);

                DefaultRepository.Edit(Entity);

                SaveSession(true);

                return
                   new PostVM
                   {
                       ID = Entity.ID,
                   };
            }
            finally { Dispose(); }
        }

        /// <summary>
        /// Retrieves a single advertisment
        /// </summary>
        /// <param name="identifier"></param>
        /// <returns></returns>
        public PostVM Get(string identifier)
        {
            SetSession();
            try
            {
                var entity = new Post();

                var guid = Guid.Empty;

                //get entity with ID or the linkprefix
                if (Guid.TryParse(identifier, out guid))
                    entity = DefaultRepository.GetEntity(guid);
                else
                    entity = DefaultRepository.GetSingleOrEmpty(x => x.LinkPostfix == identifier);

                if (entity == null || entity.ID.IsNull())
                    return null;

                var vm = EntityToVM(entity, true);

                //retrieve rating for the owner.
                var ratings = GetRepository<ReviewRepository>().AverageRating(vm.Owner.ID);

                vm.Owner.Ratings = new List<ReviewVM>();

                vm.Owner.Ratings.Add(new ReviewVM
                {
                    Rating = ratings,
                    Reviewee = new UserVM
                    {
                        ID = vm.Owner.ID
                    }
                });

                return vm;
            }
            finally { Dispose(); }
        }

        /// <summary>
        /// Odata query for 
        /// </summary>
        /// <param name="options"></param>
        public SearchResultOld<List<PostVM>> Search(ODataQueryOptions<Post> options, 
            bool includePremiumData = false)
        {
            bool once = false;

            SetSession(hasTransaction:true);

            try
            {
                SearchResultOld<List<PostVM>> resultVM = new SearchResultOld<List<PostVM>>()
                {
                    Result = new List<PostVM>()
                };

                var result = DefaultRepository.ODataQuery(options);

                if (result.Result.Count > 0)
                {
                    foreach (var entity in result.Result)
                    {
                        //if feature expired. set featured to false.
                        if (UpdatePostBeforeReturning(entity))
                        {
                            once = true;
                        }

                        var vm = EntityToVM(entity, withTxtUrgency: false, withOwner: true);
                        vm.Description = "";
                        vm.OtherDescription = "";
                        resultVM.Result.Add(vm);
                    };
                }
                else
                    resultVM.Result = new List<PostVM>();

                resultVM.Count = result.Count;

                if (includePremiumData) IncludePremium(resultVM.Result);

                if (once)
                    SaveSession();

                return resultVM;
            }
            finally
            {
                Dispose();
            }
        }

        public SearchResultOld<List<PostVM>> SearchFilter(SearchRequest<Post> request,
            bool includePremiumData = false)
        {
            bool once = false;

            SetSession(hasTransaction: true);

            try
            {
                SearchResultOld<List<PostVM>> resultVM = new SearchResultOld<List<PostVM>>()
                {
                    Result = new List<PostVM>()
                };

                var result = DefaultRepository.SearchFilter(request);

                if (result.Items.Count() > 0)
                {
                    foreach (var entity in result.Items)
                    {
                        //if feature expired. set featured to false.
                        if (UpdatePostBeforeReturning(entity))
                        {
                            once = true;
                        }

                        var vm = EntityToVM(entity, withTxtUrgency: false, withOwner: true);
                        vm.Description = "";
                        vm.OtherDescription = "";
                        resultVM.Result.Add(vm);
                    };
                }
                else
                    resultVM.Result = new List<PostVM>();

                resultVM.Count = result.Total;

                if (includePremiumData) IncludePremium(resultVM.Result);

                if (once)
                    SaveSession();

                return resultVM;
            }
            finally
            {
                Dispose();
            }
        }

        public List<Post> Find(Expression<Func<Post, bool>> predicate)
        {
            SetSession(hasTransaction: false);

            return DefaultRepository.Find(predicate);
        }
        /// <summary>
        /// updates the post on search
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        private bool UpdatePostBeforeReturning(Post entity)
        {
            bool update = false;
            if ((entity.FeatureExpirationDate != null && entity.FeatureExpirationDate < DateTime.UtcNow))
            {
                entity.Featured = false;
                entity.FeatureExpirationDate = null;
                update = true;
            }

            if (entity.Status != Constants.PostStatus.EXPIRED &&
                entity.ExpirationDate != default(DateTime) &&
                entity.ExpirationDate < DateTime.UtcNow)
            {
                entity.Status = Constants.PostStatus.EXPIRED;
                update = true;
            }

            if (update)
                DefaultRepository.Update(entity);

            return update;
        }

        /// <summary>
        /// Includes premium to the post.
        /// </summary>
        /// <param name="list"></param>
        private void IncludePremium(List<PostVM> list)
        {
            if (list == null || !list.Any())
                return;

            var premiums = GetRepository<PremiumRepository>().GetPremiumsOfPosts(list.Select(x => x.ID).ToList());

            if (premiums == null || !premiums.Any())
                return;

            list.ForEach(x =>
            {
                var prem = premiums.FirstOrDefault(o => o.Post.ID == x.ID);

                if (prem != null)
                    x.Premium = PremiumVM.EntityToVM(prem);
            });
        }

        /// <summary>
        /// retrieves random entries that are featured
        /// </summary>
        /// <param name="Top"></param>
        /// <returns></returns>
        public SearchResultOld<List<PostVM>> SearchFeatured(int Top = 10, string country = "", string city = "", bool selling = false)
        {
            SetSession();

            try
            {
                SearchResultOld<List<PostVM>> r = new SearchResultOld<List<PostVM>>()
                {
                    Result = new List<PostVM>()
                };

                var result = DefaultRepository.SearchFeatured(Top, country, city,selling);

                bool once = false;

                List<PostVM> resultVM = new List<PostVM>();

                foreach (var entity in result)
                {
                    //if feature expired. set featured to false.
                    if (UpdatePostBeforeReturning(entity))
                    {
                        once = true;
                        break;
                    }

                    var vm = EntityToVM(entity, withTxtUrgency: false, withOwner: true);

                    r.Result.Add(vm);
                };

                if (once) SaveSession();

                return r;
            }
            finally { Dispose(); }
        }

        /// <summary>
        /// transforms an entity to a viewmodel
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="withOwner"></param>
        /// <returns></returns>
        private PostVM EntityToVM(Post entity, bool withAttachments = false, bool withTxtUrgency = true, bool withOwner = true)
        {
            var q = ViewModelTransformer.Transform<Post, PostVM>(entity);

            //parse
            q.Urgency = (Urgency)entity.Urgency;

            if (withTxtUrgency)
                switch (q.Urgency)
                {
                    case Urgency.NotUrgent://99
                        q.DisplayUrgency = "Not Urgent";
                        break;
                    case Urgency.AMonth://3
                        q.DisplayUrgency = "Within a month";
                        break;
                    case Urgency.Week2://2
                        q.DisplayUrgency = "Within 2 weeks";
                        break;
                    case Urgency.Week1://1
                        q.DisplayUrgency = "Within a week";
                        break;
                    default://0
                        q.DisplayUrgency = "As soon as possible";
                        break;
                }

            q.TagList = entity.Tags.IsNullEmptyOrWhitespace() ? new List<string>() : entity.Tags.Split(Constants.Delimiter).ToList();

            q.Category = ViewModelTransformer.Transform<Category, CategoryVM>(entity.Category);

            if (entity.Category != null)
                q.CategoryNames = q.Category.MaterializedPath.Split('.').ToList();

            if (!string.IsNullOrEmpty(q.Types))
                q.TypeList = q.Types.Trim().Split(Constants.Delimiter).ToList();

            if (string.IsNullOrEmpty(q.LinkPostfix))
                q.LinkPostfix = q.ID.ToString();

            q.DisplayLocation = LocationToReadableLocation(q.Country, q.City);

            if (!string.IsNullOrEmpty(entity.ImageUrls))
            {
                q.ImageUrlList = entity.ImageUrls.Split(Constants.Delimiter).ToList();
                var file = entity.ImageUrls.Split(Constants.Delimiter).FirstOrDefault();
                if (file != null)
                {
                    q.PrimaryImage = file;
                }
            }

            //if no attachments to show
            //if (!withAttachments)
            //{
            //    q.AttachmentUrls = "";
            //}
            //else
            //{
            //    if (!string.IsNullOrEmpty(entity.AttachmentUrls))
            //    {
            //        q.AttachmentUrlList = entity.AttachmentUrls.Split(Constants.Delimiter).ToList();
            //    }
            //}

            if (withOwner)
            {
                q.Owner = UserVM.EntityToVM(entity.Owner);
                q.Owner.Country = entity.Owner.Country;
                q.Owner.City = entity.Owner.City;
            }

            return q;
        }

        /// <summary>
        /// produces a list of strings to a delimited string.
        /// </summary>
        /// <param name="TagList"></param>
        /// <returns></returns>
        private string TagListToString(List<string> TagList)
        {
            return TagList != null ? string.Join(Constants.Delimiter.ToString(), TagList.ToArray()) : "";
        }

        /// <summary>
        /// get expiration date based on the urgency provided
        /// </summary>
        /// <param name="urgency"></param>
        /// <returns></returns>
        private DateTime GetExpirationDateBasedOnUrgency(int urgency)
        {
            switch ((Urgency)Urgency)
            {
                case Urgency.NotUrgent://99
                    return DateTime.UtcNow.AddDays(120);
                    break;
                case Urgency.AMonth://3
                    return DateTime.UtcNow.AddDays(90);
                    break;
                case Urgency.Week2://2
                    return DateTime.UtcNow.AddDays(74);
                    break;
                case Urgency.Week1://1
                    return DateTime.UtcNow.AddDays(67);
                    break;
                default://0
                    return DateTime.Now.AddDays(60);
                    break;
            }
        }

        internal int CountEntities()
        {
            return DefaultRepository.Count(null);
        }

        internal int CountEntities(Dictionary<string, string> filters)
        {
            return DefaultRepository.FilterPostsCount(filters);
        }

        #endregion


        #region helpers
        internal int GetPostCountByCategory(Guid categoryID)
        {
            return DefaultRepository.Count(x => x.Category.ID == categoryID);
        }

        #endregion

        internal List<PostVM> GetActive(Guid userID)
        {
            SetSession();
            try
            {
                var entities = DefaultRepository.Find(x => x.Owner.ID == userID);

                List<PostVM> vms = new List<PostVM>();

                entities.ForEach(entity =>
                {
                    var vm = EntityToVM(entity, false);
                    vms.Add(vm);
                });
                return vms;
            }
            finally { Dispose(); }
        }

        internal List<PostVM> GetInactive(Guid guid)
        {
            return new List<PostVM>();
        }

        public bool IsLinkPostfixUnique(string postfix)
        {
            SetSession();

            try
            {
                if (string.IsNullOrEmpty(postfix))
                    return false;

                var result = DefaultRepository.IfExists(x => x.LinkPostfix == postfix.ToLower());

                if (result)
                    return false;
                else
                    return true;
            }
            finally { Dispose(); }
        }

        /// <summary>
        /// Country and city to a readable text.
        /// </summary>
        /// <param name="Country"></param>
        /// <param name="City"></param>
        /// <returns></returns>
        public string LocationToReadableLocation(string Country, string City)
        {
            //string tmp = City.Split('-')[0];

            //return Country.IsNullEmptyOrWhitespace() ?
            //       "anywhere" :
            //       Country + (City.IsNullEmptyOrWhitespace() ? "" : "-" + tmp.Trim());

            return Country.IsNullEmptyOrWhitespace() ?
                   "anywhere" :
                   Country + (City.IsNullEmptyOrWhitespace() ? "" : ", " + City);
        }

        /// <summary>
        /// get count of advertisments
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="activeAd"></param>
        /// <returns></returns>
        internal int GetCount(Expression<Func<Post, bool>> expression)
        {
            return DefaultRepository.Count(expression);
        }

        [JsonIgnore]
        public Action<string, string> BroadCast { get; set; }

        [JsonIgnore]
        public Action<string, string, string, string> SendKeywordEmail { get; set; }


        /// <summary>
        /// set featured property.
        /// </summary>
        /// <param name="p"></param>
        internal void SetFeaturedProperty(bool value, int durationInDays)
        {
            var entity = DefaultRepository.GetEntity(ID);
            entity.Featured = value;
            entity.FeatureExpirationDate = (DateTime?)DateTime.UtcNow.AddDays(durationInDays);
            DefaultRepository.Update(entity);
        }

        public bool Reactivate(Guid postID)
        {
            SetSession();

            try
            {
                var entity = DefaultRepository.GetEntity(postID);

                if (entity == null) return false;

                entity.Status = Constants.PostStatus.ACTIVE;
                entity.ExpirationDate = DateTime.UtcNow.AddDays(45);

                SaveSession();

                return true;
            }
            finally { Dispose(); }
        }
    }
}
