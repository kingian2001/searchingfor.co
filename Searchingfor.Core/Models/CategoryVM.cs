﻿using Searchingfor.DataAccess;
using Searchingfor.DataAccess.Repositories;
using Searchingfor.Entity.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Searchingfor.Core.Models
{
    public class CategoryVM : ViewModelBase<CategoryRepository, Category>
    {
        #region entity properties
        [Required]
        public Guid ID { get; set; }
        [Required]
        public string Name { get; set; }

        public string Others { get; set; }

        public CategoryVM Parent { get; set; }

        //public List<string> CategoryTreeIds { get; set; }

        public string MaterializedPath { get; set; }

        public int Sort { get; set; }

        public string CommandCode { get; set; }

        #endregion

        #region helper
        public List<CategoryVM> Subs = new List<CategoryVM>();
        public int Depth { get; set; }
        #endregion

        public CategoryVM()
        {
            //SetSession(distribute: true);
        }

        public CategoryVM(Instance instance)
        {
            SetSession(distribute: true, _instance: instance);
        }

        public Guid? Create(bool save = true)
        {
            SetSession(hasTransaction: true);
            try
            {
                Entity = Transform(this);

                if (Parent != null)
                    Entity.Parent = ViewModelTransformer.Transform<CategoryVM, Category>(Parent);

                DefaultRepository.Add(Entity, Entity.ID);

                if (save)
                    return Entity.ID;

                return null;
            }
            finally { if (save)Dispose(); }
        }

        public List<CategoryVM> GetCategoryTree()
        {
            SetSession();
            try
            {
                List<CategoryVM> vms = new List<CategoryVM>();

                vms = EntitiesToViewModels(DefaultRepository.GetAll());

                var parents = vms.Where(x => x.Parent == null).ToList();

                parents.ForEach(x =>
                {
                    x.Depth = 1;
                    x.GetChildrenHierarchy(vms);
                });

                return parents;
            }
            finally
            {
                Dispose();
            }
        }

        public List<CategoryVM> GetAll()
        {
            SetSession();
            try { 
            List<CategoryVM> vms = new List<CategoryVM>();

            int start = DateTime.UtcNow.Millisecond;

            vms = EntitiesToViewModels(DefaultRepository.GetAll());
            return vms;
            }finally { Dispose(); }
        }


        private void GetChildrenHierarchy(List<CategoryVM> vms)
        {
            this.Subs = vms.Where(x => x.Parent != null && x.Parent.ID == this.ID).ToList();

            if (Subs != null || Subs.Count > 0)
                this.Subs.ForEach(x =>
                {
                    x.Depth = this.Depth + 1;
                    x.GetChildrenHierarchy(vms);
                });
        }

        public CategoryVM EntityToViewModel(Category category)
        {
            var vm = ViewModelTransformer.Transform<Category, CategoryVM>(category);

            if (category.Parent != null)
                vm.Parent = ViewModelTransformer.Transform<Category, CategoryVM>(category.Parent);

            return vm;
        }

        public List<CategoryVM> EntitiesToViewModels(List<Category> category)
        {
            List<CategoryVM> vms = new List<CategoryVM>();

            category.ForEach(x =>
            {
                vms.Add(EntityToViewModel(x));
            });

            return vms;
        }
    }
}
