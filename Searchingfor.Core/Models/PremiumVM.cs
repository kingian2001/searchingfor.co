﻿using Searchingfor.Common;
using Searchingfor.Core.ViewModels;
using Searchingfor.DataAccess;
using Searchingfor.DataAccess.Repositories;
using Searchingfor.Entity.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.OData.Query;
using Utilities.Container;

namespace Searchingfor.Core.Models
{
    public class PremiumVM : ViewModelBase<PremiumRepository, Premium>
    {
        #region entity properties
        [Required]
        public Guid ID { get; set; }

        [Required]
        public UserVM User { get; set; }

        public KeywordUserVM KeywordUser { get; set; }

        /// <summary>
        /// for Post feature ad
        /// </summary>
        public PostVM Post { get; set; }

        /// <summary>
        /// Feature Comment, Feature Ad, Keyword Slot
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// cost of the premium
        /// </summary>
        public int Cost { get; set; }

        public string Value { get; set; }

        /// <summary>
        /// duration of premium
        /// </summary>
        public int DurationInDays { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime ExpirationDate { get; set; }
        #endregion

        #region Helper properties
        [Required(ErrorMessage = "Pricing/Duration field is required")]
        public PremiumPricingVM SelectedPricing { get; set; }
        public int HowManyBought = 1;
        #endregion

        public PremiumVM()
        {
        }

        public PremiumVM(Instance instance)
        {
            SetSession(_instance: instance);
        }

        public List<PremiumVM> Create()
        {
            SetSession(distribute: true, hasTransaction: true);

            try
            {
                Validate();

                VerifyParameters();

                Entity = Transform(this);

                //if feature ad. only one instance is allowed.
                if (SelectedPricing.Type == "Feature Ad" && (Post != null && !Post.ID.IsNull()))
                {
                    HowManyBought = 1;

                    //check if post alread has this premium
                    if (DefaultRepository.IfExists(x => x.Post.ID == Post.ID))
                        throw new CustomException("Post already has this premium");

                    Post.SetFeaturedProperty(true, SelectedPricing.DurationInDays);
                }

                if (SelectedPricing.Type == "Feature Ad" && (Post == null || Post.ID.IsNull()))
                    throw new CustomException("Post is required on 'Feature Ad'");

                //if feature comment. only one instance is allowed.
                if (SelectedPricing.Type == "Feature Comment")
                {
                    HowManyBought = 1;
                    //if user already has this premium
                    if (DefaultRepository.IfExists(x => x.User.ID == User.ID && x.Type == "Feature Comment"))
                        throw new CustomException("User already has this premium");
                }

                int currentCredits = (int)GetRepository<UserRepository>().GetField(x => x.ID == User.ID, x => x.Credits);

                //check balance.
                if (currentCredits < SelectedPricing.CreditCost * HowManyBought)
                    throw new CustomException("Unnable to purchase premium. you only have " + currentCredits + " credits left. <br>Please purchase credits to continue purchase.");

                List<PremiumVM> createdPremiumsVM = new List<PremiumVM>();

                for (int i = 0; i < HowManyBought; i++)
                {
                    Entity = null;

                    Entity = Transform(this);

                    Entity.ID = Guid.NewGuid();

                    Entity.User = new User { ID = User.ID };

                    if (Post != null)
                        Entity.Post = new Post { ID = Post.ID };

                    Entity.Cost = SelectedPricing.CreditCost;

                    Entity.DurationInDays = SelectedPricing.DurationInDays;

                    Entity.Type = SelectedPricing.Type;

                    Entity.StartDate = DateTime.UtcNow;

                    Entity.ExpirationDate = DateTime.UtcNow.AddDays(SelectedPricing.DurationInDays);

                    //subtract credits to user.
                    User.ModifyCredit(-Math.Abs(SelectedPricing.CreditCost));

                    DefaultRepository.Add(Entity, Entity.ID);

                    Audit(User.ID, "User purchased a " + SelectedPricing.Type + " duration by " + SelectedPricing.DurationInDays + " days", "/PremiumShop", Constants.AuditLogType.PremiumBuy);

                    createdPremiumsVM.Add(EntityToVM(Entity));
                }

                SaveSession(true);

                return createdPremiumsVM;
            }
            finally { Dispose(); }
        }

        public PremiumVM Extend()
        {
            SetSession(distribute: true, hasTransaction: true);
            try
            {
                Validate(false);

                VerifyParameters();

                int currentCredits = (int)GetRepository<UserRepository>().GetField(x => x.ID == User.ID, x => x.Credits);

                //check credits
                if (currentCredits < SelectedPricing.CreditCost)
                    throw new CustomException("Unnable to extend premium. you only have " + currentCredits + " credits left. <br>Please purchase credits to continue purchase.");

                var type = SelectedPricing.Type;


                Entity = DefaultRepository.GetEntity(this.ID);

                if (Entity == null)
                    throw new CustomException("Premium does not exist.");

                //if feature ad. only one instance is allowed.
                if (SelectedPricing.Type == "Feature Ad" && (Post != null && !Post.ID.IsNull()))
                    Post.SetFeaturedProperty(true, SelectedPricing.DurationInDays);

                Entity.Cost += SelectedPricing.CreditCost;

                Entity.DurationInDays += SelectedPricing.DurationInDays;

                Entity.ExpirationDate = Entity.ExpirationDate.AddDays(SelectedPricing.DurationInDays);

                //subtract credits to user.
                User.ModifyCredit(-SelectedPricing.CreditCost);

                DefaultRepository.Update(Entity);

                Audit(User.ID, "User extended a " + SelectedPricing.Type + " duration by " + SelectedPricing.DurationInDays + " days", "/PremiumShop", Constants.AuditLogType.PremiumExtend);

                SaveSession(true);

                return EntityToVM(Entity);
            }
            finally { Dispose(); }
        }

        public bool AssignKeywordUserToPremium()
        {
            SetSession(distribute: true, hasTransaction: true);

            try
            {
                if (this.ID.IsNull())
                    throw new CustomException("Premium ID and KeywordUser (and its ID) is required");

                var entity = DefaultRepository.GetEntity(this.ID);

                if (entity == null)
                    throw new CustomException("Premium is none existing");

                if (entity.Type != "Keyword Slot")
                    throw new CustomException("Premium is not of type 'Keyword Slot'");

                if (entity.KeywordUser != null && KeywordUser != null && entity.KeywordUser.ID == KeywordUser.ID)
                    return true;

                if (KeywordUser != null)
                    if (DefaultRepository.IfExists(x => KeywordUser.ID == x.KeywordUser.ID && x.ID != ID))
                        throw new CustomException("Keyword already assigned to a slot.");

                entity.KeywordUser = this.KeywordUser == null ? null : new KeywordUser { ID = this.KeywordUser.ID };

                DefaultRepository.Update(entity);

                SaveSession(true);

                return true;
            }
            finally { Dispose(); }
        }

        public void VerifyParameters()
        {
            if (!Constants.PremiumTypes.Contains(SelectedPricing.Type))
                throw new CustomException("Selected premium values '" + String.Join(",", Constants.PremiumTypes.ToArray()) + "'");

            var pricingSelection = PremiumPricingVM.pricing.Where(x => x.Type == SelectedPricing.Type).ToList();

            if (!pricingSelection.Any())
                throw new CustomException("Pricing selection is required");

            var selection = pricingSelection.FirstOrDefault(x => x.Label == SelectedPricing.Label);

            if (selection == null)
                throw new CustomException("Pricing selected does not exist");

            if (selection.Type == "Feature Ad" && (Post == null || Post.ID.IsNull()))
                throw new CustomException("Post is required when you choose 'Feature Ad' Premium");

            SelectedPricing = selection;
        }

        /// <summary>
        /// retrieve premiums via id and type. with flag. includeExpired
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="type"></param>
        /// <param name="includeExpired"></param>
        /// <returns></returns>
        public List<PremiumVM> GetPremiums(Guid userId, string type, bool includeExpired)
        {
            SetSession();
            try
            {
                List<Premium> entities = null;

                if (includeExpired)
                    entities = DefaultRepository.Find(x => x.User.ID == userId && x.Type == type);
                else
                    entities = DefaultRepository.Find(x => x.User.ID == userId && x.Type == type && x.ExpirationDate > DateTime.UtcNow);

                List<PremiumVM> premiumVMs = new List<PremiumVM>();

                if (entities.Any())
                {
                    entities.ForEach(x =>
                    {
                        premiumVMs.Add(EntityToVM(x));
                    });
                }

                return premiumVMs;
            }
            finally { Dispose(); }
        }

        public static PremiumVM EntityToVM(Premium premium)
        {
            if (premium == null)
                return null;

            PremiumVM premiumVM = ViewModelTransformer.Transform<Premium, PremiumVM>(premium);

            premiumVM.User = new UserVM { ID = premium.User.ID };

            if (premium.Post != null)
                premiumVM.Post = new PostVM { ID = premium.Post.ID };

            if (premium.KeywordUser != null)
                premiumVM.KeywordUser = KeywordUserVM.EntityToVM(premium.KeywordUser);

            return premiumVM;
        }

        /// <summary>
        /// assign keywordSlots to the user
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="count"></param>
        internal void AssignKeywordSlots(Guid userID, int count, bool save = true)
        {
            string type = "Keyword Slot";
            int durationInDays = 1200;
            for (int i = 0; i < count; i++)
            {
                Entity = null;

                Entity = Transform(this);

                Entity.ID = Guid.NewGuid();

                Entity.User = new User { ID = userID };

                Entity.Cost = 0;

                Entity.DurationInDays = durationInDays;

                Entity.Type = type;

                Entity.StartDate = DateTime.UtcNow;

                Entity.ExpirationDate = DateTime.UtcNow.AddDays(Entity.DurationInDays);

                DefaultRepository.Add(Entity, Entity.ID);
            }

            Audit(userID, "User was given " + count + " FREE " + type + "/s duration of " + durationInDays + " days", "/PremiumShop", Constants.AuditLogType.PremiumBuy);

            Notify("Premiums", userID, "User was given 4 " + type + " for free just by signing up!", "Duration of each is " + Entity.DurationInDays + " days for a total cost of 4200 credits each just for free! Thats how we appreciate you! ", "/PremiumShop/Keyword Watch", null);

            if (save)
                SaveSession();
        }
    }
}
