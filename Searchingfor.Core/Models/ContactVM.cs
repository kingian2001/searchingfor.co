﻿using AmazonS3;
using Searchingfor.Common;
using Searchingfor.DataAccess.Repositories;
using Searchingfor.Entity.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.OData.Query;
using Utilities.Container;

namespace Searchingfor.Core.Models
{
    public class ContactVM : ViewModelBase<ContactRepository, Contact>
    {
        #region entity properties
        public virtual Guid ID { get; set; }

        [Required]
        public virtual string Type { get; set; }

        public virtual UserVM User { get; set; }

        [Required]
        public virtual string Name { get; set; }

        [Required]
        public virtual string Email { get; set; }

        [Required]
        public virtual string Message { get; set; }
        #endregion

        public ContactVM()
        {
            SetSession();
        }

        /// <summary>
        /// Creates a Contact attached to a post
        /// </summary>
        /// <returns></returns>
        public bool Create()
        {
            Validate();

            SetSession(hasTransaction: true);

            try
            {
                Entity = Transform(this);

                if (User != null)
                    Entity.User = new User()
                    {
                        ID = User.ID
                    };

                Entity.ID = Guid.NewGuid();

                Entity.DateCreated = DateTime.UtcNow;

                DefaultRepository.Add(Entity, Entity.ID);

                SaveSession(true);

                return true;
            }
            finally { Dispose(); }
        }

        /// <summary>
        /// Odata query for 
        /// </summary>
        /// <param name="options"></param>
        public SearchResultOld<List<ContactVM>> Search(ODataQueryOptions<Contact> options)
        {
            SetSession();

            try
            {
                SearchResultOld<List<ContactVM>> resultVM = new SearchResultOld<List<ContactVM>>()
                {
                    Result = new List<ContactVM>()
                };

                var result = DefaultRepository.ODataQuery(options);

                if (result.Result.Count > 0)
                {
                    result.Result.ForEach((entity) =>
                    {
                        var vm = EntityToVM(entity);

                        resultVM.Result.Add(vm);
                    });
                }
                else
                    resultVM.Result = new List<ContactVM>();

                resultVM.Count = result.Count;

                return resultVM;
            }
            finally { Dispose(); }
        }

        /// <summary>
        /// turns an entity into a viewmodel
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        private ContactVM EntityToVM(Contact entity)
        {
            var temp = ViewModelTransformer.Transform<Contact, ContactVM>(entity);

            if (entity.User != null)
                temp.User = ViewModelTransformer.Transform<User, UserVM>(entity.User);

            return temp;
        }
    }
}
