﻿using Searchingfor.Common;
using Searchingfor.DataAccess;
using Searchingfor.DataAccess.Repositories;
using Searchingfor.Entity.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Utilities.Container;

namespace Searchingfor.Core.Models
{
    public class ReviewVM : ViewModelBase<ReviewRepository, Review>
    {
        #region entity properties
        [Required]
        public Guid ID { get; set; }
        [Required]
        public UserVM Reviewer { get; set; }
        [Required]
        public UserVM Reviewee { get; set; }
        [Required]
        public string Type { get; set; }
        [Required]
        public double Rating { get; set; }
        [Required, MaxLength(150)]
        public string Description { get; set; }

        public DateTime DateCreated { get; set; }
        #endregion

        #region helper properties
        /// <summary>
        /// rating just for show
        /// </summary>
        public List<object> RatingArray = new List<object>();
        #endregion

        public Action<string, string> Broadcast = null;

        public ReviewVM()
        {
            //SetSession();
        }

        public ReviewVM(Instance _instance)
        {
            SetSession(_instance: _instance);
        }

        public ReviewVM Create()
        {
            SetSession(hasTransaction:true);

            try
            {
                Validate();

                if (!Constants.ReviewType.Contains(Type))
                    throw new CustomException("Type should be of value " + string.Join(",", Constants.ReviewType.ToArray()));

                Entity = Transform(this);

                Entity.ID = Guid.NewGuid();

                Entity.Reviewer = new User { ID = Reviewer.ID };
                Entity.Reviewee = new User { ID = Reviewee.ID };
                Entity.DateCreated = DateTime.UtcNow;

                DefaultRepository.Add(Entity, Entity.ID);

                Audit(Reviewer.ID, "User reviewed " + Reviewee.Name + " with " + Rating + " stars", "/Profile/" + Reviewee.ID + "/Reviews", Constants.AuditLogType.Review);
                Notify("Ratings", Reviewee.ID, Reviewer.Name + " has reviewed you", Reviewer.Name + " gave you " + Rating + " stars", "/Profile/" + Reviewee.ID + "/Reviews", Broadcast);

                SaveSession(true);

                return new ReviewVM { ID = Entity.ID };
            }
            finally { Dispose(); }
        }

        /// <summary>
        /// odata query for review
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        public SearchResultOld<List<ReviewVM>> Search(System.Web.OData.Query.ODataQueryOptions<Review> options)
        {
            SetSession();
            try
            {
                SearchResultOld<List<ReviewVM>> resultVM = new SearchResultOld<List<ReviewVM>>()
                {
                    Result = new List<ReviewVM>()
                };

                var result = DefaultRepository.ODataQuery(options);

                if (result.Result.Count > 0)
                {
                    result.Result.ForEach((entity) =>
                    {
                        resultVM.Result.Add(EntityToVM(entity));
                    });
                }
                else
                    resultVM.Result = new List<ReviewVM>();

                resultVM.Count = result.Count;

                return resultVM;
            }
            finally { Dispose(); }
        }

        public static ReviewVM EntityToVM(Review entity)
        {
            var temp = new ReviewVM();

            temp = ViewModelTransformer.Transform<Review, ReviewVM>(entity);
            temp.Reviewer = UserVM.EntityToVM(entity.Reviewer);
            temp.Reviewee = UserVM.EntityToVM(entity.Reviewee);


            var rating = temp.Rating;
            //from 1 to 5
            for (int i = 1; i <= 5; i++)
            {
                rating = rating - 1;
                if (rating == .5)
                    temp.RatingArray.Add(new { Value = .5 });
                else if (rating <= 0)
                    temp.RatingArray.Add(new { Value = 0 });
                else if (rating > 0)
                    temp.RatingArray.Add(new { Value = 1 });
            }

            return temp;
        }

        /// <summary>
        /// Get average rating
        /// </summary>
        /// <returns></returns>
        internal double GetAverageRating(Expression<Func<Review, bool>> predicate)
        {    
                List<Review> entities = DefaultRepository.Find(predicate);
                if (entities.Count == 0 || entities == null)
                    return 0;
                return entities.Select(x => x.Rating).Average();
        }
    }
}
