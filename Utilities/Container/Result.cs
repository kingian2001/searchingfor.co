﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities.Container
{
    /// <summary>
    /// standardized Result model
    /// </summary>
    public class Result
    {
        public string Message { get; set; }
        public string ResultType { get; set; }
        public bool Successful { get; set; }
    }

    /// <summary>
    /// standardized result model with custom model
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Result<T> : Result
    {
        public T Model;

        public Result()
        { }
        public Result(T model)
        {
            this.Model = model;
            Successful = true;
            Message = "Successful";
        }
    }

    /// <summary>
    /// standardized result model for list of returns
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class SearchResult<T> : Result
    {
        public IQueryable<T> Items { get; set; }
        public int Total { get; set; }

        public SearchResult()
        { }

        public SearchResult(IQueryable<T> items)
        {
            this.Items = items;
            Total = this.Items.Count();
            Successful = true;
            Message = "Successful";
        }

        public SearchResult(List<T> items)
        {
            this.Items = items.AsQueryable();
            Total = this.Items.Count();
            Successful = true;
        }
    }
}
