﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Utilities.Container;

namespace Utilities.Container
{
    /// <summary>
    /// standardized way of passing queries from odata to other logical layers.
    /// </summary>
    /// <typeparam name="Model"></typeparam>
    public class SearchRequest<Model> where Model : class, new()
    {
        public int PageSize { get; set; }
        public int PageIndex { get; set; }
        public Expression<Func<Model, bool>> Filter { get; set; }
        //public SortExpression<T>[] Sort { get; set; }
        public List<SortExpression<Model>> Sort { get; set; }

        public string FilterQueryRaw = "";
        public string OrderQueryRaw = "";
        //public SortQuert
    }

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class SortExpression<T>
    {
        public Expression<Func<T, object>> Sort { get; set; }
        public bool Ascending { get; set; }

        public SortExpression(Expression<Func<T, object>> sort, bool ascending)
        {
            this.Sort = sort;
            this.Ascending = ascending;
        }
    }

}
