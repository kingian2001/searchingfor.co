﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities
{
    public static class ObjectUtil
    {
        public static string TryParseToString(object obj)
        {
            if (obj is DateTime?)
            {
                return ((DateTime)obj).ToString(@"MM/dd/yyyy");
            }

            return obj.ToString();
        }

        /// <summary>
        /// checks if both objects are not null and has different value in comparison
        /// </summary>
        /// <param name="changedvalue"></param>
        /// <param name="dbvalue"></param>
        /// <returns></returns>
        public static bool BothNotNullAndDifferent(object changedvalue, object dbvalue)
        {
            if ((changedvalue != null && dbvalue != null)
                &&
                (changedvalue.ToString() != dbvalue.ToString()))
            {
                return true;
            }

            return false;
        }
    }
}
