﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities.Container;

/// <summary>
/// Interface ILogContext
/// </summary>
/// <seealso cref="IDisposable" />
public interface IValidatable
{
    /// <summary>
    /// Validates 
    /// </summary>
    /// <param name="IsCreate">if set to <c>true</c> [is create].</param>
    /// <param name="validateHierarchically">if set to <c>true</c> [validate hierarchically].</param>
    /// <returns></returns>
    Result<List<ValidationResult>> Validate(bool IsCreate, bool validateHierarchically);
}

