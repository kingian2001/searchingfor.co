﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public static class ClassExtension
{

    public static bool ContainsFromStringProperties(this object obj, string text)
    {
        var stringProperties = obj.GetType().GetProperties()
                      .Where(p => p.PropertyType == typeof(string));

        foreach (var stringProperty in stringProperties)
        {
            string currentValue = (string)stringProperty.GetValue(obj, null);
            if (currentValue != null)
            {
                if (text == currentValue)
                    return true;
            }
        }

        return false;
    }
}
