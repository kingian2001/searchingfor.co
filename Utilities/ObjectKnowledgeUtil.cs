﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities
{
    /// <summary>
    /// Know about the object more
    /// </summary>
    public static class ObjectKnowledgeUtil
    {
        /// <summary>
        /// Determines whether the specified o is primitive.
        /// </summary>
        /// <param name="o">The o.</param>
        /// <returns>
        ///   <c>true</c> if the specified o is primitive; otherwise, <c>false</c>.
        /// </returns>
        public static bool IsObject(object o)
        {
            if (o != null && !o.GetType().FullName.StartsWith("System"))
                return true;

            return false;
        }
    }
}
