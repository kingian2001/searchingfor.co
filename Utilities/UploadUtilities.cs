﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;


namespace Utilities
{
    public static class UploadUtilities
    {
        /// <summary>
        /// Converts a file to byte array
        /// </summary>
        /// <param name="path">the path of the file</param>
        /// <returns></returns>
        public static Byte[] ConvertFileToByte(string path)
        {
            Byte[] byteFile = null;

            if (!String.IsNullOrEmpty(path))
            {
                using (var file = File.Open(path, FileMode.Open))
                {
                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        file.CopyTo(memoryStream);

                        return byteFile = memoryStream.ToArray();
                    }
                }

            }
            return byteFile;
        }
    }
}
