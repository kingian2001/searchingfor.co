﻿using Searchingfor.Data;
using Searchingfor.DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using NHibernate.Cfg;
using Searchingfor.Entity.Entities;
using NHibernate;
using FluentNHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using System.Linq.Expressions;
using System.Runtime.Serialization;
using System.Diagnostics;

namespace Searchingfor.DataAccess
{
    public class Instance
    {
        public List<Repository> Repositories = new List<Repository>();

        //public string _ConnectionStringName = "SearchingforConstring";

        [ThreadStatic]
        static ISession _session = null;

        [ThreadStatic]
        static ITransaction _transaction = null;

        private readonly object _syncRoot = new object();
        bool isSet = false;
        public Instance()
        {
        }

        /// <summary>
        /// Sets the session
        /// </summary>
        public void SetSession(bool saveAuto = false,
            bool distributeToRpositories = false,
            bool hasTransaction = false)
        {
            if (isSet)
                return;

            lock (_syncRoot)
            {
                if (_session != null &&
                       _session.IsOpen)
                {
                    if (distributeToRpositories)
                        DistributeSessionToRepositories(_session);

                    if (hasTransaction && _transaction == null)
                        _transaction = _session.BeginTransaction();

                    return;
                }

                try
                {
                    if (_session == null)
                        _session = NHibernateSession.Instance.SessionFactory.OpenSession();

                    if (!_session.IsOpen)
                    {
                        _session.Dispose();

                        _session = NHibernateSession.Instance.SessionFactory.OpenSession();
                    }

                    isSet = true;
                    _session.FlushMode = saveAuto ? FlushMode.Auto : FlushMode.Commit;

                    if (hasTransaction)
                        _transaction = _session.BeginTransaction();
                }
                catch (Exception ex)
                {
                    //something terribly  went wrong.
                    Dispose();
                }
            }
        }

        /// <summary>
        /// Distributes the session to all the repositories then list them on the repositories list
        /// </summary>
        /// <param name="value"></param>
        void DistributeSessionToRepositories(object value)
        {
            if (value == null)
                return;

            Repositories.Clear();

            Type[] typesInThisAssembly = Assembly.GetExecutingAssembly().GetTypes();

            foreach (Type type in typesInThisAssembly)
            {
                if (type.IsSubclassOf(typeof(ARepository)) &&
                    !type.ContainsGenericParameters)
                {
                    var instance = Activator.CreateInstance(type);

                    foreach (var propertyInfo in type.GetProperties(
                        BindingFlags.Public |
                        BindingFlags.Instance |
                        BindingFlags.FlattenHierarchy |
                        BindingFlags.GetProperty |
                        BindingFlags.NonPublic))
                    {
                        //set all with context the value
                        if (propertyInfo.PropertyType == typeof(ISession))
                        {
                            propertyInfo.SetValue(
                        instance, value, null);

                            //then add to list
                            if (!Repositories.Any(x => x.Name == type.Name))
                            {
                                Repositories.Add(new Repository
                                {
                                    Name = type.Name,
                                    Repo = instance
                                });
                            }
                            break;
                        }
                    }
                }
            }
        }

        public TRepo Repo<TRepo>() where TRepo : class, new()
        {
            if (Repositories != null && Repositories.Count > 0)
            {
                var repo = (Repositories.FirstOrDefault(x => x.Name == typeof(TRepo).Name));
                if (repo != null)
                    return repo.Repo as TRepo;
                return null;
            }
            return null;
        }

        /// <summary>
        /// saves the session and all its objects to the database
        /// </summary>
        public void SaveSession()
        {
            try
            {
                if (!_transaction.WasCommitted)
                {
                    _transaction.Commit();
                    //recreate transaction after commit.
                    //_transaction = _session.BeginTransaction();
                }
            }
            catch (Exception ex)
            {
                Dispose();

                throw ex;
            }
        }

        /// <summary>
        /// Retrieves a repository injected with _session
        /// </summary>
        /// <typeparam name="TRepo">the repository to get</typeparam>
        /// <returns></returns>
        public TRepo RepositoryFactory<TRepo>() where TRepo : class, new()
        {
            //retrieve from current list.
            var repo = this.Repo<TRepo>();
            if (repo != null)
                return repo;

            Type[] typesInThisAssembly = Assembly.GetExecutingAssembly().GetTypes();

            foreach (Type type in typesInThisAssembly)
            {
                //find type inheriting Arepository
                if (type.IsSubclassOf(typeof(ARepository)) &&
                    !type.ContainsGenericParameters)
                {
                    if (type.Name != typeof(TRepo).Name)
                        continue;

                    //create instance
                    TRepo instance = Instanciate<TRepo>.Instance();

                    foreach (var propertyInfo in type.GetProperties(
                        BindingFlags.Public |
                        BindingFlags.Instance |
                        BindingFlags.FlattenHierarchy |
                        BindingFlags.GetProperty |
                        BindingFlags.NonPublic))
                    {
                        //set all with context the session
                        if (propertyInfo.PropertyType == typeof(ISession))
                        {
                            propertyInfo.SetValue(instance, _session, null);

                            //then add to list
                            if (!Repositories.Any(x => x.Name == type.Name))
                            {
                                Repositories.Add(new Repository
                                {
                                    Name = type.Name,
                                    Repo = instance
                                });
                            }
                        }
                    }

                    return instance;
                }
            }

            return default(TRepo);
        }

        /// <summary>
        /// Disposes of all the session distributed
        /// and resources 
        /// </summary>
        public void Dispose()
        {
            Debug.WriteLine("Executing Dispose");

            if (_transaction != null)
                if (!_transaction.WasCommitted || _transaction.IsActive)
                {
                    if (!_transaction.WasRolledBack)
                        _transaction.Rollback();
                }

            if (_session != null)
                if (_session.IsOpen || _session.IsConnected)
                {
                    //_session.Close();
                    _session.Dispose();
                }

            //if (NHibernateSession.Instance.SessionFactory != null)
            //    if (!NHibernateSession.Instance.SessionFactory.IsClosed)
            //    {
            //        NHibernateSession.Instance.SessionFactory.Dispose();
            //        NHibernateSession.Instance.SessionFactory.Close();
            //        Debug.WriteLine("Session Closed");
            //    }
        }

        ~Instance()
        {
            Dispose();
            Debug.WriteLine("instance Deconstructor Executed");
        }
    }


}
