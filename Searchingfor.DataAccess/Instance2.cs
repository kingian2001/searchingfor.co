﻿using Searchingfor.Data;
using Searchingfor.DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using NHibernate.Cfg;
using Searchingfor.Entity.Entities;
using NHibernate;
using FluentNHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;
using System.Linq.Expressions;

namespace Searchingfor.DataAccess
{
    public class Instance2
    {
        public List<Repository> Repositories = new List<Repository>();

        //public string _ConnectionStringName = "SearchingforConstring";

        [ThreadStatic]
        static ISession _session = null;

        [ThreadStatic]
        static ITransaction _transaction = null;

        private readonly object _syncRoot = new object();

        public Instance2()
        {
        }

        /// <summary>
        /// Sets the session
        /// </summary>
        public void SetSession(bool saveAuto = false)
        {
            lock (_syncRoot)
            {
                if (_session != null &&
                    _session.IsOpen)
                {
                    DistributeSessionToRepositories(_session);
                    return;
                }

                try
                {
                    _session = NHibernateSession.Instance.SessionFactory.OpenSession();

                    _session.FlushMode = saveAuto ? FlushMode.Auto : FlushMode.Commit;

                    _transaction = _session.BeginTransaction();
                }
                catch (Exception ex)
                {
                    //something terribly  went wrong.
                }
                finally
                {
                    DistributeSessionToRepositories(_session);
                }
            }
        }

        /// <summary>
        /// saves the session and all its objects to the database
        /// </summary>
        public void SaveSession()
        {
            try
            {
                if (!_transaction.WasCommitted)
                {
                    _transaction.Commit();
                    //recreate transaction after commit.
                    //_transaction = _session.BeginTransaction();
                }
            }
            catch (Exception ex)
            {
                Dispose();

                SetSession();

                throw new Exception(ex.Message);
            }
        }


        /// <summary>
        /// Disposes of all the session distributed
        /// and resources 
        /// </summary>
        public void Dispose()
        {
            if (_transaction != null)
                if (!_transaction.WasCommitted)
                    _transaction.Rollback();

            if (NHibernateSession.Instance.SessionFactory != null)
                if (!NHibernateSession.Instance.SessionFactory.IsClosed)
                    NHibernateSession.Instance.SessionFactory.Close();

            _session.Close();
            _session.Dispose();
            _transaction.Dispose();
            //clears repositories
            Repositories.Clear();
        }

        /// <summary>
        /// Distributes the session to all the repositories then list them on the repositories lsit
        /// </summary>
        /// <param name="value"></param>
        void DistributeSessionToRepositories(object value)
        {
            if (value == null)
                return;

            Repositories.Clear();

            Type[] typesInThisAssembly = Assembly.GetExecutingAssembly().GetTypes();

            foreach (Type type in typesInThisAssembly)
            {
                if (type.IsSubclassOf(typeof(ARepository)) &&
                    !type.ContainsGenericParameters)
                {
                    var instance = Activator.CreateInstance(type);

                    foreach (var propertyInfo in type.GetProperties(
                        BindingFlags.Public |
                        BindingFlags.Instance |
                        BindingFlags.FlattenHierarchy |
                        BindingFlags.GetProperty |
                        BindingFlags.NonPublic))
                    {
                        //set all with context the value
                        if (propertyInfo.PropertyType == typeof(ISession))
                        {
                            propertyInfo.SetValue(
                        instance, value, null);

                            //then add to list
                            if (!Repositories.Any(x => x.Name == type.Name))
                            {
                                Repositories.Add(new Repository
                                {
                                    Name = type.Name,
                                    Repo = instance
                                });
                            }
                            break;
                        }
                    }
                }
            }
        }

        public TRepo Repo<TRepo>() where TRepo : class, new()
        {
            return (Repositories.First(x => x.Name == typeof(TRepo).Name).Repo as TRepo);
        }
    }
}
