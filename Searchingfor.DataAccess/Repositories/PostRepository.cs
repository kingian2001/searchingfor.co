﻿using Searchingfor.Entity.Entities;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Searchingfor.DataAccess.Repositories
{
    public class PostRepository : ARepository<Post>
    {
        public List<Post> FilterPosts(Dictionary<string, string> Filters = null,
            int page = 0,
            int PageSize = 0,
            string SortBy = "")
        {
            try
            {
                //                select *
                //from "Post"
                //order by ModifiedOn desc
                //limit 10
                var result = _session.QueryOver<Post>();

                switch (SortBy)
                {
                    case "moddatedesc":
                        result = result.OrderBy(x => x.ModifiedOn).Desc;
                        break;
                    case "titleasc":
                        result = result.OrderBy(x => x.Title).Asc;
                        break;
                    case "titledesc":
                        result = result.OrderBy(x => x.Title).Desc;
                        break;
                    case "budgetasc":
                        result = result.OrderBy(x => x.BudgetMax).Asc;
                        break;
                    case "budgetdesc":
                        result = result.OrderBy(x => x.BudgetMax).Desc;
                        break;
                    case "moddateasc":
                    default:
                        result = result.OrderBy(x => x.ModifiedOn).Asc;
                        break;
                }

                FilterResults(ref result, Filters);

                if (PageSize == 0)
                {
                    return result
                            .List<Post>()
                            .ToList();
                }
                else
                {
                    page = page > 0 ? page - 1 : page;

                    return result
                             .Skip(PageSize * page)
                             .Take(PageSize)
                             .List<Post>()
                             .ToList();
                }
            }
            catch
            {
                return new List<Post>();
            }
        }

        public int FilterPostsCount(Dictionary<string, string> Filters = null)
        {
            try
            {
                var result = _session.QueryOver<Post>();

                FilterResults(ref result, Filters);

                return result.RowCount();
            }
            catch
            {
                return 0;
            }
        }

        private void FilterResults(ref IQueryOver<Post, Post> result, Dictionary<string, string> Filters)
        {
            if (Filters != null || Filters.Count <= 0)
            {
                if (Filters.ContainsKey("Title"))
                {
                    result = result.Where(x => x.Title.IsInsensitiveLike(Filters["Title"], MatchMode.Anywhere) || x.Description.IsInsensitiveLike(Filters["Title"], MatchMode.Anywhere));
                }
                if (Filters.ContainsKey("Description"))
                {
                    result = result.Where(x => x.Description.IsInsensitiveLike(Filters["Description"], MatchMode.Anywhere));
                }
                if (Filters.ContainsKey("OtherDescription"))
                {
                    result = result.Where(x => x.OtherDescription.IsInsensitiveLike(Filters["OtherDescription"], MatchMode.Anywhere));
                }
                if (Filters.ContainsKey("BudgetMin"))
                {
                    result = result.Where(x => x.BudgetMin >= int.Parse(Filters["BudgetMin"]));
                }
                if (Filters.ContainsKey("BudgetMax"))
                {
                    result = result.Where(x => x.BudgetMax <= int.Parse(Filters["BudgetMax"]));
                }
                //if (Filters.ContainsKey("Urgency"))
                //{
                //    result = result.Where(x => x.Urgency == int.Parse(Filters["Urgency"]));
                //}
                if (Filters.ContainsKey("OwnerID"))
                {
                    result = result.Where(x => x.Owner.ID == Guid.Parse(Filters["OwnerID"]));
                }
                if (Filters.ContainsKey("CategoryID"))
                {
                    result = result.Where(x => x.Category.ID == Guid.Parse(Filters["CategoryID"]));
                }
                if (Filters.ContainsKey("CountryID"))
                {
                    var queryString = "select ID from \"User\" where country_id = '" + Guid.Parse(Filters["CountryID"]).ToString() + "'";

                    var query = _session.CreateSQLQuery(queryString).List();

                    result = result.AndRestrictionOn(x => x.Owner.ID).IsIn(query);
                }
                if (Filters.ContainsKey("CityID"))
                {
                    var queryString = "select ID from \"User\" where city_id = '" + Guid.Parse(Filters["CityID"]).ToString() + "'";

                    var query = _session.CreateSQLQuery(queryString).List();

                    result = result.AndRestrictionOn(x => x.Owner.ID).IsIn(query);
                }
            }
        }

        public List<Post> SearchFeatured(int Top = 10, string country = "", string city = "", bool selling = false)
        {
            List<string> filters = new List<string>() { 
                "p.Featured = true",
                "p.FeatureExpirationDate >= now()",
                "p.Status = 'active'",
            };
            if (!country.IsNullEmptyOrWhitespace() && (!country.Contains("undefined") || !country.Contains("null")))
                filters.Add("u.country='" + country.ToString() + "'");
            if (!city.IsNullEmptyOrWhitespace() && (!city.Contains("undefined") || !city.Contains("null")))
                filters.Add("u.city='" + city.ToString() + "'");
            if (selling)
                filters.Add("p.Selling=true");
            else
                filters.Add("(p.Selling=false or p.Selling = null)");

            int o = 100;
            try
            {
                o = int.Parse(ConfigurationManager.AppSettings["FeatureAdFeatureRandom"].ToString());
            }
            catch (Exception e)
            {
                o = 100;
            }

            //Featured = true and FeatureExpirationDate >= now() and Status = 'active'
            string query = @"SELECT p.* FROM ""Post"" as p TABLESAMPLE SYSTEM (" + o + @") 
            left join ""User"" as u on u.id = owner_id 
            where " + string.Join(" and ", filters.ToArray()) + " Limit " + Top;

            var result = _session.CreateSQLQuery(query).AddEntity("p", typeof(Post))
                .List<Post>();

            return result.ToList();
        }
    }
}
