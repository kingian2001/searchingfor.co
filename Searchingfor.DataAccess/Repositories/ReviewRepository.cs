﻿using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;
using Searchingfor.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.OData.Query;
using Utilities.Container;

namespace Searchingfor.DataAccess.Repositories
{
    public class ReviewRepository : ARepository<Review>
    {
        public List<Review> TrySomething()
        {
            var w = _session.QueryOver<Review>().List();


            return w.ToList();
        }

        public double AverageRating(Guid userID)
        {
            var query = "select AVG(rating) from \"Review\" as r where reviewee_id = '" + userID.ToString() + "'";

            var result = _session.CreateSQLQuery(query).UniqueResult<double>();

            return result;
        }

        public List<Review> AverageRating(List<Guid> userIDs)
        {
            List<Review> rev = new List<Review>();

            var str = String.Join("','", userIDs.ToArray());

            if (str.IsNullEmptyOrWhitespace())
                return new List<Review>();

            str = "'" + str + "'";

            var query = "SELECT r.id,r.type,r.reviewee_id,r.description,r.DateCreated,r.reviewer_id, AVG(rating) as rating FROM \"Review\" as r where reviewee_id in (" + str + ") GROUP BY r.reviewee_id,r.type,r.id";

            query = "SELECT r.type,r.reviewee_id, AVG(rating) as rating FROM \"Review\" as r where reviewee_id in (" + str + ") GROUP BY r.type,r.reviewee_id";

            var result = _session.CreateSQLQuery(query);

            var l = result.List();

            var o = l as List<object>;

            foreach (var z in o)
            {
                object[] array = z as object[];
                rev.Add(new Review
                {
                    Type = array[0].ToString(),
                    Reviewee = new User
                    {
                        ID = Guid.Parse(array[1].ToString())
                    },
                    Rating = double.Parse(array[2].ToString())
                });
            }

            return rev;
        }
    }
}
