﻿using Searchingfor.Entity.Entities;
using System;
using System.Collections.Generic;
using Searchingfor.Entity.Entities;
using NHibernate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Linq.Expressions;
using System.Threading;
using System.Web.OData.Query;
using System.Web.OData.NHibernate;
using Utilities.Container;
using NHibernate.OData;
using System.Text.RegularExpressions;
using NHibernate.Criterion;
using Searchingfor.Interface.DA;

namespace Searchingfor.DataAccess.Repositories
{
    public class PremiumRepository : ARepository<Premium>
    {
        public List<Premium> GetPremiumsOfPosts(List<Guid> postIDs)
        {
            var premiums = _session.QueryOver<Premium>().AndRestrictionOn(x => x.Post.ID).IsIn(postIDs);
            return premiums.List<Premium>().ToList();
        }
    }
}
