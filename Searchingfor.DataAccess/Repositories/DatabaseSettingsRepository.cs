﻿using NHibernate;
using NHibernate.Criterion;
using Searchingfor.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.OData.Query;
using Utilities.Container;

namespace Searchingfor.DataAccess.Repositories
{
    public class DatabaseSettingsRepository : ARepository
    {

        public void StopIdle()
        {
   
                //ku.user_id, ku.keyword_id,k.Word
                var query =
                    @"SELECT pg_terminate_backend(pid)
                    FROM pg_stat_activity
                    WHERE state = 'idle'
                    AND state_change < current_timestamp - INTERVAL '2' MINUTE;";

                var result = _session.CreateSQLQuery(query).List();

        }
    }
}
