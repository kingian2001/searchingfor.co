﻿using AutoPoco;
using AutoPoco.Engine;
using Searchingfor.Data;
using Searchingfor.Entity;
using Searchingfor.Entity.Entities;
using NHibernate;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NhibernateSchemaConsole
{
    class Program
    {
        static ISessionFactory instance;

        static int choice = 0;

        static void Main(string[] args)
        {
            try
            {
                NHibernateSession.Instance.choice = choice;

                Console.WriteLine("Press the corresponding key");
                Console.WriteLine("1 => create new schema throwing current data");
                Console.WriteLine("2 => update existing schema retaining data");

                if (NHibernateSession.Instance.choice == 0)
                    NHibernateSession.Instance.choice = int.Parse(Console.ReadLine());

                Console.WriteLine("Please wait please please");

                switch (NHibernateSession.Instance.choice)
                {
                    case 1:
                        instance = NHibernateSession.Instance.SessionFactory;
                        //WithDependency();
                        WithoutDependency();
                        break;
                    case 2:
                        instance = NHibernateSession.Instance.SessionFactory;
                        break;
                }

                Console.WriteLine("Done!");

                //var fileName = DateTime.Now.Month + "~" + DateTime.Now.Day + "~" + DateTime.Now.Year + ".txt";

                //fileName = "ERROR - " + fileName;

                //File.WriteAllText("test", fileName);

                Console.ReadLine();
            }
            catch (Exception e)
            {
                //var fileName = DateTime.Now.Month + "~" + DateTime.Now.Day + "~" + DateTime.Now.Year + ".txt";

                //fileName = "ERROR - " + fileName;

                //File.WriteAllText(e.Message, fileName);
            }
        }

        private static void WithoutDependency()
        {
            ISession ses = instance.OpenSession();

            ses.FlushMode = FlushMode.Commit;
            using (var trans = ses.Transaction)
            {
                trans.Begin();
                //Create Dummy Data using AUTOPOCO
                var factory = AutoPocoContainer.Configure(x =>
                {
                    x.Conventions(c =>
                    {
                        c.UseDefaultConventions();
                    });
                    x.AddFromAssemblyContainingType<BaseEntity>();
                });

                IGenerationSession session = factory.CreateSession();

                //set urgency
                _SetUrgency(ref session, ref ses);
                //set categories
                Categories.SetCategories(ref ses);
                //set countries
                _SetCountries(ref session, ref ses);

                if (!trans.WasCommitted)
                    trans.Commit();
            }
        }

        private static void _SetUrgency(ref IGenerationSession session, ref ISession ses)
        {
            var urgencyList = session.List<Lookup>(7)
                     .First(1)
                         .Impose(x => x.Name, "Whenever - im not in a hurry")
                         .Impose(x => x.Sort, 1)
                     .Next(1)
                         .Impose(x => x.Name, "ASAP")
                         .Impose(x => x.Sort, 2)
                     .Next(1)
                         .Impose(x => x.Name, "Within a week")
                         .Impose(x => x.Sort, 3)
                     .Next(1)
                         .Impose(x => x.Name, "Within 2 weeks")
                         .Impose(x => x.Sort, 4)
                     .Next(1)
                         .Impose(x => x.Name, "Within a month")
                         .Impose(x => x.Sort, 5)
                     .Next(1)
                         .Impose(x => x.Name, "A month from now")
                         .Impose(x => x.Sort, 6)
                     .Next(1)
                         .Impose(x => x.Name, "Months from now")
                         .Impose(x => x.Sort, 7)
                     .All()
                         .Impose(x => x.GroupName, "Urgency")
                     .Get();

            foreach (var sub in urgencyList.ToList())
                ses.Save(sub);
        }

        private static void _SetCountries(ref IGenerationSession session, ref ISession ses)
        {
            var countriesList = session.List<Lookup>(1)
                     .First(1)
                         .Impose(x => x.Name, "Philippines")
                         .Impose(x => x.Sort, 1)
                //.Next(1)
                //    .Impose(x => x.Name, "blabla1")
                //    .Impose(x => x.Sort, 2)
                //.Next(1)
                //    .Impose(x => x.Name, "blabla2")
                //    .Impose(x => x.Sort, 3)
                     .All()
                         .Impose(x => x.GroupName, "Country")
                     .Get();

            foreach (var sub in countriesList.ToList())
                ses.Save(sub);

            var citiesList = session.List<Lookup>(144)
                .First(1)
                    .Impose(x => x.Name, "Alaminos - Pangasinan")
                    .Impose(x => x.Sort, 2)
                .Next(1)
                    .Impose(x => x.Name, "Angeles - Pampanga")
                    .Impose(x => x.Sort, 3)
                .Next(1)
                    .Impose(x => x.Name, "Antipolo - Rizal")
                    .Impose(x => x.Sort, 4)
                .Next(1)
                    .Impose(x => x.Name, "Bacolod - Negros Occidental")
                    .Impose(x => x.Sort, 5)
                .Next(1)
                    .Impose(x => x.Name, "Bacoor - Cavite")
                    .Impose(x => x.Sort, 6)
                .Next(1)
                    .Impose(x => x.Name, "Bago - Negros Occidental")
                    .Impose(x => x.Sort, 7)
                .Next(1)
                    .Impose(x => x.Name, "Baguio - Benguet")
                    .Impose(x => x.Sort, 8)
                .Next(1)
                    .Impose(x => x.Name, "Bais - Negros Oriental")
                    .Impose(x => x.Sort, 9)
                .Next(1)
                    .Impose(x => x.Name, "Balanga - Bataan")
                    .Impose(x => x.Sort, 10)
                .Next(1)
                    .Impose(x => x.Name, "Batac - Ilocos Norte")
                    .Impose(x => x.Sort, 11)
                .Next(1)
                    .Impose(x => x.Name, "Batangas - Batangas")
                    .Impose(x => x.Sort, 12)
                .Next(1)
                    .Impose(x => x.Name, "Bayawan - Negros Oriental")
                    .Impose(x => x.Sort, 13)
                .Next(1)
                    .Impose(x => x.Name, "Baybay - leyte")
                    .Impose(x => x.Sort, 14)
                .Next(1)
                    .Impose(x => x.Name, "Bayugan - Agusan del Sur")
                    .Impose(x => x.Sort, 15)
                .Next(1)
                    .Impose(x => x.Name, "Biñan - Laguna")
                    .Impose(x => x.Sort, 16)
                .Next(1)
                    .Impose(x => x.Name, "Bislig - Surigao del Sur")
                    .Impose(x => x.Sort, 17)
                .Next(1)
                    .Impose(x => x.Name, "Bogo - Cebu")
                    .Impose(x => x.Sort, 18)
                .Next(1)
                    .Impose(x => x.Name, "Borongan - Eastern Samar")
                    .Impose(x => x.Sort, 19)
                .Next(1)
                    .Impose(x => x.Name, "Butuan - Agusan del Norte")
                                        .Impose(x => x.Sort, 20)
                                    .Next(1)
                    .Impose(x => x.Name, "Cabadbaran - Agusan del Norte")
                                        .Impose(x => x.Sort, 21)
                                    .Next(1)
                    .Impose(x => x.Name, "Cabanatuan - Nueva Ecija")
                                        .Impose(x => x.Sort, 22)
                                    .Next(1)
                    .Impose(x => x.Name, "Cabuyao - Laguna")
                                        .Impose(x => x.Sort, 23)
                                    .Next(1)
                    .Impose(x => x.Name, "Cadiz - Negros Occidental")
                                        .Impose(x => x.Sort, 24)
                                    .Next(1)
                    .Impose(x => x.Name, "Cagayan de Oro - Misamis Oriental")
                                        .Impose(x => x.Sort, 25)
                                    .Next(1)
                    .Impose(x => x.Name, "Calamba - Laguna")
                                        .Impose(x => x.Sort, 26)
                                    .Next(1)
                    .Impose(x => x.Name, "Calapan - Oriental Mindoro")
                                        .Impose(x => x.Sort, 27)
                                    .Next(1)
                    .Impose(x => x.Name, "Calbayog - Western Samar")
                                        .Impose(x => x.Sort, 28)
                                    .Next(1)
                    .Impose(x => x.Name, "Caloocan - Metro Manila")
                                        .Impose(x => x.Sort, 29)
                                    .Next(1)
                    .Impose(x => x.Name, "Candon - Ilocos Sur")
                                        .Impose(x => x.Sort, 30)
                                    .Next(1)
                    .Impose(x => x.Name, "Canlaon - Negros Oriental")
                                        .Impose(x => x.Sort, 31)
                                    .Next(1)
                    .Impose(x => x.Name, "Carcar - Cebu")
                                        .Impose(x => x.Sort, 32)
                                    .Next(1)
                    .Impose(x => x.Name, "Catbalogan - Western Samar")
                                        .Impose(x => x.Sort, 33)
                                    .Next(1)
                    .Impose(x => x.Name, "Cauayan - Isabela")
                                        .Impose(x => x.Sort, 34)
                                    .Next(1)
                    .Impose(x => x.Name, "Cavite - Cavite")
                                        .Impose(x => x.Sort, 35)
                                    .Next(1)
                    .Impose(x => x.Name, "Cebu - Cebu")
                                        .Impose(x => x.Sort, 36)
                                    .Next(1)
                    .Impose(x => x.Name, "Cotabato - Maguindanao")
                                        .Impose(x => x.Sort, 37)
                                    .Next(1)
                    .Impose(x => x.Name, "Dagupan - Pangasinan")
                                        .Impose(x => x.Sort, 38)
                                    .Next(1)
                    .Impose(x => x.Name, "Danao - Cebu")
                                        .Impose(x => x.Sort, 39)
                                    .Next(1)
                    .Impose(x => x.Name, "Dapitan - Zamboanga del Norte")
                                        .Impose(x => x.Sort, 40)
                                    .Next(1)
                    .Impose(x => x.Name, "Dasmarinas - Cavite")
                                        .Impose(x => x.Sort, 41)
                                    .Next(1)
                    .Impose(x => x.Name, "Davao - Davao del Sur")
                                        .Impose(x => x.Sort, 42)
                                    .Next(1)
                    .Impose(x => x.Name, "Digos - Davao del Sur")
                                        .Impose(x => x.Sort, 43)
                                    .Next(1)
                    .Impose(x => x.Name, "Dipalog - Zamboanga del Norte")
                                        .Impose(x => x.Sort, 44)
                                    .Next(1)
                    .Impose(x => x.Name, "Dumaguete - Negros Oriental")
                                        .Impose(x => x.Sort, 45)
                                    .Next(1)
                    .Impose(x => x.Name, "El Salvador - Misamis Oriental")
                                        .Impose(x => x.Sort, 46)
                                    .Next(1)
                    .Impose(x => x.Name, "Escalante - Negros Occidental")
                                        .Impose(x => x.Sort, 47)
                                    .Next(1)
                    .Impose(x => x.Name, "Gapan - Nueva Ecija")
                                        .Impose(x => x.Sort, 48)
                                    .Next(1)
                    .Impose(x => x.Name, "General Santos - South Cotabato")
                                        .Impose(x => x.Sort, 49)
                                    .Next(1)
                    .Impose(x => x.Name, "General Trias - Cavite")
                                        .Impose(x => x.Sort, 50)
                                    .Next(1)
                    .Impose(x => x.Name, "Guingoon - Misamis Oriental")
                                        .Impose(x => x.Sort, 51)
                                    .Next(1)
                    .Impose(x => x.Name, "Guihulngan - Negros Oriental")
                                        .Impose(x => x.Sort, 52)
                                    .Next(1)
                    .Impose(x => x.Name, "Himamaylan - Negros Occidental")
                                        .Impose(x => x.Sort, 53)
                                    .Next(1)
                    .Impose(x => x.Name, "Ilagan - Isabela")
                                        .Impose(x => x.Sort, 54)
                                    .Next(1)
                    .Impose(x => x.Name, "Iligan - Lanao del Norte")
                                        .Impose(x => x.Sort, 55)
                                    .Next(1)
                    .Impose(x => x.Name, "Iloilo - Iloilo")
                                        .Impose(x => x.Sort, 56)
                                    .Next(1)
                    .Impose(x => x.Name, "Imus - Cavite")
                                        .Impose(x => x.Sort, 57)
                                    .Next(1)
                    .Impose(x => x.Name, "Iriga - Camarines Sur")
                                        .Impose(x => x.Sort, 58)
                                    .Next(1)
                    .Impose(x => x.Name, "Isabela - Basilan")
                                        .Impose(x => x.Sort, 59)
                                    .Next(1)
                    .Impose(x => x.Name, "Kabancalan - Negros Occidental")
                                        .Impose(x => x.Sort, 60)
                                    .Next(1)
                    .Impose(x => x.Name, "Kidapawan - North Cotabato")
                                        .Impose(x => x.Sort, 61)
                                    .Next(1)
                    .Impose(x => x.Name, "Koronadal - South Cotabato")
                                        .Impose(x => x.Sort, 62)
                                    .Next(1)
                    .Impose(x => x.Name, "La Carlota - Negros Occidental")
                                        .Impose(x => x.Sort, 63)
                                    .Next(1)
                    .Impose(x => x.Name, "Lamitan - Basilan")
                                        .Impose(x => x.Sort, 64)
                                    .Next(1)
                    .Impose(x => x.Name, "Laoag - Ilocos Norte")
                                        .Impose(x => x.Sort, 65)
                                    .Next(1)
                    .Impose(x => x.Name, "Lapu‑Lapu - Cebu")
                                        .Impose(x => x.Sort, 66)
                                    .Next(1)
                    .Impose(x => x.Name, "Las Piñas - Metro Manila")
                                        .Impose(x => x.Sort, 67)
                                    .Next(1)
                    .Impose(x => x.Name, "Legazpi - Albay")
                                        .Impose(x => x.Sort, 68)
                                    .Next(1)
                    .Impose(x => x.Name, "Ligao - Albay")
                                        .Impose(x => x.Sort, 69)
                                    .Next(1)
                    .Impose(x => x.Name, "Lipa - batangas")
                                        .Impose(x => x.Sort, 70)
                                    .Next(1)
                    .Impose(x => x.Name, "Lucena - Quezon")
                                        .Impose(x => x.Sort, 71)
                                    .Next(1)
                    .Impose(x => x.Name, "Maasin - Southern Leyte")
                                        .Impose(x => x.Sort, 72)
                                    .Next(1)
                    .Impose(x => x.Name, "Mabalacat - Pampanga")
                                        .Impose(x => x.Sort, 73)
                                    .Next(1)
                    .Impose(x => x.Name, "Makati - Metro Manila")
                                        .Impose(x => x.Sort, 74)
                                    .Next(1)
                    .Impose(x => x.Name, "Malabon - Metro Manila")
                                        .Impose(x => x.Sort, 75)
                                    .Next(1)
                    .Impose(x => x.Name, "Malaybalay - Bukidnon")
                                        .Impose(x => x.Sort, 76)
                                    .Next(1)
                    .Impose(x => x.Name, "Malolos - Bulacan")
                                        .Impose(x => x.Sort, 77)
                                    .Next(1)
                    .Impose(x => x.Name, "Mandaluyong - Metro Manila")
                                        .Impose(x => x.Sort, 78)
                                    .Next(1)
                    .Impose(x => x.Name, "Mandaue - Cebu")
                                        .Impose(x => x.Sort, 79)
                                    .Next(1)
                    .Impose(x => x.Name, "Manila - Metro Manila")
                    .Impose(x => x.Sort, 80)
                .Next(1)
                    .Impose(x => x.Name, "Marawi - Lanao del sur")
                    .Impose(x => x.Sort, 81)
                .Next(1)
                    .Impose(x => x.Name, "Marikina - Metro Manila")
                    .Impose(x => x.Sort, 82)
                .Next(1)
                    .Impose(x => x.Name, "Masbate - Masbate")
                    .Impose(x => x.Sort, 83)
                .Next(1)
                    .Impose(x => x.Name, "Mati - Davao Oriental")
                    .Impose(x => x.Sort, 84)
                .Next(1)
                    .Impose(x => x.Name, "Meycauayan - Bulacan")
                    .Impose(x => x.Sort, 85)
                .Next(1)
                    .Impose(x => x.Name, "Muñoz - Nueva Ecija")
                    .Impose(x => x.Sort, 86)
                .Next(1)
                    .Impose(x => x.Name, "Muntinlupa - Metro Manila")
                    .Impose(x => x.Sort, 87)
                .Next(1)
                    .Impose(x => x.Name, "Naga - Camarines Sur")
                    .Impose(x => x.Sort, 88)
                .Next(1)
                    .Impose(x => x.Name, "Naga - Cebu")
                    .Impose(x => x.Sort, 89)
                .Next(1)
                    .Impose(x => x.Name, "Navotas - Metro Manila")
                    .Impose(x => x.Sort, 90)
                .Next(1)
                    .Impose(x => x.Name, "Olongapo - Zambales")
                    .Impose(x => x.Sort, 91)
                .Next(1)
                    .Impose(x => x.Name, "Ormoc - Leyte")
                    .Impose(x => x.Sort, 92)
                .Next(1)
                    .Impose(x => x.Name, "Oroquieta - Misamis Occidental")
                    .Impose(x => x.Sort, 93)
                .Next(1)
                    .Impose(x => x.Name, "Ozamiz - Misamis Occidental")
                    .Impose(x => x.Sort, 94)
                .Next(1)
                    .Impose(x => x.Name, "Pagadian - Zamboanga del sur")
                    .Impose(x => x.Sort, 95)
                .Next(1)
                    .Impose(x => x.Name, "Palayan - Nueva Ecija")
                    .Impose(x => x.Sort, 96)
                .Next(1)
                    .Impose(x => x.Name, "Panabo - Davao del norte")
                    .Impose(x => x.Sort, 97)
                .Next(1)
                    .Impose(x => x.Name, "Parañaque - Metro Manila")
                    .Impose(x => x.Sort, 98)
                .Next(1)
                    .Impose(x => x.Name, "Pasay - Metro Manila")
                    .Impose(x => x.Sort, 99)
                .Next(1)
                    .Impose(x => x.Name, "Pasig - Metro Manila")
                    .Impose(x => x.Sort, 100)
                .Next(1)
                    .Impose(x => x.Name, "Passi - Iloilo")
                    .Impose(x => x.Sort, 101)
                .Next(1)
                    .Impose(x => x.Name, "Puerto Princesa - Palawan")
                    .Impose(x => x.Sort, 102)
                .Next(1)
                    .Impose(x => x.Name, "Quezon - Metro Manila")
                    .Impose(x => x.Sort, 103)
                .Next(1)
                    .Impose(x => x.Name, "Roxas - Capiz")
                    .Impose(x => x.Sort, 104)
                .Next(1)
                    .Impose(x => x.Name, "Sagay - Negros Occidental")
                    .Impose(x => x.Sort,105)
                .Next(1)
                    .Impose(x => x.Name, "Samal - Davao del Norte")
                    .Impose(x => x.Sort,106)
                .Next(1)
                    .Impose(x => x.Name, "San Carlos - Negros Occidental")
                    .Impose(x => x.Sort, 107)
                .Next(1)
                    .Impose(x => x.Name, "San Fernando - La Union")
                    .Impose(x => x.Sort, 108)
                .Next(1)
                    .Impose(x => x.Name, "San Fernando - Pampanga")
                    .Impose(x => x.Sort, 109)
                .Next(1)
                    .Impose(x => x.Name, "San Jose - Nueva Ecija")
                    .Impose(x => x.Sort, 110)
                .Next(1)
                    .Impose(x => x.Name, "San Jose del Monte - Bulacan")
                    .Impose(x => x.Sort, 111)
                .Next(1)
                    .Impose(x => x.Name, "San Juan - Metro Manila")
                    .Impose(x => x.Sort,112)
                .Next(1)
                    .Impose(x => x.Name, "San Pablo - Laguna")
                    .Impose(x => x.Sort, 113)
                .Next(1)
                    .Impose(x => x.Name, "San Pedro - Laguna")
                    .Impose(x => x.Sort, 114)
                .Next(1)
                    .Impose(x => x.Name, "Santa Rosa - Laguna")
                    .Impose(x => x.Sort,115)
                .Next(1)
                    .Impose(x => x.Name, "Santiago - Isabela")
                    .Impose(x => x.Sort, 116)
                .Next(1)
                    .Impose(x => x.Name, "Silay - Negros Occidental")
                    .Impose(x => x.Sort, 117)
                .Next(1)
                    .Impose(x => x.Name, "Sipalay - Negros Occidental")
                    .Impose(x => x.Sort, 118)
                .Next(1)
                    .Impose(x => x.Name, "Sorsogon - Sorsogon")
                    .Impose(x => x.Sort, 119)
                .Next(1)
                    .Impose(x => x.Name, "Surigao - Surigao del Norte")
                    .Impose(x => x.Sort, 120)
                .Next(1)
                    .Impose(x => x.Name, "Tabaco - Albay")
                    .Impose(x => x.Sort, 121)
                .Next(1)
                    .Impose(x => x.Name, "Tabuk - Kalinga")
                    .Impose(x => x.Sort, 122)
                .Next(1)
                    .Impose(x => x.Name, "Tacloban - Negros Occidental")
                    .Impose(x => x.Sort, 123)
                .Next(1)
                    .Impose(x => x.Name, "Tacurong - Sultan Kudarat")
                    .Impose(x => x.Sort, 124)
                .Next(1)
                    .Impose(x => x.Name, "Tagaytay - Cavite")
                    .Impose(x => x.Sort, 125)
                .Next(1)
                    .Impose(x => x.Name, "Tagbilaran - Bohol")
                    .Impose(x => x.Sort, 126)
                .Next(1)
                    .Impose(x => x.Name, "Taguig - Metro Manila")
                    .Impose(x => x.Sort, 127)
                .Next(1)
                    .Impose(x => x.Name, "Tagum - Davao del Norte")
                    .Impose(x => x.Sort, 128)
                .Next(1)
                    .Impose(x => x.Name, "Talisay - Cebu")
                    .Impose(x => x.Sort, 129)
                .Next(1)
                    .Impose(x => x.Name, "Talisay - Negros Occidental")
                    .Impose(x => x.Sort, 130)
                .Next(1)
                    .Impose(x => x.Name, "Tanauan - Batangas")
                    .Impose(x => x.Sort, 131)
                .Next(1)
                    .Impose(x => x.Name, "Tandag - Surigao del Sur")
                    .Impose(x => x.Sort, 132)
                .Next(1)
                    .Impose(x => x.Name, "Tangub - Misamis Occidental")
                    .Impose(x => x.Sort, 133)
                .Next(1)
                    .Impose(x => x.Name, "Tanjay - Negros Oriental")
                    .Impose(x => x.Sort, 134)
                .Next(1)
                    .Impose(x => x.Name, "Tarlac - Tarlac")
                    .Impose(x => x.Sort, 135)
                .Next(1)
                    .Impose(x => x.Name, "Tayabas - Quezon")
                    .Impose(x => x.Sort, 136)
                .Next(1)
                    .Impose(x => x.Name, "Toledo - Cebu")
                    .Impose(x => x.Sort, 137)
                .Next(1)
                    .Impose(x => x.Name, "Trece Martires - Cavite")
                    .Impose(x => x.Sort, 138)
                .Next(1)
                    .Impose(x => x.Name, "Tuguegarao - Cagayan")
                    .Impose(x => x.Sort, 139)
                .Next(1)
                    .Impose(x => x.Name, "Urdaneta - Pangasinan")
                    .Impose(x => x.Sort, 140)
                .Next(1)
                    .Impose(x => x.Name, "Valencia - Bukidnon")
                    .Impose(x => x.Sort, 141)
                .Next(1)
                    .Impose(x => x.Name, "Valenzuela - Metro Manila")
                    .Impose(x => x.Sort, 142)
                .Next(1)
                    .Impose(x => x.Name, "Victorias - Negros Occidental")
                    .Impose(x => x.Sort, 143)
                .Next(1)
                    .Impose(x => x.Name, "Vigan - Ilocos Sur")
                    .Impose(x => x.Sort, 144)
                .Next(1)
                    .Impose(x => x.Name, "Zamboanga - Zamboanga del Sur")
                    .Impose(x => x.Sort, 145)
                    .All()
                        .Impose(x => x.GroupName, "Philippines")
                    .Get();

            foreach (var sub in citiesList.ToList())
                ses.Save(sub);
        }

        /// <summary>
        /// saves each list
        /// </summary>
        /// <param name="list"></param>
        /// <param name="ses"></param>
        public static void SaveEach<T>(IList<T> list, ref ISession ses)
        {
            foreach (var sub in list.ToList())
                ses.Save(sub);
        }

        private static string RandomString(int length)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < length; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            return builder.ToString();
        }


    }
}
