﻿using AutoPoco;
using AutoPoco.Engine;
using NHibernate;
using Searchingfor.Core.Models;
using Searchingfor.Core.ViewModels;
using Searchingfor.DataAccess;
using Searchingfor.DataAccess.Repositories;
using Searchingfor.Entity;
using Searchingfor.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NhibernateSchemaConsole
{
    public static class Categories
    {
        public static Instance instance = null;

        public static CategoryVM vm = null;

        public static ISession session = null;

        static IGenerationSession auto = null;

        static Categories()
        {
            instance = new Instance();
            vm = new CategoryVM(instance);

            var factory = AutoPocoContainer.Configure(x =>
            {
                x.Conventions(c =>
                {
                    c.UseDefaultConventions();
                });
                x.AddFromAssemblyContainingType<BaseEntity>();
            });

            auto = factory.CreateSession();
        }

        /***
         * Command Codes
         * bsr = buyableswappablerentable
         * s = services
         * j = jobs
         * c = community
         * */

        internal static void SetCategories(ref ISession ses)
        {
            IList<CategoryVM> list =
                new List<CategoryVM>{
                    new CategoryVM{
                         ID= Guid.Parse("f796be27-2193-405b-ae63-000000010000"),
                         Name= "Buyable/Swappable/Rentable" ,
                         MaterializedPath= "Buyable/Swappable/Rentable" ,
                         Sort= 1 ,
                         CommandCode= "bsr",
                         Parent = null,
                    },
                    new CategoryVM{
                         ID = Guid.Parse("F796BE27-2193-405B-AE63-000000020000"),
                         Name = "Service" ,
                         MaterializedPath = "Service" ,
                         Sort = 2 ,
                         CommandCode  ="s" ,
                         Parent = null
                    },
                    new CategoryVM{
                        ID= Guid.Parse("F796BE27-2193-405B-AE63-000000030000"),
                        Name= "Job",
                        MaterializedPath= "Job",
                        Sort= 3,
                        CommandCode= "j",
                        Parent = null
                    },
                    new CategoryVM{
                     ID = Guid.Parse("F796BE27-2193-405B-AE63-000000040000"),
                     Name = "Community" ,
                     MaterializedPath = "Community" ,
                     Sort=  4 ,
                     CommandCode = "c" ,
                     Parent = null
                    }
                };

            foreach (var sub in list.ToList())
                sub.Create(false);

            SetBuyableSwappableRentable(ref ses);

            SetServices(ref ses);

            SetJobs(ref auto, ref ses);

            SetCommunity(ref auto, ref ses);

            instance.SaveSession();
        }

        private static void SetBuyableSwappableRentable(ref ISession ses)
        {
            IList<CategoryVM> bsSubCategory = new List<CategoryVM>
            {
                new CategoryVM{
                         ID= Guid.Parse("F796BE27-2193-405B-AE63-000000011000"),
                         Name= "Mobile",
                         MaterializedPath= "Buyable/Swappable/Rentable.Mobile",
                         Sort= 1,
                         Parent = new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000010000") },
                            },
                    new CategoryVM{
                         ID= Guid.Parse("F796BE27-2193-405B-AE63-000000012000"),
                         Name= "Computer",
                         MaterializedPath= "Buyable/Swappable/Rentable.Computer",
                         Sort= 2,
                          Parent = new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000010000") },
                    },
                    new CategoryVM{
                         ID= Guid.Parse("F796BE27-2193-405B-AE63-000000013000"),
                         Name= "Clothing",
                         MaterializedPath= "Buyable/Swappable/Rentable.Clothing",
                         Sort= 3,
                          Parent = new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000010000") },
                    },
                    new CategoryVM{
                         ID= Guid.Parse("F796BE27-2193-405B-AE63-000000014000"),
                         Name= "Cars, Motorcycles and Automotives",
                         MaterializedPath= "Buyable/Swappable/Rentable.Cars, Motorcycles and Automotives",
                         Sort= 4,
                          Parent = new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000010000") },
                    },
                    new CategoryVM{
                         ID=Guid.Parse("F796BE27-2193-405B-AE63-000000015000"),
                         MaterializedPath= "Buyable/Swappable/Rentable.Furnature and Appliances",
                         Name= "Furnature and Appliances",
                         Sort= 5,
                          Parent = new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000010000") },
                    },
                    new CategoryVM{
                         ID= Guid.Parse("F796BE27-2193-405B-AE63-000000016000"),
                         MaterializedPath= "Buyable/Swappable/Rentable.Pet",
                         Name= "Pet",
                         Sort= 6,
                          Parent = new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000010000") },
                    },
                    new CategoryVM{
                         ID= Guid.Parse("F796BE27-2193-405B-AE63-000000017000"),
                         MaterializedPath= "Buyable/Swappable/Rentable.Real Estate",
                         Name= "Real Estate",
                         Sort= 7,
                          Parent = new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000010000") },
                    },
                    new CategoryVM{
                         ID= Guid.Parse("F796BE27-2193-405B-AE63-000000018000"),
                         MaterializedPath= "Buyable/Swappable/Rentable.Hobbies",
                         Name= "Hobbies",
                         Sort= 8,
                          Parent = new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000010000") },
                },
                new CategoryVM{
                        ID= Guid.Parse("F796BE27-2193-405B-AE63-000000019000"),
                        MaterializedPath= "Buyable/Swappable/Rentable.Professional Equipments",
                        Name= "Professional Equipments",
                        Sort= 9,
                        Parent = new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000010000") },
                },
                new CategoryVM{
                        ID= Guid.Parse("F796BE27-2193-405B-AE63-200000019000"),
                        MaterializedPath= "Buyable/Swappable/Rentable.Consumer Electronics",
                        Name= "Consumer Electronics",
                        Sort= 9,
                        Parent = new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000010000") },
                },
                new CategoryVM{
                        ID= Guid.Parse("F796BE27-2193-405B-AE63-000100019999"),
                        MaterializedPath= "Buyable/Swappable/Rentable.Others",
                        Name= "Others",
                        Sort= 99,
                        Parent = new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000010000") },
                }
            };

            foreach (var sub in bsSubCategory.ToList())
                sub.Create(false);

            BuyableSwappableSubCategories(ref ses);
        }

        private static void BuyableSwappableSubCategories(ref ISession ses)
        {
            #region mobile
            IList<CategoryVM> mobileSubCategory = new List<CategoryVM>
              {
                  new CategoryVM{
                           ID= Guid.Parse("F796BE27-2193-405B-AE63-000000011100"),
                         MaterializedPath= "Buyable/Swappable/Rentable.Mobile.Mobile Phones and Smart Phones",
                         Name= "Mobile Phones and Smart Phones",
                         Sort= 1,
                         Parent = new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000011000") },
                     },
                    new CategoryVM{
                         ID= Guid.Parse("F796BE27-2193-405B-AE63-000000011200"),
                         MaterializedPath= "Buyable/Swappable/Rentable.Mobile.Accessories",
                         Name= "Accessories",
                         Sort= 2,
                         Parent = new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000011000") },
                    },
                    new CategoryVM{
                         ID= Guid.Parse("F796BE27-2193-405B-AE63-000000011300"),
                         MaterializedPath= "Buyable/Swappable/Rentable.Mobile.Tablets",
                         Name= "Tablets",
                         Sort= 3,
                         Parent = new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000011000") },
                    },
                    new CategoryVM{
                         ID= Guid.Parse("F796BE27-2193-405B-AE63-000000011999"),
                         MaterializedPath= "Buyable/Swappable/Rentable.Mobile.Others",
                         Name="Others",
                         Sort= 99,
                         Parent = new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000011000") },
                    }
              };

            foreach (var sub in mobileSubCategory.ToList())
                sub.Create(false);
            #endregion

            #region computer
            IList<CategoryVM> computersSubCategory = new List<CategoryVM>
           {
                new CategoryVM{
                       ID= Guid.Parse("F796BE27-2193-405B-AE63-000000012110"),
                       MaterializedPath= "Buyable/Swappable/Rentable.Computer.Notebooks, Laptops and Netbooks",
                       Name= "Notebooks, Laptops and Netbooks",
                       Sort= 1,
                       Parent=new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000012000")}
                  },
                  new CategoryVM{
                       ID= Guid.Parse("F796BE27-2193-405B-AE63-000000012200"),
                       MaterializedPath= "Buyable/Swappable/Rentable.Computer.Peripherals, Components, and Parts",
                       Name= "Peripherals, Components, and Parts",
                       Sort= 2,
                       Parent=new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000012000")}
                  },
                  new CategoryVM{
                       ID= Guid.Parse("F796BE27-2193-405B-AE63-000000012300"),
                       MaterializedPath= "Buyable/Swappable/Rentable.Computer.Accessories",
                       Name= "Accessories",
                       Sort= 3,
                       Parent=new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000012000")}
                  },
                  new CategoryVM{
                       ID= Guid.Parse("F796BE27-2193-405B-AE63-000000012400"),
                       MaterializedPath= "Buyable/Swappable/Rentable.Computer.Printers and Scanners",
                       Name= "Printers and Scanners",
                       Sort= 4,
                       Parent=new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000012000")}
                  },
                  new CategoryVM{
                       ID= Guid.Parse("F796BE27-2193-405B-AE63-000000012500"),
                       MaterializedPath= "Buyable/Swappable/Rentable.Computer.Desktops",
                       Name= "Desktops",
                       Sort= 5,
                       Parent=new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000012000")}
                  },
                  new CategoryVM{
                       ID= Guid.Parse("F796BE27-2193-405B-AE63-000000012600"),
                       MaterializedPath= "Buyable/Swappable/Rentable.Computer.Networking and Servers",
                       Name= "Networking and Servers",
                       Sort= 6,
                       Parent=new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000012000")}
                  },
                  new CategoryVM{
                       ID= Guid.Parse("F796BE27-2193-405B-AE63-000000012700"),
                       MaterializedPath= "Buyable/Swappable/Rentable.Computer.Storage Devices",
                       Name= "Storage Devices",
                       Sort= 7,
                       Parent=new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000012000")}
                  },
                  new CategoryVM{
                       ID= Guid.Parse("F796BE27-2193-405B-AE63-000000012800"),
                       MaterializedPath= "Buyable/Swappable/Rentable.Computer.Internet Gadgets",
                       Name= "Internet Gadgets",
                       Sort= 8,
                       Parent=new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000012000")}
                  },
                  new CategoryVM{
                       ID= Guid.Parse("F796BE27-2193-405B-AE63-000000012900"),
                       MaterializedPath= "Buyable/Swappable/Rentable.Computer.Computer Monitors and LCDs",
                       Name= "Computer Monitors and LCDs",
                       Sort= 9,
                       Parent=new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000012000")}
                  },
                   new CategoryVM{
                       ID= Guid.Parse("F796BE27-2193-405B-AE63-000000012120"),
                       MaterializedPath= "Buyable/Swappable/Rentable.Computer.Software",
                       Name= "Software",
                       Sort= 10,
                       Parent=new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000012000")}
                  },
                  new CategoryVM{
                       ID= Guid.Parse("F796BE27-2193-405B-AE63-000000012999"),
                       MaterializedPath= "Buyable/Swappable/Rentable.Computer.Others",
                       Name= "Others",
                       Sort= 99,
                       Parent=new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000012000")}
                  }
           };

            foreach (var sub in computersSubCategory.ToList())
                sub.Create(false);
            #endregion

            #region Clothing
            IList<CategoryVM> clothingSubCategory = new List<CategoryVM>
           {
                new CategoryVM{
                   ID= Guid.Parse("F796BE27-2193-405B-AE63-000000013100"),
                   MaterializedPath= "Buyable/Swappable/Rentable.Clothing.Footwear",
                   Name= "Footwear",
                   Sort= 1,
                   Parent = new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000013000") }
                },
              new CategoryVM{
                   ID= Guid.Parse("F796BE27-2193-405B-AE63-000000013200"),
                   MaterializedPath= "Buyable/Swappable/Rentable.Clothing.Clothes",
                   Name= "Clothes",
                   Sort= 2,
                   Parent = new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000013000") }
              },
              new CategoryVM{
                   ID= Guid.Parse("F796BE27-2193-405B-AE63-000000013300"),
                   MaterializedPath= "Buyable/Swappable/Rentable.Clothing.Luggage, Bags and Wallets",
                   Name= "Luggage, Bags and Wallets",
                   Sort= 3,
                   Parent = new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000013000") }
              },
              new CategoryVM{
                   ID= Guid.Parse("F796BE27-2193-405B-AE63-000000013400"),
                   MaterializedPath= "Buyable/Swappable/Rentable.Clothing.Jewelry and Watches",
                   Name= "Jewelry and Watches",
                   Sort= 4,
                   Parent = new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000013000") }
              },
              new CategoryVM{
                   ID= Guid.Parse("F796BE27-2193-405B-AE63-000000013500"),
                   MaterializedPath= "Buyable/Swappable/Rentable.Clothing.Accessories and Glasses",
                   Name= "Accessories and Glasses",
                   Sort= 5,
                   Parent = new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000013000") }
              },
              new CategoryVM{
                   ID= Guid.Parse("F796BE27-2193-405B-AE63-000000013600"),
                   MaterializedPath= "Buyable/Swappable/Rentable.Clothing.Costumes",
                   Name= "Costumes",
                   Sort= 6,
                   Parent = new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000013000") }
              },
              new CategoryVM{
                   ID= Guid.Parse("F796BE27-2193-405B-AE63-000000013999"),
                   MaterializedPath= "Buyable/Swappable/Rentable.Clothing.Others",
                   Name= "Others",
                   Sort= 99,
                   Parent = new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000013000") }
              }
           };

            foreach (var sub in clothingSubCategory.ToList())
                sub.Create(false);
            #endregion

            #region CMAs
            IList<CategoryVM> CMASubCategory = new List<CategoryVM>
            {
                 new CategoryVM {
                   ID= Guid.Parse("F796BE27-2193-405B-AE63-000000014100"),
                   MaterializedPath= "Buyable/Swappable/Rentable.Cars, Motorcycles and Automotives.Cars and Sedans",
                   Name= "Cars and Sedans",
                   Sort= 1,
                    Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000014000") }
                 },
              new CategoryVM {
                   ID= Guid.Parse("F796BE27-2193-405B-AE63-000000014200"),
                   MaterializedPath= "Buyable/Swappable/Rentable.Cars, Motorcycles and Automotives.SUV, AUV, Pickups, Jeeps, 4WDs",
                   Name= "SUV, AUV, Pickups, Jeeps, 4WDs",
                   Sort= 2,
                    Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000014000") }
              },
              new CategoryVM {
                   ID= Guid.Parse("F796BE27-2193-405B-AE63-000000014300"),
                   MaterializedPath= "Buyable/Swappable/Rentable.Cars, Motorcycles and Automotives.Vans, MPVs and RVs",
                   Name= "Vans, MPVs and RVs",
                   Sort= 3,
                    Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000014000") }
              },
              new CategoryVM {
                   ID= Guid.Parse("F796BE27-2193-405B-AE63-000000014400"),
                   MaterializedPath="Buyable/Swappable/Rentable.Cars, Motorcycles and Automotives.Automotive Parts and Accessories",
                   Name= "Automotive Parts and Accessories",
                   Sort= 4,
                    Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000014000") }
              },
              new CategoryVM {
                   ID= Guid.Parse("F796BE27-2193-405B-AE63-000000014500"),
                   MaterializedPath= "Buyable/Swappable/Rentable.Cars, Motorcycles and Automotives.Motorcycles and Scooters",
                   Name= "Motorcycles and Scooters",
                   Sort= 5,
                    Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000014000") }
              },
              new CategoryVM {
                   ID= Guid.Parse("F796BE27-2193-405B-AE63-000000014600"),
                   MaterializedPath= "Buyable/Swappable/Rentable.Cars, Motorcycles and Automotives.Trucks and Buses",
                   Name= "Trucks and Buses",
                   Sort= 6,
                    Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000014000") }
              },
              new CategoryVM {
                   ID= Guid.Parse("F796BE27-2193-405B-AE63-000000014700"),
                   MaterializedPath= "Buyable/Swappable/Rentable.Cars, Motorcycles and Automotives.Boats, Aircrafts and Recreational Vehicles",
                   Name= "Boats, Aircrafts and Recreational Vehicles",
                   Sort= 7,
                    Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000014000") }
              },
              new CategoryVM {
                   ID= Guid.Parse("F796BE27-2193-405B-AE63-000000014999"),
                   MaterializedPath= "Buyable/Swappable/Rentable.Cars, Motorcycles and Automotives.Others",
                   Name= "Others",
                   Sort= 8,
                    Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000014000") }
              }
            };

            foreach (var sub in CMASubCategory.ToList())
                sub.Create(false);
            #endregion

            #region appliances
            IList<CategoryVM> appliancesSubCategory = new List<CategoryVM>
            {new CategoryVM {
                   ID= Guid.Parse("F796BE27-2193-405B-AE63-000000015100"),
                   MaterializedPath= "Buyable/Swappable/Rentable.Furnature and Appliances.Lighting and Electricals",
                   Name= "Lighting and Electricals",
                   Sort= 1,
                   Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000015000") }
            },
              new CategoryVM {
                    ID= Guid.Parse("F796BE27-2193-405B-AE63-000000015200"),
                   MaterializedPath= "Buyable/Swappable/Rentable.Furnature and Appliances.Other Kitchen Appliances",
                   Name= "Other Kitchen Appliances",
                   Sort= 2,
                   Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000015000") }
              },
              new CategoryVM {
                   ID=Guid.Parse("F796BE27-2193-405B-AE63-000000015300"),
                   MaterializedPath= "Buyable/Swappable/Rentable.Furnature and Appliances.Air Conditioning and Heating",
                   Name= "Air Conditioning and Heating",
                   Sort= 3,
                   Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000015000") }
              },
              new CategoryVM {
                    ID= Guid.Parse("F796BE27-2193-405B-AE63-000000015400"),
                   MaterializedPath= "Buyable/Swappable/Rentable.Furnature and Appliances.Cooking and Ovens",
                   Name= "Cooking and Ovens",
                   Sort= 4,
                   Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000015000") }
              },
              new CategoryVM {
                    ID= Guid.Parse("F796BE27-2193-405B-AE63-000000015500"),
                   MaterializedPath= "Buyable/Swappable/Rentable.Furnature and Appliances.Refrigerators and Freezers",
                   Name= "Refrigerators and Freezers",
                   Sort= 5,
                   Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000015000") }
              },
              new CategoryVM {
                    ID= Guid.Parse("F796BE27-2193-405B-AE63-000000015600"),
                   MaterializedPath= "Buyable/Swappable/Rentable.Furnature and Appliances.Water Dispenser and Purifier",
                   Name= "Water Dispenser and Purifier",
                   Sort= 6,
                   Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000015000") }
              },
              new CategoryVM {
                    ID= Guid.Parse("F796BE27-2193-405B-AE63-000000015700"),
                   MaterializedPath = "Buyable/Swappable/Rentable.Furnature and Appliances.Washing Machines and Dryers",
                   Name= "Washing Machines and Dryers",
                   Sort= 7,
                   Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000015000") }
              },
              new CategoryVM {
                    ID= Guid.Parse("F796BE27-2193-405B-AE63-000000015800"),
                   MaterializedPath= "Buyable/Swappable/Rentable.Furnature and Appliances.Electric Fans",
                   Name= "Electric Fans",
                   Sort= 8,
                   Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000015000") }
              },
              new CategoryVM {
                    ID= Guid.Parse("F796BE27-2193-405B-AE63-000000015900"),
                   MaterializedPath= "Buyable/Swappable/Rentable.Furnature and Appliances.Sewing Machines",
                   Name="Sewing Machines",
                   Sort= 9,
                   Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000015000") }
              },
              new CategoryVM {
                    ID= Guid.Parse("F796BE27-2193-405B-AE63-000000015110"),
                   MaterializedPath= "Buyable/Swappable/Rentable.Furnature and Appliances.Other Kitchen Appliances",
                   Name= "Furnature and Fixture",
                   Sort= 10,
                   Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000015000") }
              },
              new CategoryVM {
                    ID= Guid.Parse("F796BE27-2193-405B-AE63-000000015120"),
                   MaterializedPath= "Buyable/Swappable/Rentable.Furnature and Appliances.Outdoors and Gardens",
                   Name= "Outdoors and Gardens",
                   Sort= 11,
                   Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000015000") }
              },
              new CategoryVM {
                    ID= Guid.Parse("F796BE27-2193-405B-AE63-000000015130"),
                   MaterializedPath= "Buyable/Swappable/Rentable.Furnature and Appliances.Appliances",
                   Name= "Appliances",
                   Sort= 12,
                   Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000015000") }
              },

            };


            foreach (var sub in appliancesSubCategory.ToList())
                sub.Create(false);
            #endregion

            #region pets
            IList<CategoryVM> petsSubCategory = new List<CategoryVM>
           {new CategoryVM {
                   ID= Guid.Parse("F796BE27-2193-405B-AE63-000000016100"),
                   MaterializedPath = "Buyable/Swappable/Rentable.Pet.Dogs",
                   Name= "Dogs",
                   Sort= 1,
                    Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000016000") }
           },
              new CategoryVM {
                   ID= Guid.Parse("F796BE27-2193-405B-AE63-000000016200"),
                   MaterializedPath= "Buyable/Swappable/Rentable.Pet.Cats",
                   Name="Cats",
                   Sort= 2,
                    Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000016000") }
           },
              new CategoryVM {
                   ID= Guid.Parse("F796BE27-2193-405B-AE63-000000016300"),
                   MaterializedPath= "Buyable/Swappable/Rentable.Pet.Accessories for Pets",
                   Name= "Accessories for Pets",
                   Sort= 3,
                    Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000016000") }
           },
              new CategoryVM {
                   ID= Guid.Parse("F796BE27-2193-405B-AE63-000000016400"),
                   MaterializedPath= "Buyable/Swappable/Rentable.Pet.Birds",
                   Name= "Birds",
                   Sort= 4,
                    Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000016000") }
           },
              new CategoryVM {
                   ID= Guid.Parse("F796BE27-2193-405B-AE63-000000016500"),
                   MaterializedPath= "Buyable/Swappable/Rentable.Pet.Fishes",
                   Name= "Fishes",
                   Sort= 5,
                    Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000016000") }
           },
              new CategoryVM {
                   ID= Guid.Parse("F796BE27-2193-405B-AE63-000000016600"),
                   MaterializedPath= "Buyable/Swappable/Rentable.Pet.Exotic",
                   Name= "Exotic",
                   Sort= 6,
                    Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000016000") }
           },
              new CategoryVM {
                   ID= Guid.Parse("F796BE27-2193-405B-AE63-000000016700"),
                   MaterializedPath= "Buyable/Swappable/Rentable.Pet.Mammals",
                   Name= "Mammals",
                   Sort= 7,
                    Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000016000") }
           },
              new CategoryVM {
                   ID= Guid.Parse("F796BE27-2193-405B-AE63-000000016800"),
                   MaterializedPath= "Buyable/Swappable/Rentable.Pet.Insects",
                   Name= "Insects",
                   Sort= 8,
                    Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000016000") }
           },
              new CategoryVM {
                   ID= Guid.Parse("F796BE27-2193-405B-AE63-000000016900"),
                   MaterializedPath= "Buyable/Swappable/Rentable.Pet.Reptiles and Amphibians",
                   Name= "Reptiles and Amphibians",
                   Sort= 9,
                    Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000016000") }
           }
           };


            foreach (var sub in petsSubCategory.ToList())
                sub.Create(false);
            #endregion

            #region RealEstate
            IList<CategoryVM> RealEstateSubCategory = new List<CategoryVM>
            { new CategoryVM {
                   ID= Guid.Parse("F796BE27-2193-405B-AE63-000000017100"),
                   MaterializedPath= "Buyable/Swappable/Rentable.Real Estate.House and Lot, Townhouses and Subdivisions",
                   Name= "House and Lot, Townhouses and Subdivisions",
                   Sort= 1,
                   Parent=new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000017000") }
            },
              new CategoryVM {
                   ID= Guid.Parse("F796BE27-2193-405B-AE63-000000017200"),
                   MaterializedPath= "Buyable/Swappable/Rentable.Real Estate.Apartment and Condominium",
                   Name= "Apartment and Condominium",
                   Sort= 2,
                   Parent=new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000017000") }
            },
              new CategoryVM {
                   ID= Guid.Parse("F796BE27-2193-405B-AE63-000000017300"),
                   MaterializedPath= "Buyable/Swappable/Rentable.Real Estate.Land and Farm",
                   Name= "Land and Farm",
                   Sort= 3,
                   Parent=new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000017000") }
            },
              new CategoryVM {
                   ID= Guid.Parse("F796BE27-2193-405B-AE63-000000017400"),
                   MaterializedPath= "Buyable/Swappable/Rentable.Real Estate.Commercial and Industrial Properties",
                   Name= "Commercial and Industrial Properties",
                   Sort= 4,
                   Parent=new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000017000") }
            },
              new CategoryVM {
                   ID= Guid.Parse("F796BE27-2193-405B-AE63-000000017500"),
                   MaterializedPath= "Buyable/Swappable/Rentable.Real Estate.Beach and Resorts",
                   Name= "Beach and Resorts",
                   Sort= 5,
                   Parent=new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000017000") }
            },
              new CategoryVM {
                   ID= Guid.Parse("F796BE27-2193-405B-AE63-000000017600"),
                   MaterializedPath= "Buyable/Swappable/Rentable.Real Estate.Memorial Lots",
                   Name= "Memorial Lots",
                   Sort= 6,
                   Parent=new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000017000") }
            }
            };


            foreach (var sub in RealEstateSubCategory.ToList())
                sub.Create(false);
            #endregion

            #region Hobbies
            IList<CategoryVM> HobbiesSubCategory =
                new List<CategoryVM>
                {
                    new CategoryVM {
                     ID= Guid.Parse("F796BE27-2193-405B-AE63-000000018100"),
                     MaterializedPath= "Buyable/Swappable/Rentable.Sports",
                     Name= "Sports",
                     Sort= 1,
                     Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000018000") }
                    },
                 new CategoryVM {
                     ID= Guid.Parse("F796BE27-2193-405B-AE63-000000018200"),
                     MaterializedPath= "Buyable/Swappable/Rentable.Collectables",
                     Name= "Collectables",
                     Sort= 2,
                     Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000018000") }
                    },
                 new CategoryVM {
                     ID= Guid.Parse("F796BE27-2193-405B-AE63-000000018300"),
                     MaterializedPath= "Buyable/Swappable/Rentable.Arts, Crafts and Scultures",
                     Name= "Arts, Crafts and Sculptures",
                     Sort= 3,
                     Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000018000") }
                    },
                 new CategoryVM {
                     ID= Guid.Parse("F796BE27-2193-405B-AE63-000000018400"),
                     MaterializedPath= "Buyable/Swappable/Rentable.Metal, Glass and Wood Works",
                     Name= "Metal, Glass and Wood Works",
                     Sort= 4,
                     Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000018000") }
                    },
                 new CategoryVM {
                     ID= Guid.Parse("F796BE27-2193-405B-AE63-000000018500"),
                     MaterializedPath= "Buyable/Swappable/Rentable.Instruments",
                     Name= "Instruments",
                     Sort= 5,
                     Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000018000") }
                    },
                 new CategoryVM {
                     ID= Guid.Parse("F796BE27-2193-405B-AE63-000000018600"),
                     MaterializedPath= "Buyable/Swappable/Rentable.Books and other Publications",
                     Name= "Books and other Publications",
                     Sort= 6,
                     Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000018000") }
                    },
                };

            foreach (var sub in HobbiesSubCategory.ToList())
                sub.Create(false);
            #endregion

            #region Professional Equipments
            IList<CategoryVM> profCategories =
               new List<CategoryVM>
               { new CategoryVM {
                     ID= Guid.Parse("F796BE27-2193-405B-AE63-000000019100"),
                     MaterializedPath= "Buyable/Swappable/Rentable.Professional Equipments.Medical",
                     Name= "Medical",
                     Sort= 1,
                     Parent = new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000019000") }
               },
                 new CategoryVM {
                     ID= Guid.Parse("F796BE27-2193-405B-AE63-000000019300"),
                     MaterializedPath= "Buyable/Swappable/Rentable.Professional Equipments.Engineering",
                     Name= "Engineering",
                     Sort= 3,
                     Parent = new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000019000") }
               },
                 new CategoryVM {
                     ID= Guid.Parse("F796BE27-2193-405B-AE63-000000019999"),
                     MaterializedPath= "Buyable/Swappable/Rentable.Professional Equipments.Others",
                     Name= "Others",
                     Sort= 99,
                     Parent = new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000019000") }
               }
               };

            foreach (var sub in profCategories.ToList())
                sub.Create(false);
            #endregion

            #region Consumer Electronics
            IList<CategoryVM> ConsumerElectronicsCategories =
               new List<CategoryVM>
               { new CategoryVM {
                     ID= Guid.Parse("F796BE27-2193-405B-AE63-200000001000"),
                     MaterializedPath= "Buyable/Swappable/Rentable.Consumer Electronics.Office and School Equipment",
                     Name= "Office and School Equipment",
                     Sort= 1,
                     Parent = new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-200000019000") }
               },
                 new CategoryVM {
                     ID= Guid.Parse("F796BE27-2193-405B-AE63-200000002000"),
                     MaterializedPath= "Buyable/Swappable/Rentable.Consumer Electronics.Audio and Video Electronics",
                     Name= "Audio and Video Electronics",
                     Sort= 2,
                     Parent = new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-200000019000") }
               },
                  new CategoryVM {
                     ID= Guid.Parse("F796BE27-2193-405B-AE63-200000003000"),
                     MaterializedPath= "Buyable/Swappable/Rentable.Consumer Electronics.CCTV and Security Products",
                     Name= "CCTV and Security Products",
                     Sort= 3,
                     Parent = new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-200000019000") }
               },
                 new CategoryVM {
                     ID= Guid.Parse("F796BE27-2193-405B-AE63-200000004000"),
                     MaterializedPath= "Buyable/Swappable/Rentable.Consumer Electronics.Video Games and Consoles",
                     Name= "Video Games and Consoles",
                     Sort= 4,
                     Parent = new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-200000019000") }
               },
                new CategoryVM {
                     ID= Guid.Parse("F796BE27-2193-405B-AE63-200000005000"),
                     MaterializedPath= "Buyable/Swappable/Rentable.Consumer Electronics.Communication devices",
                     Name= "Communication devices",
                     Sort= 5,
                     Parent = new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-200000019000") }
               },
                 new CategoryVM {
                     ID= Guid.Parse("F796BE27-2193-405B-AE63-200000007000"),
                     MaterializedPath= "Buyable/Swappable/Rentable.Consumer Electronics.Others",
                     Name= "Others",
                     Sort= 99,
                     Parent = new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-200000019000") }
               },
               };

            foreach (var sub in ConsumerElectronicsCategories.ToList())
                sub.Create(false);
            #endregion

        }

        private static void SetServices(ref ISession ses)
        {
            IList<CategoryVM> servicesCategory =
                new List<CategoryVM>
                {
                    new CategoryVM {
                         ID= Guid.Parse("F796BE27-2193-405B-AE63-000000021000"),
                         MaterializedPath= "Service.Events",
                         Name= "Events",
                         Sort= 1,
                         Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000020000") }
                    },
                    new CategoryVM {
                         ID= Guid.Parse("F796BE27-2193-405B-AE63-000000022000"),
                         MaterializedPath= "Service.Planning and Entertainment",
                         Name= "Planning and Entertainment",
                         Sort= 2,
                         Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000020000") }
                    },
                    new CategoryVM {
                         ID= Guid.Parse("F796BE27-2193-405B-AE63-000000023000"),
                         MaterializedPath= "Service.Advertising",
                         Name= "Advertising",
                         Sort= 3,
                         Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000020000") }
                    },
                    new CategoryVM {
                         ID= Guid.Parse("F796BE27-2193-405B-AE63-000000024000"),
                         MaterializedPath= "Service.Travel and Tourism",
                         Name= "Travel and Tourism",
                         Sort= 4,
                         Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000020000") }
                    },
                    new CategoryVM {
                         ID= Guid.Parse("F796BE27-2193-405B-AE63-000000025000"),
                         MaterializedPath= "Service.Recreational",
                         Name= "Recreational",
                         Sort= 5,
                         Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000020000") }
                    },
                    new CategoryVM {
                         ID= Guid.Parse("F796BE27-2193-405B-AE63-000000026000"),
                         MaterializedPath= "Service.Engineering",
                         Name= "Engineering",
                         Sort= 6,
                         Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000020000") }
                    },
                    new CategoryVM {
                         ID= Guid.Parse("F796BE27-2193-405B-AE63-000000027000"),
                         MaterializedPath= "Service.Repair and Maintenance - Gadget",
                         Name= "Repair and Maintenance - Gadget",
                         Sort= 7,
                         Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000020000") }
                    },
                    new CategoryVM {
                         ID= Guid.Parse("F796BE27-2193-405B-AE63-000000028000"),
                         MaterializedPath= "Service.Repair and Maintenance - Household",
                         Name= "Repair and Maintenance - Household",
                         Sort= 8,
                         Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000020000") }
                    },
                    new CategoryVM {
                         ID= Guid.Parse("F796BE27-2193-405B-AE63-000000029000"),
                         MaterializedPath= "Service.Repair and Maintenance - Others",
                         Name= "Repair and Maintenance - Others",
                         Sort= 9,
                         Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000020000") }
                    },
                    new CategoryVM {
                         ID= Guid.Parse("F796BE27-2193-405B-AE63-000000021100"),
                         MaterializedPath= "Service.Food and Related Products",
                         Name= "Food and Related Products",
                         Sort= 10,
                         Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000020000") }
                    },
                    new CategoryVM {
                         ID= Guid.Parse("F796BE27-2193-405B-AE63-000000021200"),
                         MaterializedPath= "Service.Animal Welfare",
                         Name= "Animal Welfare",
                         Sort= 11,
                         Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000020000") }
                    },
                    new CategoryVM {
                         ID= Guid.Parse("F796BE27-2193-405B-AE63-000000021300"),
                         MaterializedPath= "Service.Education",
                         Name= "Education",
                         Sort= 12,
                         Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000020000") }
                    },
                    new CategoryVM {
                         ID= Guid.Parse("F796BE27-2193-405B-AE63-000000021400"),
                         MaterializedPath= "Service.Medical or Relaxation",
                         Name= "Medical or Relaxation",
                         Sort= 13,
                         Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000020000") }
                    },
                    new CategoryVM {
                         ID= Guid.Parse("F796BE27-2193-405B-AE63-000000021500"),
                         MaterializedPath= "Service.Retail",
                         Name= "Retail",
                         Sort= 14,
                         Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000020000") }
                    },
                    new CategoryVM {
                         ID= Guid.Parse("F796BE27-2193-405B-AE63-000000021600"),
                         MaterializedPath= "Service.Information Technology and Computer Service",
                         Name= "Information Technology and Computer Service",
                         Sort= 15,
                         Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000020000") }
                    },
                    new CategoryVM {
                         ID= Guid.Parse("F796BE27-2193-405B-AE63-000000021700"),
                         MaterializedPath= "Service.Consulting",
                         Name= "Consulting",
                         Sort= 16,
                         Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000020000") }
                    },
                    new CategoryVM {
                         ID= Guid.Parse("F796BE27-2193-405B-AE63-000000021900"),
                         MaterializedPath= "Service.Telecommunication",
                         Name= "Telecommunication",
                         Sort= 17,
                         Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000020000") }
                    },
                    new CategoryVM {
                         ID= Guid.Parse("F796BE27-2193-405B-AE63-000000022100"),
                         MaterializedPath= "Service.Agriculture and Forestry",
                         Name= "Agriculture and Forestry",
                         Sort= 18,
                         Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000020000") }
                    },
                    new CategoryVM {
                         ID= Guid.Parse("F796BE27-2193-405B-AE63-000000022200"),
                         MaterializedPath= "Service.Marketing and Sales",
                         Name= "Marketing and Sales",
                         Sort= 19,
                         Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000020000") }
                    },
                    new CategoryVM {
                         ID= Guid.Parse("F796BE27-2193-405B-AE63-000000022300"),
                         MaterializedPath= "Service.Human Resources and Employment Agencies",
                         Name= "Human Resources and Employment Agencies",
                         Sort= 20,
                         Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000020000") }
                    },
                    new CategoryVM {
                         ID= Guid.Parse("F796BE27-2193-405B-AE63-000000022400"),
                         MaterializedPath= "Service.Publishing",
                         Name= "Publishing",
                         Sort= 21,
                         Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000020000") }
                    },
                    new CategoryVM {
                         ID= Guid.Parse("F796BE27-2193-405B-AE63-000000022500"),
                         MaterializedPath= "Service.Courier and Logistics",
                         Name= "Courier and Logistics",
                         Sort= 22,
                         Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000020000") }
                    },
                    new CategoryVM {
                         ID= Guid.Parse("F796BE27-2193-405B-AE63-000000022600"),
                         MaterializedPath= "Service.Security and Detective Agencies",
                         Name= "Security and Detective Agencies",
                         Sort= 23,
                         Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000020000") }
                    },
                    new CategoryVM {
                         ID= Guid.Parse("F796BE27-2193-405B-AE63-000000022700"),
                         MaterializedPath= "Service.Brokerage and Investment",
                         Name= "Brokerage and Investment",
                         Sort= 24,
                         Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000020000") }
                    },
                    new CategoryVM {
                         ID= Guid.Parse("F796BE27-2193-405B-AE63-000000022900"),
                         MaterializedPath= "Service.Legal and Law",
                         Name= "Legal and Law",
                         Sort= 25,
                         Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000020000") }
                    },
                    new CategoryVM {
                         ID= Guid.Parse("F796BE27-2193-405B-AE63-000000023100"),
                         MaterializedPath= "Service.Government",
                         Name= "Government",
                         Sort= 26,
                         Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000020000") }
                    },
                    new CategoryVM {
                         ID= Guid.Parse("F796BE27-2193-405B-AE63-000000023200"),
                         MaterializedPath= "Service.Casting and Auditions",
                         Name= "Casting and Auditions",
                         Sort= 27,
                         Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000020000") }
                    },
                    new CategoryVM {
                         ID= Guid.Parse("F796BE27-2193-405B-AE63-000000023300"),
                         MaterializedPath= "Service.Non-Profit",
                         Name= "Non-Profit",
                         Sort= 28,
                         Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000020000") }
                    },
                    new CategoryVM {
                         ID= Guid.Parse("F796BE27-2193-405B-AE63-000000023400"),
                         MaterializedPath= "Service.Science",
                         Name= "Science",
                         Sort= 29,
                         Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000020000") }
                    },
                    new CategoryVM {
                         ID= Guid.Parse("F796BE27-2193-405B-AE63-000000029999"),
                         MaterializedPath= "Service.Others",
                         Name= "Others",
                         Sort= 99,
                         Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000020000") }
                    }
                
                };

            foreach (var sub in servicesCategory.ToList())
                sub.Create(false);
        }

        private static void SetJobs(ref IGenerationSession auto, ref ISession ses)
        {
            IList<CategoryVM> jobsCategory = new List<CategoryVM>
            {
                new CategoryVM {
                        ID= Guid.Parse("F796BE27-2193-405B-AE63-000000031000"),
                        Name= "Accounting and Finance",
                        MaterializedPath= "Job.Accounting and Finance",
                        Sort= 1,
                        Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000030000") }
                },
                   new CategoryVM {
                        ID= Guid.Parse("F796BE27-2193-405B-AE63-000000032000"),
                        Name= "Arts/Media/Communications",
                        MaterializedPath= "Job.Arts/Media/Communications",
                        Sort= 2,
                        Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000030000") }
                },
                   new CategoryVM {
                        ID= Guid.Parse("F796BE27-2193-405B-AE63-000000033000"),
                        Name= "Building/Construction",
                        MaterializedPath= "Job.Building/Construction",
                        Sort= 3,
                        Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000030000") }
                },
                   new CategoryVM {
                        ID= Guid.Parse("F796BE27-2193-405B-AE63-000000034000"),
                        Name= "Computer/Information Technology",
                        MaterializedPath= "Job.Computer/Information Technology",
                        Sort= 4,
                        Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000030000") }
                },
                   new CategoryVM {
                        ID= Guid.Parse("F796BE27-2193-405B-AE63-000000035000"),
                        Name= "Education/Training",
                        MaterializedPath= "Job.Education/Training",
                        Sort= 5,
                        Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000030000") }
                },
                   new CategoryVM {
                        ID= Guid.Parse("F796BE27-2193-405B-AE63-000000036000"),
                        Name= "Engineering",
                        MaterializedPath= "Job.Engineering",
                        Sort= 6,
                        Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000030000") }
                },
                   new CategoryVM {
                        ID= Guid.Parse("F796BE27-2193-405B-AE63-000000037000"),
                        Name= "Healthcare",
                        MaterializedPath= "Job.HealthCare",
                        Sort= 7,
                        Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000030000") }
                },
                   new CategoryVM {
                        ID= Guid.Parse("F796BE27-2193-405B-AE63-000000038000"),
                        Name= "Hotel/Restaurant",
                        MaterializedPath= "Job.Hotel/Restaurant",
                        Sort= 8,
                        Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000030000") }
                },
                   new CategoryVM {
                        ID= Guid.Parse("F796BE27-2193-405B-AE63-000000039000"),
                        Name= "Manufacturing",
                        MaterializedPath= "Job.Manufacturing",
                        Sort= 9,
                        Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000030000") }
                },
                   new CategoryVM {
                        ID= Guid.Parse("F796BE27-2193-405B-AE63-000000031200"),
                        Name= "Sales/Marketing",
                        MaterializedPath= "Job.Sales/Marketing",
                        Sort= 10,
                        Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000030000") }
                },
                   new CategoryVM {
                        ID= Guid.Parse("F796BE27-2193-405B-AE63-000000031300"),
                        Name= "Science",
                        MaterializedPath= "Job.Science",
                        Sort= 11,
                        Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000030000") }
                },
                   new CategoryVM {
                        ID= Guid.Parse("F796BE27-2193-405B-AE63-000000031400"),
                        Name= "Services",
                        MaterializedPath= "Job.Services",
                        Sort= 12,
                        Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000030000") }
                },
                new CategoryVM {
                    ID= Guid.Parse("F796BE27-2193-405B-AE63-000000031500"),
                    Name= "Man Power",
                    MaterializedPath= "Job.Man Power",
                    Sort= 12,
                    Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000030000") }
                },
                   new CategoryVM {
                        ID= Guid.Parse("F796BE27-2193-405B-AE63-000000031600"),
                        Name= "Others",
                        MaterializedPath= "Job.Others",
                        Sort= 13,
                        Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000030000") }
                },
            };

            foreach (var sub in jobsCategory.ToList())
                sub.Create(false);
        }

        private static void SetCommunity(ref IGenerationSession auto, ref ISession ses)
        {
            IList<CategoryVM> communityCategories = new List<CategoryVM>
            { new CategoryVM {
                       ID= Guid.Parse("F796BE27-2193-405B-AE63-000000041000"),
                       Name= "Carpool",
                       MaterializedPath= "Community.Carpool",
                       Sort= 1,
                        Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000040000") }
            },
                  new CategoryVM {
                       ID= Guid.Parse("F796BE27-2193-405B-AE63-000000042000"),
                       Name= "Charity",
                       MaterializedPath= "Community.Charity",
                       Sort= 2,
                        Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000040000") }
            },
                  new CategoryVM {
                       ID= Guid.Parse("F796BE27-2193-405B-AE63-000000043000"),
                       Name= "Blood Donor",
                       MaterializedPath= "Community.Blood Donor",
                       Sort= 3,
                        Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000040000") }
            },
               new CategoryVM {
                       ID= Guid.Parse("F796BE27-2193-405B-AE63-000000044000"),
                       Name= "Business",
                       MaterializedPath= "Community.Business",
                       Sort= 4,
                        Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000040000") }
            },
             new CategoryVM {
                       ID= Guid.Parse("F796BE27-2193-405B-AE63-000000045000"),
                       Name= "Volunteer Work",
                       MaterializedPath= "Community.Volunteer Work",
                       Sort= 5,
                        Parent= new CategoryVM { ID = Guid.Parse("F796BE27-2193-405B-AE63-000000040000") }
            }};

            foreach (var sub in communityCategories.ToList())
                sub.Create(false);
        }


    }
}
