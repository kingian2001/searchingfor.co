﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using System.IO;

namespace Searchingfor.ComingSoon.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        [HttpGet]
        public ActionResult Index()
        {
            return View("~/Views/index.cshtml");
        }

        [HttpPost]
        public ActionResult Index(string email)
        {
            if (String.IsNullOrEmpty(email))
            {
                ViewBag.Message = "Please input an email";
                goto View;
            }

            var emails = Global.Emails;

            foreach (string e in emails)
            {
                if (e.Contains(email))
                {
                    ViewBag.Message = "Please input another email, this one is already taken";
                    goto View;
                }
            }

            System.IO.File.AppendAllLines(Global.EmailPath, new List<string>() { email.ToLower() + "," }, System.Text.Encoding.Default);

            ViewBag.Message = "Thank you! will see you around!";

            View:
            return View("~/Views/index.cshtml");
        }

    }
}
