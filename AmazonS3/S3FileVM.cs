﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Searchingfor.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AmazonS3
{
    public class S3FileVM
    {
        public DateTime? LastModified { get; set; }

        public DateTime? LastModifiedDate { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }

        public string Base64 { get; set; }

        public string Ext { get; set; }

        public bool Old { get; set; }

        string thumbNailPrefix = "tmb.";

        string BucketName = ConfigurationManager.AppSettings["AWSS3:BucketName"].ToString();

        string KeyName = ConfigurationManager.AppSettings["AWSS3:KeyName"].ToString();

        string DNSUrl = ConfigurationManager.AppSettings["AWSS3:CloudFrontUrl"].ToString();

        public string Save()
        {
            var fileName = Old ? Name : Name + "-" + Guid.NewGuid().ToString() + "." + Ext;

            var keyname = KeyName + fileName;

            keyname = keyname.Replace(" ", "");

            if (Base64.IsNullEmptyOrWhitespace() || Name.IsNullEmptyOrWhitespace())
                return "";

            if (Old)
                return DNSUrl + keyname;

            Byte[] bytes = Convert.FromBase64String(this.Base64);

            using (var client = new AmazonS3Client(RegionEndpoint.APSoutheast1))
            {
                using (var stream = new MemoryStream(bytes))
                {
                    // simple object put
                    PutObjectRequest request = new PutObjectRequest()
                    {
                        BucketName = BucketName,
                        Key = keyname,
                        InputStream = stream,
                        StreamTransferProgress = uploadProgress,
                    };

                    request.Headers["Cache-Control"] = "max-age=10000000";
                    request.Headers["Access-Control-Allow-Origin"] = "*";

                    PutObjectResponse response = client.PutObject(request);

                    return DNSUrl + keyname;
                }
            }
        }

        private void uploadProgress(object sender, Amazon.Runtime.StreamTransferProgressArgs e)
        {

        }

        /// <summary>
        /// save all files then return urls with '|' delimited
        /// </summary>
        /// <param name="files">the files</param>
        /// <returns>urls '|' delimited</returns>
        public static string SaveAll(List<S3FileVM> files)
        {
            if (files != null && files.Count > 0)
            {
                var urls = string.Empty;
                foreach (var file in files)
                    urls += file.Save() + Constants.Delimiter.ToString();

                urls = urls.Trim(Constants.Delimiter);
                return urls;
            }
            return "";
        }
    }
}
