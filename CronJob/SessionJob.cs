﻿using Quartz;
using Searchingfor.DataAccess;
using Searchingfor.DataAccess.Repositories;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CronJob
{
   public class SessionJob: IJob
    {

       public void Execute(IJobExecutionContext context)
       {
           try
           {
               Debug.WriteLine("execute ran");
               var start = DateTime.Now;
               Instance instance = new Instance();
               instance.SetSession();
               instance.RepositoryFactory<DatabaseSettingsRepository>().StopIdle();
               instance.Dispose();
               var end = DateTime.Now;
               Debug.WriteLine((end - start).Milliseconds);
           }
           catch { }
       }
    }
}
