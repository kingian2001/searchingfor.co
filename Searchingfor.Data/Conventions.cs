﻿using FluentNHibernate.Conventions;
using FluentNHibernate.Conventions.AcceptanceCriteria;
using FluentNHibernate.Conventions.Inspections;
using FluentNHibernate.Conventions.Instances;
using System;
using System.Reflection;

namespace Searchingfor.Data
{
    public class GeneralConventions : IPropertyConvention, IPropertyConventionAcceptance
    {
        public void Accept(IAcceptanceCriteria<IPropertyInspector> criteria)
        {
            criteria.Expect(x => !String.IsNullOrEmpty(x.Type.Name));
        }

        public void Apply(IPropertyInstance instance)
        {
        }
    }

    public class Caching : IClassConvention
    {
        public void Apply(IClassInstance instance)
        {
            //instance.Table(instance.EntityType.Name);
            instance.Not.LazyLoad();
            //instance.Cache.ReadWrite();
        }
    }

    /// <summary>
    /// Makes the property string to a range from 0 to N
    /// </summary>
    public class StringColumnLengthConvention : IPropertyConvention, IPropertyConventionAcceptance
    {
        public void Accept(IAcceptanceCriteria<IPropertyInspector> criteria) { criteria.Expect(x => x.Type == typeof(string)).Expect(x => x.Length == 0); }

        public void Apply(IPropertyInstance instance)
        {
            int leng = 255;

            MemberInfo[] myMemberInfos = ((PropertyInstance)(instance)).EntityType.GetMember(instance.Name);
            if (myMemberInfos.Length > 0)
            {
                object[] myCustomAttrs = myMemberInfos[0].GetCustomAttributes(false);
                foreach (var attr in myCustomAttrs)
                {
                    if (attr is Searchingfor.Entity.StringLength)
                    {
                        leng = ((Searchingfor.Entity.StringLength)(attr)).Length;
                    }
                }
            }

            instance.Length(leng);
        }
    }

    /// <summary>
    /// Makes the property of type 'text' 
    /// </summary>
    public class StringColumnLengtMaxhConvention : IPropertyConvention, IPropertyConventionAcceptance
    {
        public void Accept(IAcceptanceCriteria<IPropertyInspector> criteria)
        {
            criteria.Expect(x => x.Type == typeof(string)).Expect(x => x.Type == typeof(string));
        }

        public void Apply(IPropertyInstance instance)
        {
            int leng = 255;

            MemberInfo[] myMemberInfos = ((PropertyInstance)(instance)).EntityType.GetMember(instance.Name);
            if (myMemberInfos.Length > 0)
            {
                object[] myCustomAttrs = myMemberInfos[0].GetCustomAttributes(false);
                foreach (var attr in myCustomAttrs)
                {
                    if (attr is Searchingfor.Entity.StringLengthMax)
                    {
                        leng = 40000;
                    }
                }
            }

            instance.Length(leng);
        }
    }

    /// <summary>
    /// Makes the property of the Entity not nullable
    /// </summary>
    public class NotNullableConvention : IPropertyConvention, IPropertyConventionAcceptance
    {
        public void Accept(IAcceptanceCriteria<IPropertyInspector> criteria) { criteria.Expect(x => !String.IsNullOrEmpty(x.Type.Name)); }

        public void Apply(IPropertyInstance instance)
        {
            MemberInfo[] myMemberInfos = ((PropertyInstance)(instance)).EntityType.GetMember(instance.Name);
            if (myMemberInfos.Length > 0)
            {
                object[] myCustomAttrs = myMemberInfos[0].GetCustomAttributes(false);
                foreach (var attr in myCustomAttrs)
                {
                    if (attr is Searchingfor.Entity.NotNullable)
                    {
                        instance.Not.Nullable();
                    }
                }
            }
        }
    }

    /// <summary>
    /// Makes the property of the Entity nullable
    /// </summary>
    public class NullableConvention : IPropertyConvention, IPropertyConventionAcceptance
    {
        public void Accept(IAcceptanceCriteria<IPropertyInspector> criteria) { criteria.Expect(x => !String.IsNullOrEmpty(x.Type.Name)); }

        public void Apply(IPropertyInstance instance)
        {
            MemberInfo[] myMemberInfos = ((PropertyInstance)(instance)).EntityType.GetMember(instance.Name);
            if (myMemberInfos.Length > 0)
            {
                object[] myCustomAttrs = myMemberInfos[0].GetCustomAttributes(false);
                foreach (var attr in myCustomAttrs)
                {
                    if (attr is Searchingfor.Entity.Nullable)
                    {
                        instance.Nullable();
                    }
                }
            }
        }

    }
}
