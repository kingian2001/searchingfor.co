﻿using NHibernate;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace Searchingfor.Data
{
    public class SqlStatementInterceptor : EmptyInterceptor
    {
        public override NHibernate.SqlCommand.SqlString OnPrepareStatement(NHibernate.SqlCommand.SqlString sql)
        {
            //var fileName = "Additional Queries " + DateTime.Now.Month + "~" + DateTime.Now.Day + "~" + DateTime.Now.Year + ".txt";

            //using (FileStream fs = new FileStream(new FileInfo(fileName).FullName, FileMode.Append, FileAccess.Write))
            //using (StreamWriter sw = new StreamWriter(fs))
            //{
            //    sw.WriteLine(sql.ToString());
            //}

            return sql;
        }
    }
}
