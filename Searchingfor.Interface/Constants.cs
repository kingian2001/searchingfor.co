﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Searchingfor.Common
{
    /// <summary>
    /// Constants should be placed here
    /// </summary>
    public static class Constants
    {
        public static char Delimiter = '|';

        public static List<string> ReviewType = new List<string>
        {
            "Seller","Service Provider","Employer"
        };

        public static List<string> BuySwapRent = new List<string>
        {
            "Buy","Swap","Rent","Sell"
        };

        public static List<string> JobTypes = new List<string>
        {
            "Full-Time","Part-Time","Remote","Intern","Freelance"
        };

        public static List<string> NotificationType = new List<string>
        {
            "Announcements","Offers","Payments","Ratings","Replies","Keywords","Premiums"
        };

        public static List<string> PremiumTypes = new List<string>
        {
            "Feature Comment","Feature Ad","Keyword Slot"
        };

        public static List<string> ConditionTypes = new List<string>
        {
            "Used","New"
        };

        public enum AuditLogType
        {
            Post,
            Register,
            Comment,
            ModifyAccount,
            EmailVerification,
            SMSVerification,
            Review,
            PremiumBuy,
            PremiumExtend,
            PostModify
        }

        public static class PostStatus
        {
            public static string ACTIVE = "active";
            public static string REPORTED = "reported";
            public static string EXPIRED = "expired";
        }


        //public static class NotificationType
        //{
        //    public static string Announcements = "Announcements";
        //    public static string Offers = "Offers";
        //    public static string Payments = "Payments";
        //    public static string Ratings = "Ratings";
        //    public static string Reports = "Reports";
        //    public static string Posts = "Posts";
        //    public static string Bids = "Bids";
        //}
    }
}
