﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Searchingfor.Entity.Entities
{
    public class KeywordUser
    {
        [NotNullable]
        public virtual Guid ID { get; set; }

        [NotNullable]
        public virtual Keyword Keyword { get; set; }

        [NotNullable]
        public virtual User User { get; set; }

        public virtual string Country { get; set; }

        public virtual string City { get; set; }

        public virtual string Status { get; set; }
    }
}
