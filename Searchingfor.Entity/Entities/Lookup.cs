﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Searchingfor.Entity.Entities
{
    public class Lookup
    {
        [NotNullable]
        public virtual Guid ID { get; set; }
        [NotNullable]
        public virtual string GroupName { get; set; }
        [NotNullable]
        public virtual string Name { get; set; }

        public virtual string SafeName { get; set; }
        [NotNullable]
        public virtual int Sort { get; set; }
    }
}
