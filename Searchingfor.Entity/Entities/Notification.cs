﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Searchingfor.Entity.Entities
{
    public class Notification
    {
        [NotNullable]
        public virtual Guid ID { get; set; }
        [NotNullable]
        public virtual string Type { get; set; }

        public virtual User User { get; set; }

        [NotNullable]
        public virtual string Title { get; set; }

        [StringLengthMax]
        public virtual string Description { get; set; }

        public virtual string Link { get; set; }

        public virtual bool Read { get; set; }

        public virtual DateTime DateTime { get; set; }
    }
}
