﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Searchingfor.Entity.Entities
{
    public class Premium
    {
        [NotNullable]
        public virtual Guid ID { get; set; }

        [NotNullable]
        public virtual User User { get; set; }

        /// <summary>
        /// Keywords slot premium
        /// </summary>
        public virtual KeywordUser KeywordUser { get; set; }

        /// <summary>
        /// for Post feature ad
        /// </summary>
        public virtual Post Post { get; set; }

        /// <summary>
        /// Comment Feature Ad, Post Feature Ad, Keyword Slot
        /// </summary>
        public virtual string Type { get; set; }

        /// <summary>
        /// cost of the premium
        /// </summary>
        public virtual int Cost { get; set; }

        public virtual string Value { get; set; }

        /// <summary>
        /// duration of premium
        /// </summary>
        public virtual int DurationInDays { get; set; }

        public virtual DateTime StartDate { get; set; }

        public virtual DateTime ExpirationDate { get; set; }
    }
}
