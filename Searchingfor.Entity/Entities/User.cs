﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Searchingfor.Entity.Entities
{
    public class User
    {
        [NotNullable]
        public virtual Guid ID { get; set; }
        [NotNullable]
        public virtual string Type { get; set; }
        [StringLength(50)]
        public virtual string Hash { get; set; }
        [NotNullable, StringLength(100)]
        public virtual string Name { get; set; }
        [NotNullable, StringLength(60)]
        public virtual string Email { get; set; }
        [StringLength(60)]
        public virtual string Mobile { get; set; }

        public virtual string Country { get; set; }

        public virtual string City { get; set; }

        public virtual string ImageUrl { get; set; }

        [StringLength(20)]
        public virtual string Status { get; set; }

        public virtual int Credits { get; set; }

        public virtual bool EmailVerified { get; set; }

        public virtual bool MobileVerified { get; set; }
    }
}
