﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Searchingfor.Entity.Entities
{
    public class AuditLog
    {
        [NotNullable]
        public virtual Guid ID { get; set; }

        public virtual User User { get; set; }

        [NotNullable]
        public virtual string Message { get; set; }

        public virtual string Others { get; set; }

        /// <summary>
        /// any json object.
        /// </summary>
        public virtual string JSON { get; set; }

        [NotNullable]
        public virtual DateTime DateTime { get; set; }

        public virtual int Type { get; set; }
    }
}
