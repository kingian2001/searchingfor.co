﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Searchingfor.Entity.Entities
{
    public class Contact
    {
        [NotNullable]
        public virtual Guid ID { get; set; }

        [NotNullable]
        public virtual string Type { get; set; }

        public virtual User User { get; set; }

        public virtual string Name { get; set; }
        
        public virtual string Email { get; set; }

        public virtual string Message { get; set; }

        public virtual DateTime DateCreated { get; set; }
    }
}
