﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Searchingfor.Entity.Entities
{
    public class CarpoolPost
    {
        [NotNullable]
        public virtual Guid ID { get; set; }
        [StringLength(150)]
        public virtual string Title { get; set; }
        [StringLengthMax]
        public virtual string Description { get; set; }
        [StringLengthMax]
        public virtual string OtherDescription { get; set; }
        [StringLength(100)]
        public virtual string FromLocation { get; set; }
        [StringLength(100)]
        public virtual string ToLocation { get; set; }
        [NotNullable]
        public virtual int Urgency { get; set; }
        [StringLength(10)]
        public virtual string LinkPostfix { get; set; }
        public virtual User Owner { get; set; }
        [NotNullable]
        public virtual DateTime CreatedOn { get; set; }
        [NotNullable]
        public virtual DateTime ModifiedOn { get; set; }
    }
}
