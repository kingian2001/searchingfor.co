﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Searchingfor.Entity.Entities
{
    public class Review
    {
        [NotNullable]
        public virtual Guid ID { get; set; }
        [NotNullable]
        public virtual string Type { get; set; }
        [NotNullable]
        public virtual User Reviewer { get; set; }
        [NotNullable]
        public virtual User Reviewee { get; set; }
        [NotNullable]
        public virtual double Rating { get; set; }
        [NotNullable, StringLength(150)]
        public virtual string Description { get; set; }

        public virtual DateTime DateCreated { get; set; }
    }
}
