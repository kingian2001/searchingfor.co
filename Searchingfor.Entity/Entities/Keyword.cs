﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Searchingfor.Entity.Entities
{
    public class Keyword
    {
        [NotNullable]
        public virtual Guid ID { get; set; }

        [NotNullable]
        public virtual string Word { get; set; }
    }
}
