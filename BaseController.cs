﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Searchingfor.Controllers
{
    public class BaseController<TViewModel> : ApiController where TViewModel : class, new()
    {
        public TViewModel viewModel = new TViewModel();

        public string status = "OK";
        public string message = "";

        public BaseController()
        {
        }

        public TViewModel Repository
        {
            get
            {
                if (viewModel == null)
                {
                    viewModel = new TViewModel();
                }

                return viewModel;
            }
        }

        /// <summary>
        /// deserialize an object to a dictionary
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public Dictionary<string, object> Deserialize(object p)
        {
            return JsonConvert.DeserializeObject<Dictionary<string, object>>(p.ToString());
        }

        /// <summary>
        /// deserialize an object to a dictionary then remove nulls
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public Dictionary<string, object> DeserializeThenRemoveNulls(object p)
        {
            var result = JsonConvert.DeserializeObject<Dictionary<string, object>>(p.ToString());

            foreach (var item in result.Where(item => item.Value == null).ToList())
                result.Remove(item.Key);

            return result;
        }
    }
}