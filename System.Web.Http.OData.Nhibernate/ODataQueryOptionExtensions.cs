﻿using System.ComponentModel;
using System.Web.OData.Query;
using NHibernate;
using System.Text.RegularExpressions;
using Utilities;
using System.Collections.Generic;
namespace System.Web.OData.NHibernate
{
    [EditorBrowsable(EditorBrowsableState.Never)]
    public static class ODataQueryOptionExtensions
    {
        /// <summary>
        /// Apply odataqueryoptions to nhibernate session
        /// </summary>
        /// <param name="query">QueryOptions</param>
        /// <param name="TableName">the table name</param>
        /// <param name="session">the session</param>
        /// <param name="filteronly">if filter is to be applied</param>
        /// <returns></returns>
        public static IQuery ApplyTo(this ODataQueryOptions query, string TableName, ISession session, bool filteronly = false)
        {
            string from = "from " + TableName + " $it" + Environment.NewLine;

            string orderBy = string.Empty;

            // convert $filter to HQL where clause.
            WhereClause where = ToFilterQuery(query.Filter);

            // convert $orderby to HQL orderby clause.
            if (!filteronly)
                orderBy = ToOrderByQuery(query.OrderBy);

            // create a query using the where clause and the orderby clause.
            string queryString = from + where.Clause + orderBy;
            IQuery hQuery = session.CreateQuery(queryString);
            for (int i = 0; i < where.PositionalParameters.Length; i++)
            {
                hQuery.SetParameter(i, where.PositionalParameters[i]);
            }

            // Apply $skip.
            if (!filteronly)
                hQuery = hQuery.Apply(query.Skip);

            // Apply $top.
            if (!filteronly)
                hQuery = hQuery.Apply(query.Top);

            return hQuery;
        }

        /// <summary>
        /// Count based on filter. excluding skip and top
        /// </summary>
        /// <param name="query"></param>
        /// <param name="TableName"></param>
        /// <param name="session"></param>
        /// <returns></returns>
        public static int Count(this ODataQueryOptions query, string TableName, ISession session)
        {
            WhereClause where = query.GetFilterString();

            string from = "select Count(*) from " + TableName + " $it" + Environment.NewLine;

            string querystring = from + where.Clause;

            IQuery q = session.CreateQuery(querystring);

            for (int i = 0; i < where.PositionalParameters.Length; i++)
            {
                q.SetParameter(i, where.PositionalParameters[i]);
            };

            return int.Parse(q.UniqueResult().ToString());
        }

        /// <summary>
        /// Apply odataqueryoptions to nhibernate session
        /// </summary>
        /// <param name="query">QueryOptions</param>
        /// <param name="session">the session</param>
        /// <param name="filteronly">if filter is to be applied</param>
        /// <returns></returns>
        public static IQuery ApplyTo(this ODataQueryOptions query, ISession session, bool filteronly = false)
        {
            string from = "from " + query.Context.ElementClrType.Name + " $it" + Environment.NewLine;

            string orderBy = string.Empty;
            WhereClause where = null;
            List<string> whr = new List<string>();
            //substringof
            //Regex regex = new Regex(@"substringof\(\w+,.\w+.\)");
            //Match match = regex.Match(query.RawValues.Filter ?? "");

            MatchCollection matches = Regex.Matches(query.RawValues.Filter ?? "", @"substringof\(\w+,.\w+.\)");
            if (matches.Count > 0)
            {
                foreach (Match match in matches)
                {
                    var propertyname = match.Value.Replace("substringof(", "").Split(',')[0].Replace(",", "");
                    var value = match.Value.Replace("substringof(", "").Split(',')[1].ReplaceStrings("", ",", "\"", ")", "'");
                    whr.Add(" lower(" + propertyname + ") like '%" + value + "%' ");
                }
            }
            else
            {
                // convert $filter to HQL where clause.
                where = ToFilterQuery(query.Filter);
            }
            // convert $orderby to HQL orderby clause.
            if (!filteronly)
                orderBy = ToOrderByQuery(query.OrderBy);

            // create a query using the where clause and the orderby clause.
            string queryString = from + (where == null ? "" : where.Clause) + (whr.Count <= 0 ? "" : " where " + string.Join("and", whr)) + orderBy;
            IQuery hQuery = session.CreateQuery(queryString);

            if (where != null)
                for (int i = 0; i < where.PositionalParameters.Length; i++)
                {
                    hQuery.SetParameter(i, where.PositionalParameters[i]);
                }

            // Apply $skip.
            if (!filteronly)
                hQuery = hQuery.Apply(query.Skip);

            // Apply $top.
            if (!filteronly)
                hQuery = hQuery.Apply(query.Top);

            return hQuery;
        }

        public static WhereClause GetFilterString(this ODataQueryOptions query)
        {
            // convert $filter to HQL where clause.
            WhereClause where = ToFilterQuery(query.Filter);
            return where;
        }



        private static IQuery Apply(this IQuery query, TopQueryOption topQuery)
        {
            if (topQuery != null)
            {
                query = query.SetMaxResults(topQuery.Value);
            }
            return query;
        }

        private static IQuery Apply(this IQuery query, SkipQueryOption skipQuery)
        {
            if (skipQuery != null)
            {
                query = query.SetFirstResult(skipQuery.Value);
            }

            return query;
        }

        private static string ToOrderByQuery(OrderByQueryOption orderByQuery)
        {
            return NHibernateOrderByBinder.BindOrderByQueryOption(orderByQuery);
        }

        private static WhereClause ToFilterQuery(FilterQueryOption filterQuery)
        {
            return NHibernateFilterBinder.BindFilterQueryOption(filterQuery);
        }
    }
}
