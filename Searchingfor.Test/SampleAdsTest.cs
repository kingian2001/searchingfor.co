﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Searchingfor.Core.Models;
using Searchingfor.Core.Enums;
using System.Threading;

namespace Searchingfor.Test
{
    [TestClass]
    public class SampleAdsTest
    {
        [TestMethod]
        public void SampleAds()
        {
            var categories = new CategoryVM().GetAll();
            var owners = new UserVM().GetAll();
            var index = 0;
            for (int i = 0; i < 3000; i++)
            {

                var category = categories[new Random().Next(0, categories.Count - 1)];
                var owner = owners[new Random().Next(0, owners.Count - 1)];

                PostVM post1 = new PostVM
                {
                    Category = category,
                    Title = "Sample testing good" + index++,
                    Description = @"Sample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample DescriptionSample Description",
                    BudgetMin = new Random().Next(0, 200000),
                    BudgetMax = new Random().Next(200000, 400000),
                    Urgency = (Urgency)new Random().Next(0, 3),
                    Owner = owner
                };
                if (category.MaterializedPath.Contains("Buyable/Swappable/Rentable"))
                    post1.TypeList = new System.Collections.Generic.List<string> { "Buy", "Swap", "Rent" };
                if (category.MaterializedPath.Contains("Job."))
                    post1.TypeList = new System.Collections.Generic.List<string> { "Part-Time" };
                post1.Condition = "Used";
                post1.Create();
            }
        }
    }
}
