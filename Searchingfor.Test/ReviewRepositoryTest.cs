﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Searchingfor.DataAccess;
using Searchingfor.DataAccess.Repositories;

namespace Searchingfor.Test
{
    [TestClass]
    public class ReviewRepositoryTest
    {
        [TestMethod]
        public void TestSpeed()
        {
            Instance instance = new Instance();
            instance.SetSession();
            var repo = instance.RepositoryFactory<ReviewRepository>();
            var repoResult = repo.Find(x=>x.Rating>0);
            int d1 = DateTime.Now.Millisecond;
            var ggw = repo.TrySomething();
            int d2 = DateTime.Now.Millisecond;
            var q = (d2 - d1);
            int d3 = DateTime.Now.Millisecond;
            var ew = repo.GetAll();
            int d4 = DateTime.Now.Millisecond;
            var q2 = (d4 - d3);
        }

        [TestMethod]
        public void Test()
        {
            Instance instance = new Instance();
            instance.SetSession();
            var repo = instance.RepositoryFactory<PostRepository>();
            var w = repo.GetField(x=>x.Notify==true,x=>x.Title);
        }
    }
}
