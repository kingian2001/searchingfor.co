﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Searchingfor.DataAccess;
using Searchingfor.DataAccess.Repositories;
using Searchingfor.Core.Models;
using Searchingfor.Core.ViewModels;
using System.Threading;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.OData.Query;
using Searchingfor.Entity.Entities;

namespace Searchingfor.Test
{
    [TestClass]
    public class NhibernateTest
    {
        [TestMethod]
        public void instance_test()
        {
            Instance instance = new Instance();
            instance.SetSession();
            var q = instance.RepositoryFactory<UserRepository>();

            Assert.IsNotNull(q);
        }

        [TestMethod]
        public void pool_connection_test()
        {
            for (int i = 0; i < 100; i++)
            {
                new Thread(() =>
                {
                    //Thread.CurrentThread.IsBackground = true;
                    /* run your code here */
                    CountryVM vm = new CountryVM();
                    vm.GetAllActive();
                }).Start();

            }
        }

        [TestMethod]
        public void session_distribution_test()
        {
            Instance instance = new Instance();
            for (int i = 0; i < 2; i++)
            {
                //instance.SetSession();
                ////instance.Dispose();
                //List<AuditLogVM> list = new List<AuditLogVM>()
                //    {
                //       new AuditLogVM{
                //           Message="test",
                //           Others="test",
                //           Type=2
                //       },
                //       new AuditLogVM{
                //           Message="test2 ",
                //           Others="test2 ",
                //           Type=2
                //       }
                //    };
                var w = new AuditLogVM().Search(1);
                //list.ForEach(x => x.Create(false));

                //instance.SaveSession();
                instance.Dispose();
            }

        }

        Action sesAction;


        //[TestMethod]
        //public void activator_vs_ExpressionCompile_speed_test()
        //{
        //    int d1 = DateTime.Now.Millisecond;
        //    for (int i = 0; i < 100000; i++)
        //    {
        //        var instancez = Activator.CreateInstance(typeof(UserRepository));
        //    }
        //    int d2 = DateTime.Now.Millisecond;
        //    var q = (d2 - d1);
        //    int d3 = DateTime.Now.Millisecond;
        //    for (int i = 0; i < 100000; i++)
        //    {
        //        UserRepository me = New<UserRepository>.Instance();
        //    }
        //    int d4 = DateTime.Now.Millisecond;
        //    var q2 = (d4 - d3);
        //}
    }
}
