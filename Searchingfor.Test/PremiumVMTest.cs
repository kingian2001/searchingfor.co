﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Searchingfor.DataAccess;
using Searchingfor.DataAccess.Repositories;
using Searchingfor.Core.Models;
using Searchingfor.Core.ViewModels;

namespace Searchingfor.Test
{
    [TestClass]
    public class PremiumVMTest
    {
        [TestMethod]
        public void premiumVM_create()
        {
            var premium = new PremiumVM();
            premium.User = new UserVM { ID = Guid.Parse("01857276-c49f-4dd0-94c5-73077fe81b2e") };
            premium.SelectedPricing = new PremiumPricingVM
            {
                Type = "Feature Ad",
                Label = "15 Days"
            };

            premium.Create();
        }

        [TestMethod]
        public void premiumVM_post_create()
        {

            var premium = new PremiumVM();
            premium.Post = new PostVM { ID = Guid.Parse("4bc62e04-80ea-415b-9809-bdccd63d5766") };
            premium.User = new UserVM { ID = Guid.Parse("01857276-c49f-4dd0-94c5-73077fe81b2e") };
            premium.SelectedPricing = new PremiumPricingVM
            {
                Type = "Feature Ad",
                Label = "15 Days"
            };

            premium.Create();
        }

        [TestMethod]
        public void premiumVM_extend()
        {
            var premium = new PremiumVM();
            premium.ID = Guid.Parse("31acec50-6242-48e1-b889-d906251aa4b6");
            premium.User = new UserVM { ID = Guid.Parse("01857276-c49f-4dd0-94c5-73077fe81b2e") };
            premium.SelectedPricing = new PremiumPricingVM
            {
                Type = "Feature Comment",
                Label = "15 Days"
            };

            premium.Extend();
        }
    }
}
