﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Searchingfor.UI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            //routes.MapRoute(
            //    name: "Default",
            //    url: "{controller}/{action}/{id}",
            //    defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            //);

            //paired with $locationProvider.html5Mode(true); on angularJS 1.5
            routes.MapRoute(
               name: "AngularHtml5Route",
               url: "{*url}",
               defaults: new { controller = "Home", action = "Index" });

            routes.MapRoute(
                "Default",                                              // Route name
                "sitemap",                           // URL with parameters
                new { controller = "Sitemap", action = "Index", id = "" } , // Parameter defaults
                new string[] { "Searchingfor.UI.Controllers" }
            );
        }
    }
}