﻿using Newtonsoft.Json;
using Searchingfor.Core.Models;
using Searchingfor.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Http;
using System.IO;
using System.Web.Script.Serialization;
using Searchingfor.Entity.Entities;
using Utilities.Container;
using System.Net;
using System.Text;
using System.Net.Http;
using System.Collections.Specialized;
using System.Net.Http.Headers;
using Utilities;
using Searchingfor.UI.Utilities;
using Searchingfor.UI.Models;
using System.Threading.Tasks;

namespace Searchingfor.UI.Controllers
{
    public class PostController : BaseController<PostVM>
    {
        [HttpGet]
        public IHttpActionResult Get(string identifier)
        {
            try
            {
                var result = viewModel.Get(identifier);

                if (result == null)
                    throw new NotFoundException();

                return Ok(new { result = result });
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch (NotFoundException e)
            {
                return NotFound();
            }
            catch (Exception e)
            {
                log.Error(e.Message, e);
                return InternalServerError(new Exception("There was an internal server error, unable to save."));
            }
        }

        [HttpGet]
        public IHttpActionResult SearchWithPremium(System.Web.OData.Query.ODataQueryOptions<Post> options)
        {
            try
            {
                SearchResultOld<List<PostVM>> SearchResult = ViewModel.Search(options, true);

                return Ok(new
                {
                    result = JsonConvertHelper.SerializeObjectNoNulls(SearchResult)
                });
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                log.Error(e.Message, e);
                return InternalServerError(new Exception("There was an internal server error, unable to save."));
            }
        }

        [HttpGet]
        public IHttpActionResult Search(System.Web.OData.Query.ODataQueryOptions<Post> options)
        {
            try
            {
                SearchResultOld<List<PostVM>> SearchResult = ViewModel.Search(options, false);

                return Ok(new
                {
                    result = JsonConvertHelper.SerializeObjectNoNulls(SearchResult)
                });
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                log.Error(e.Message, e);
                return InternalServerError(new Exception("There was an internal server error, unable to save."));
            }
        }

        [HttpGet]
        public IHttpActionResult SearchFilter(System.Web.OData.Query.ODataQueryOptions<Post> options)
        {
            try
            {
              var searchRequest =   ConvertToSearchRequest<Post>(options);

                SearchResultOld<List<PostVM>> SearchResult = ViewModel.SearchFilter(searchRequest, false);

                return Ok(new
                {
                    result = JsonConvertHelper.SerializeObjectNoNulls(SearchResult)
                });
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                log.Error(e.Message, e);
                return InternalServerError(new Exception("There was an internal server error, unable to save."));
            }
        }


        [HttpGet]
        public async Task<IHttpActionResult> SearchFeatured(int Top = 10,bool selling=false, string country ="", string city = "")
        {
            try
            {
                SearchResultOld<List<PostVM>> SearchResult = ViewModel.SearchFeatured(Top, country, city,selling);

                return Ok(new
                {
                    result = JsonConvertHelper.SerializeObjectNoNulls(SearchResult)
                });
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                log.Error(e.Message, e);
                return InternalServerError(new Exception("There was an internal server error, unable to save."));
            }
        }

        [HttpGet]
        [CacheWebApi(Duration = 1, CacheTo = "CachedPostSearch")]
        public IHttpActionResult CacheSearch(System.Web.OData.Query.ODataQueryOptions<Post> options)
        {
            try
            {
                SearchResultOld<List<PostVM>> SearchResult = ViewModel.Search(options, false);

                return Ok(new
                {
                    result = JsonConvertHelper.SerializeObjectNoNulls(SearchResult)
                });
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                log.Error(e.Message, e);
                return InternalServerError(new Exception("There was an internal server error, unable to save."));
            }
        }

        [HttpPost]
        public IHttpActionResult Create(PostVM post)
        {
            try
            {
              //  string token = "";
                post.BroadCast = Notify;
                post.SendKeywordEmail = SendKeywordMatchEmail;
                PostVM values = post.Create();
                if(post.Owner.NewUser)
                {
                    //send email to the user
                    EmailVM emailVM = new EmailVM(log);
                    emailVM.Subject = "Email Verification";
                    emailVM.Recipient = post.Owner.Email;
                    string validationLink = Application.getAppPath() + "VerifiedEmail/" + post.Owner.ID;
                    String body = "<p>Good day " + post.Owner.Name + "</p><br/>"
                   + "<p> Thank you for your registration. We will make sure to provide you the best</p>"
                   + "<p> service we can. To continue, please click the link below.</p>"
                   + @"<br><a target""_blank"" href=" + validationLink + ">Click this link to verify your email</a>"
                   + "<br/><br/>Sincerly,<br/>Searchingfor.co";
                    emailVM.HtmlBody = body;//
                    emailVM.SendEmail();


                }
                //if new user. get token
                //if (!post.Owner.NewUser)
                //{
                //    string uri = Request.RequestUri.Scheme + "://" + Request.RequestUri.Authority + "/api/token";

                //    var strContent = "grant_type=password&username=" + post.Owner.Email + "&password=" + post.Owner.Password;

                //   // token = Token.Get(uri, strContent);
                //}
               

                return Ok(new
                {
                    result = JsonConvertHelper.SerializeObjectNoNulls(values),
                    //token = token
                });
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                log.Error(e.Message, e);
                return InternalServerError(new Exception("There was an internal server error, unable to save."));
            }
        }
        
        public Action<string, string, string, string> SendKeywordMatchEmail = (Recipient, Title, Name, Keyword) =>
        {
            EmailVM emailVM = new EmailVM();
            emailVM.Subject = "Email Verification";
            emailVM.Recipient = Recipient;
           // string validationLink = PostLink;
            String body = "<p>Good day " + Name + "</p><br/>"
           + "<p> There is a new post with a title "+Title+" that matches your keyword "+Keyword+"</p>"
           + "<p> Login to your searchingfor account and go to notification to view the post</p>"
           + "<br/><br/>Sincerly,<br/>Searchingfor.co";
            emailVM.HtmlBody = body;//
            emailVM.SendEmail();
        };
        [HttpPost, Authorize]
        public IHttpActionResult Reactivate([FromUri] Guid postID)
        {
            try
            {
                bool values = viewModel.Reactivate(postID);

                return Ok(new
                {
                    result = values
                });
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                log.Error(e.Message, e);
                return InternalServerError(new Exception("There was an internal server error, unable to save."));
            }
        }

        [HttpPost, Authorize]
        public IHttpActionResult Update(PostVM post)
        {
            try
            {
                PostVM values = post.Update();

                return Ok(new
                {
                    result = JsonConvertHelper.SerializeObjectNoNulls(values)
                });
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                log.Error(e.Message, e);
                return InternalServerError(new Exception("There was an internal server error, unable to save."));
            }
        }


        [HttpGet]
        public IHttpActionResult IsLinkPostfixUnique(string postfix)
        {
            try
            {
                bool result = ViewModel.IsLinkPostfixUnique(postfix);

                return Ok(new
                {
                    result = result
                });
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                log.Error(e.Message, e);
                return BadRequest("Unable to check if link shortcut is unique, please try again later.");
            }
        }



        #region unused
        /// <summary>
        /// not used
        /// </summary>
        async void SaveFile()
        {
            //if (!Request.Content.IsMimeMultipartContent())
            //{
            //    this.Request.CreateResponse(HttpStatusCode.UnsupportedMediaType);
            //}

            //var provider = null;// GetMultipartProvider();

            //var result = await Request.Content.ReadAsMultipartAsync(provider);

            ////var fileUploadObj = GetFormData<TViewModel>(provider);

            //foreach (MultipartFileData fileData in provider.FileData)
            //{
            //    try
            //    {
            //        //returns file name and view model
            //        //action(uploadedFileName = fileData.LocalFileName.Replace
            //        //    (rootFolder, ""), fileUploadObj);
            //    }
            //    catch
            //    { }//Do not remove - ramil
            //}
        }

        /// <summary>
        ///not used
        /// </summary>
        /// <returns></returns>
        //MultipartFormDataStreamProvider GetMultipartProvider()
        //{
        //    Directory.CreateDirectory(rootFolder);

        //    return new MultipartFormDataStreamProvider(rootFolder);
        //}

        #endregion
    }
}
