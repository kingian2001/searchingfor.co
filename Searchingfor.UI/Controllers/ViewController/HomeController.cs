﻿using NReco.PhantomJS;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Utilities;
using Utilities.Web.ActionResult;
using Utilities.Web.SEO;
using System.Net;
using System.Net.Http;

namespace Searchingfor.UI.Controllers.ViewController
{
    public class HomeController : BaseMVCController
    {
        /// <summary>
        /// Main controller action to show app
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            if (HasEscapedFragment() ||
                (Request.UserAgent != null &&
                (Request.UserAgent.Contains("facebookexternalhit") ||
                Request.UserAgent.Contains("Twitterbot") ||
                 Request.UserAgent.Contains("Pinterest") ||
                 Request.UserAgent.Contains("Google"))))
                return SetupSEOResult();


            //if (RequestUriAbsolutePathStartsWith("/ADMIN"))
            //   return View("~/Views/AdminIndex.cshtml");

            if (RequestUriAbsolutePathStartsWith("/CreateSitemap"))
                CreateSitemap();

            return View("~/Views/Index.cshtml");
        }

        /// <summary>
        /// Product Sitemap
        /// </summary>
        /// <returns></returns>
        protected void CreateSitemap()
        {
            string resultString = "";

            try
            {
                String url = Request.Url.GetLeftPart(UriPartial.Authority);

                HttpClient client = new HttpClient();
                client.BaseAddress = new Uri(url);
                client.DefaultRequestHeaders.Accept.Clear();

                var stringContent = new StringContent("");

                HttpResponseMessage response =
                    Task.Run(() => client.PostAsync("/api/Sitemap", stringContent)).Result;

            }
            catch (Exception e)
            {
                throw new CustomException("Unable to call sitemap create API");
            }
        }


    }
}
