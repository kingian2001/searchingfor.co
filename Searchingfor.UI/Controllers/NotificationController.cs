﻿using Searchingfor.Core.Models;
using Searchingfor.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Utilities.Container;

namespace Searchingfor.UI.Controllers
{
    public class NotificationController : BaseController<NotificationVM>
    {
        /// <summary>
        /// Creates a Notification
        /// </summary>
        /// <param name="Notification"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public IHttpActionResult Create(NotificationVM Notification)
        {
            try
            {
                Notification.Broadcast = Notify;

                var result = Notification.Create();

                return Ok(
                    new
                    {
                        result = result
                    });
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch
            {
                return InternalServerError();
            }
        }


        /// <summary>
        /// Updates the read property of the notification
        /// </summary>
        /// <param name="Notification"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public IHttpActionResult UpdateReadPropertyBulk(List<NotificationVM> Notification)
        {
            try
            {
                bool result = ViewModel.UpdateReadPropertyBulk(Notification);

                return Ok(
                    new
                    {
                        result = result
                    });
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                log.Error(e.Message, e);
                return InternalServerError(new Exception("There was an internal server error, unable to save."));
            }
        }

        /// <summary>
        /// Updates the read property of the notification
        /// </summary>
        /// <param name="Notification"></param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public IHttpActionResult UpdateReadProperty(NotificationVM Notification)
        {
            try
            {
                bool result = Notification.UpdateReadProperty();

                return Ok(
                    new
                    {
                        result = result
                    });
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                log.Error(e.Message, e);
                return InternalServerError(new Exception("There was an internal server error, unable to save."));
            }
        }

        /// <summary>
        /// searches Notifications
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        public IHttpActionResult Search(System.Web.OData.Query.ODataQueryOptions<Notification> options)
        {
            try
            {
                SearchResultOld<List<NotificationVM>> SearchResult = ViewModel.Search(options);

                return Ok(new
                {
                    result = SearchResult
                });
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                log.Error(e.Message, e);
                return InternalServerError(new Exception("There was an internal server error, unable to save."));
            }
        }

        /// <summary>
        /// searches Notifications
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        [HttpGet]
        [Authorize]
        public IHttpActionResult UnreadCount(Guid userId)
        {
            try
            {
                int result = ViewModel.Count(x => x.Read == false && x.User.ID == userId);

                return Ok(new
                {
                    result = result
                });
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                log.Error(e.Message, e);
                return InternalServerError(new Exception("There was an internal server error, unable to save."));
            }
        }
    }
}