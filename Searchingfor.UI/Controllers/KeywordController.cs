﻿using Searchingfor.Core.Models;
using Searchingfor.Core.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Searchingfor.UI.Controllers
{
    public class KeywordController : BaseController<KeywordUserVM>
    {
        /// <summary>
        /// Assigns keywords to users.
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public IHttpActionResult AssignKeywords([FromBody] List<KeywordUserVM> keywordUsers)
        {
            try
            {
                var result = ViewModel.Create(keywordUsers);

                return Ok(
                    new
                    {
                        result = result
                    });
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                log.Error(e.Message, e);
                return InternalServerError(new Exception("There was an internal server error."));
            }
        }

        /// <summary>
        /// Keywords assign to slots
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        public IHttpActionResult KeywordsToSlots([FromBody] List<KeywordUserVM> keywordUsers)
        {
            try
            {
                var result = ViewModel.AssignToSlots(keywordUsers);

                return Ok(
                    new
                    {
                        result = result
                    });
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                log.Error(e.Message, e);
                return InternalServerError(new Exception("There was an internal server error."));
            }
        }

        /// <summary>
        /// retrieve keywords for user.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult GetKeywords(Guid userID)
        {
            try
            {
                List<KeywordUserVM> result = ViewModel.GetKeywords(userID);

                return Ok(new { result = result });
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                log.Error(e.Message, e);
                return InternalServerError(new Exception("There was an internal server error."));
            }
        }
    }
}