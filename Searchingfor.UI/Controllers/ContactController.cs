﻿using Searchingfor.Core.Models;
using Searchingfor.Core.ViewModels;
using Searchingfor.Entity.Entities;
using Searchingfor.UI.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Utilities;
using Utilities.Container;
using Searchingfor.UI.Models;

namespace Searchingfor.UI.Controllers
{
    public class ContactController : BaseController<ContactVM>
    {
        /// <summary>
        /// Save a Contacts data
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult Create(ContactVM contact)
        {
            try
            {
                bool values = contact.Create();

                return Ok(
                    new
                    {
                        result = values,
                    });
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                log.Error(e.Message, e);
                return BadRequest("Unable to request. try again later.");
            }
        }
        /// <summary>
        /// retrieve list of Contacts
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IHttpActionResult Search(System.Web.OData.Query.ODataQueryOptions<Contact> options)
        {
            try
            {
                SearchResultOld<List<ContactVM>> SearchResult = ViewModel.Search(options);

                return Ok(SearchResult);
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                log.Error(e.Message, e);
                return InternalServerError(new Exception("There was an internal server error, unable to save."));
            }
        }
    }
}