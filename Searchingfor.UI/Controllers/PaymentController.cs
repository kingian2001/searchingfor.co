﻿using Searchingfor.Core.Models;
using Searchingfor.Core.ViewModels;
using Searchingfor.Entity.Entities;
using Searchingfor.UI.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Utilities;
using Utilities.Container;
using Searchingfor.UI.Models;
using log4net;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using Newtonsoft.Json.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;

namespace Searchingfor.UI.Controllers
{
    public class PaymentController : BaseController<NotificationVM>
    {
        [Route("IPN")]
        [HttpPost]
        public IHttpActionResult IPN()
        {
            try
            {
                // if you want to use the PayPal sandbox change this from false to true
                string response = string.Empty;

                string postedString = Task.Run(() => Request.Content.ReadAsStringAsync()).Result;

                //response = obj;
                response = GetPayPalResponse(false, postedString);

                if (response == "VERIFIED")
                {
                    var variables = ParseIPN(postedString);

                    var notification = new NotificationVM();

                    notification.Type = "Payments";

                    UserVM user = notification.User = new UserVM()
                    {
                        ID = Guid.Parse(variables["custom"])
                    };

                    user.ModifyCredit((int)double.Parse(variables["mc_gross"]));

                    notification.Title = "Successfully bought (" + variables["mc_gross"] + ") " + variables["mc_currency"] + " credits";

                    notification.Broadcast = Notify;

                    notification.Create();

                    return Ok();
                }
                else
                {
                    return BadRequest();
                }

                return Ok();
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch (NotFoundException e)
            {
                return NotFound();
            }
            catch (Exception e)
            {
                var log = LogManager.GetLogger(typeof(PaymentController));

                log.Error(e.Message, e);

                return InternalServerError(new Exception("Was unable to process payment."));
            }
        }

        string GetPayPalResponse(bool useSandbox, string request)
        {
            string responseState = "INVALID";

            try
            {
                // Parse the variables
                // Choose whether to use sandbox or live environment
                string paypalUrl = useSandbox ? "https://www.sandbox.paypal.com/"
                : "https://www.paypal.com/";

                //string validateUrl = paypalUrl + "cmd=_notify-validate";

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                ServicePointManager.ServerCertificateValidationCallback += (sender, cert, chain, sslPolicyErrors) => true;

                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(paypalUrl);

                    var result = client.PostAsync("cmd=_notify-validate", new StringContent(request)).Result;

                    responseState = result.Content.ReadAsStringAsync().Result;
                }

                return responseState;
            }
            catch (Exception e)
            {
                return responseState;
            }
        }

        Dictionary<string, string> ParseIPN(string s)
        {
            List<string> z = s.Split('&').ToList();

            Dictionary<string, string> dic = new Dictionary<string, string>();

            foreach (var o in z)
            {
                var q = o.Split('=');

                dic.Add(q[0], q[1]);
            }

            return dic;
        }

    }
}