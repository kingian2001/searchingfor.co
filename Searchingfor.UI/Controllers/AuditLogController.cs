﻿using Searchingfor.Core.Models;
using Searchingfor.Core.ViewModels;
using Searchingfor.Entity.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Utilities.Container;

namespace Searchingfor.UI.Controllers
{
    [Authorize]
    public class AuditLogController : BaseController<AuditLogVM>
    {
        /// <summary>
        /// searches reviews
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        [HttpGet]
        public IHttpActionResult Search(System.Web.OData.Query.ODataQueryOptions<AuditLog> options)
        {
            try
            {
                SearchResultOld<List<AuditLogVM>> SearchResult = ViewModel.Search(options);

                return Ok(new
                {
                    result = SearchResult
                });
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                log.Error(e.Message, e);
                return InternalServerError(new Exception("There was an internal server error, unable to save."));
            }
        }

        [HttpGet]
        public IHttpActionResult Statistics(Guid? userId = null)
        {
            try
            {
                AuditLogStatisticsVM stat = ViewModel.GetStatistics(userId);

                return Ok(new
                {
                    result = stat
                });
            }
            catch (CustomException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception e)
            {
                log.Error(e.Message, e);
                return InternalServerError(new Exception("There was an internal server error, unable to save."));
            }
        }
    }
}