﻿app.controller('PremiumShopController',
    ['$location', '$window',
        'PremiumService',
        'UtilityService',
        'UserService',
        '$scope',
        'TabService',
        '$routeParams',
        function (
        $location, $window,
        PremiumService,
        UtilityService,
        UserService,
        $scope,
        TabService,
        $routeParams) {

            vm = this;
            vm.Util = UtilityService;
            vm.Auth = auth;

            vm.tabs = [
            //{
            //    title: 'Buy Credits',
            //    url: 'BuyCredits.html',
            //    isAuthenticated: true,//should be owner to view this
            //},
            //{
            //    title: 'Feature Ads',
            //    url: 'FeatureAds.html',
            //    isAuthenticated: true,//should be owner to view this
            //}, 
            //{
            //    title: 'Feature Bids',
            //    url: 'FeatureBids.html',
            //    isAuthenticated: true,//should be owner to view this
            //},
            {
                title: 'Keyword Watch',
                url: 'KeywordWatch.html',
                isAuthenticated: true,//should be owner to view this
            }];

            vm.ProfileHtmlLocation = "app/Views/Premium/";

            vm.currentTab = "";

            vm.RetrieveCreditBalance = function () {
                UserService.RetrieveCreditBalance(vm.Auth.UserData.ID).$promise.then(
                    function (success) {
                        vm.Auth.UserData.Credits = success.result;
                    },
                    function (error) { });
            }

            //tabs in profile
            GetTab = function () {
                if (!$routeParams.tab) {
                    vm.currentTab = vm.ProfileHtmlLocation + vm.tabs[0].url;
                    return;
                }

                for (index in vm.tabs) {
                    tab = vm.tabs[index];
                    if (tab.title == $routeParams.tab) {
                        vm.currentTab = vm.ProfileHtmlLocation + tab.url;
                        break;
                    }
                }
            }

            vm.GetPremiums = function (type) {
                PremiumService.GetPremiums(auth.UserData.ID, type, true).$promise.then(
                    function (success) {
                        vm.Premiums = success.result;
                         UtilityService.UTCToLocalTime(vm.Premiums);
                        if (vm.Premiums && vm.Premiums.length > 0)
                            vm.ShowBuyPremiumButton = true;
                    },
                    function (error) {
                    });
            }

            function init() {
                vm.Util.FocusBody();

                //set 
                TabService.SetTabs(vm.tabs, vm.ProfileHtmlLocation);

                vm.RetrieveCreditBalance();

                GetTab();
            }

            init();

            /**
            ** Utilities
            **/
            //for profile
            vm.onClickTab = function (tab) {
                $location.pathOverride("PremiumShop/" + tab.title, false).search({});
                vm.currentTab = vm.ProfileHtmlLocation + tab.url;
            }

            //for profile
            vm.isActiveTab = function (tab) {
                return vm.currentTab == vm.ProfileHtmlLocation + tab.url;
            }

            //if the user is able to see the tab
            vm.IsTabViewable = function (tab) {
                //if not logged in and is ok to view
                if (!tab.isAuthenticated)
                    return true;

                //if logged in
                if (tab.isAuthenticated && vm.Auth.UserData)
                    return true;

                return false;
            }
        }]);