﻿app.controller('PremiumShopFeatureAdController',
    ['$location', '$window',
        'PremiumService',
        'UtilityService',
        'UserService',
        '$scope',
        'TabService',
        '$routeParams',
        'PostService',
        '$uibModal',
        'PopupService',
        'TextModifierService',
        function (
        $location, $window,
        PremiumService,
        UtilityService,
        UserService,
        $scope,
        TabService,
        $routeParams,
        PostService,
        $uibModal,
        PopupService,
        TextModifierService) {

            psfac = this;
            psfac.TextModifierService = TextModifierService;
            psfac.Posts = {};
            $scope.Now = new Date();
            $scope.Now = $scope.Now.toISOString();

            psfac.RetrievePricingList = function () {
                PremiumService.GetFeatureAdPricingList().$promise.then(
                  function (success) {
                      psfac.Choices = success.result;
                  },
                  function (error) { });
            }

            psfac.GetPosts = function () {
                query = {
                    $top: 9999,
                    $skip: 0,
                    $orderby: "Title asc ,ModifiedOn desc",
                    $inlinecount: "allpages",
                    $filter : "Owner/ID eq guid'" + auth.UserData.ID + "'"
                };

                vm.Processing = true;
                PostService
                    .SearchWithPremium(query).$promise.then(
                        function (success) {
                            result = JSON.parse(success.result);
                            if (!psfac.Posts.List)
                                psfac.Posts.List = [];
                            UtilityService.UTCToLocalTime(result.Result);
                            psfac.Posts.List = result.Result;
                            psfac.Posts.Count = result.Count;
                            psfac.Processing = false;
                        }, function (error) {
                            vm.Processing = false;
                        });
            }

            function init() {
                psfac.RetrievePricingList();
                psfac.GetPosts();
            }

            init();

            psfac.SelectPricing = function (pricing) {
                psfac.SelectedPricing = pricing;
            }

            $scope.OpenPurchasePremiumModal = function (size, post) {
                $scope.selectedPost = post;
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'PurchasePremiumModal.html',
                    controller: 'ModalController',
                    scope: $scope, //passed current scope to the modal
                    size: size,
                    resolve: {
                        obj: function () {
                            return vm.Premiums;
                        }
                    }
                });

                modalInstance.result.then(function () {
                    obj = {};
                    obj.User = { ID: auth.UserData.ID };
                    obj.Post = { ID: $scope.selectedPost.ID };
                    obj.SelectedPricing = psfac.SelectedPricing
                    vm.Processing = true;
                    PremiumService.BuyPremium(obj).$promise.then(
                       function (success) {
                           vm.Processing = false;
                           psfac.SelectedPricing = null;

                           for (o in success.result) {
                               for (i in psfac.Posts.List) {
                                   if (psfac.Posts.List[i].ID == success.result[o].Post.ID) {
                                       psfac.Posts.List[i].Featured = true;
                                       psfac.Posts.List[i].Premium = success.result[o];
                                   }
                               }
                           }

                           vm.RetrieveCreditBalance();
                           PopupService.success("Featuring '" + $scope.selectedPost.Title + "' for " + obj.SelectedPricing.DurationInDays + " days");
                       },
                       function (error) { vm.Processing = false; });
                }, function (error) {
                });
            };

            $scope.OpenExtendPremiumModal = function (size, post) {
                $scope.selectedPost = post;
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'ExtendPremiumModal.html',
                    controller: 'ModalController',
                    scope: $scope, //passed current scope to the modal
                    size: size,
                    resolve: {
                        obj: function () {
                            return vm.Premiums;
                        }
                    }
                });

                modalInstance.result.then(function () {
                    obj = {};
                    obj.ID = $scope.selectedPost.Premium.ID;
                    obj.User = { ID: auth.UserData.ID };
                    obj.Post = { ID: $scope.selectedPost.ID };
                    obj.SelectedPricing = psfac.SelectedPricing
                    vm.Processing = true;
                    PremiumService.ExtendPremium(obj).$promise.then(
                       function (success) {
                           vm.Processing = false;
                           psfac.SelectedPricing = null;

                           for (i in psfac.Posts.List) {
                               if (psfac.Posts.List[i].ID == success.result.Post.ID) {
                                   psfac.Posts.List[i].Featured = true;
                                   psfac.Posts.List[i].Premium = success.result;
                               }
                           }


                           vm.RetrieveCreditBalance();
                           PopupService.success("Successfully extended the keyword slot by " + obj.SelectedPricing.DurationInDays + " days");
                       },
                       function (error) { vm.Processing = false; });
                }, function (error) {
                });
            };
        }]);