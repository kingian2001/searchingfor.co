﻿app.controller('PremiumShopFeatureBidController',
    ['$location', '$window',
        'PremiumService',
        'UtilityService',
        'UserService',
        '$scope',
        'TabService',
        '$routeParams',
        '$uibModal',
        'PopupService',
        function (
        $location, $window,
        PremiumService,
        UtilityService,
        UserService,
        $scope,
        TabService,
        $routeParams,
        $uibModal,
        PopupService) {
            psfbc = this;
            vm.Util = UtilityService;

            psfbc.RetrievePricingList = function () {
                PremiumService.GetFeatureCommentPricingList().$promise.then(
                  function (success) {
                      psfbc.Choices = success.result;
                  },
                  function (error) { });
            }

            function init() {
                psfbc.RetrievePricingList();
                vm.GetPremiums("Feature Comment");
            }

            init();

            psfbc.SelectPricing = function (pricing) {
                psfbc.SelectedPricing = pricing;
            }

            $scope.OpenPurchasePremiumModal = function (size) {
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'PurchasePremiumModal.html',
                    controller: 'ModalController',
                    scope: $scope, //passed current scope to the modal
                    size: size,
                    resolve: {
                        obj: function () {
                            return vm.Premiums;
                        }
                    }
                });

                modalInstance.result.then(function () {
                    obj = {};
                    obj.User = { ID: auth.UserData.ID };
                    obj.SelectedPricing = psfbc.SelectedPricing;
                    vm.Processing = true;
                    PremiumService.BuyPremium(obj).$promise.then(
                       function (success) {
                           vm.Processing = false;
                           psfbc.SelectedPricing = null;

                           if (vm.Premiums)
                               vm.Premiums = [];

                           vm.Premiums[0] = success.result[0];
                           vm.ShowBuyPremiumButton = false;
                           vm.RetrieveCreditBalance();

                           PopupService.success("You have purchased this premium for " + obj.SelectedPricing.DurationInDays + " days");
                       },
                       function (error) { vm.Processing = false; });
                }, function (error) {
                });
            };

            $scope.OpenExtendPremiumModal = function (size) {
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'ExtendPremiumModal.html',
                    controller: 'ModalController',
                    scope: $scope, //passed current scope to the modal
                    size: size,
                    resolve: {
                        obj: function () {
                            return vm.Premiums;
                        }
                    }
                });

                modalInstance.result.then(function () {
                    obj = {};
                    obj.ID = vm.Premiums[0].ID;
                    obj.User = { ID: auth.UserData.ID };
                    obj.SelectedPricing = psfbc.SelectedPricing;
                    vm.Processing = true;
                    PremiumService.ExtendPremium(obj).$promise.then(
                       function (success) {
                           vm.Processing = false;
                           psfbc.SelectedPricing = null;

                           if (!vm.Premiums)
                               vm.Premiums = [];

                           vm.Premiums[0] = success.result;

                           vm.RetrieveCreditBalance();
                           PopupService.success("Successfully extended the premium for " + obj.SelectedPricing.DurationInDays + " days");
                       },
                       function (error) { vm.Processing = false; });
                }, function (error) {
                });
            };
        }]);