﻿app.controller('ProfileReviewsController',
    ['$location', '$window',
        'localStorageService',
        'SharedDataService',
        'PostService',
        'CommentService',
        'PopupService',
        'UserService',
        'UtilityService',
        'LookupService',
        '$rootScope',
        '$routeParams',
        'AuthService',
        'ReviewService',
        function (
        $location, $window,
        localStorageService,
        SharedDataService,
        PostService,
        CommentService,
        PopupService,
        UserService,
        UtilityService,
        LookupService,
        $rootScope,
        $routeParams,
        AuthService,
        ReviewService) {

            rc = this;
            vm.Util = UtilityService;
            vm.Auth = auth;
            rc.Review = {};
            rc.Pagination = {
                PageSize: 4,
                Current: 1
            };
            rc.Selected = {};

            rc.SearchReviews = function (newPageNumber, OldPageNumber) {
                
                page = (newPageNumber ? newPageNumber : 0 - OldPageNumber ? OldPageNumber : 0) || 0;

                skip = (rc.Pagination.Current + (page ? 0 : page)) - 1;

                query = {
                    $top: rc.Pagination.PageSize,
                    $orderby: "DateCreated desc",
                    $skip: skip ? skip * rc.Pagination.PageSize : 0,
                    $inlinecount: "allpages"
                };

                query.$filter = "Reviewee/ID eq guid'" + vm.Profile.ID + "'";

                if (rc.Selected && rc.Selected.Type)
                    query.$filter += " and Type eq '"+rc.Selected.Type+"'";

                vm.Processing = true;
                ReviewService
                    .Search(query).$promise.then(
                        function (success) {
                            if (!rc.Review.List)
                                rc.Review.List = [];
                            rc.Review.List = success.result.Result;
                            rc.Review.Count = success.result.Count;
                            vm.Processing = false;
                        }, function (error) {
                            vm.Processing = false;
                        });
            }

            function init() {

                rc.SearchReviews();
            }

            init();


        }]);