﻿app.controller('ProfileMyAdsController',
    ['$location', '$window',
        'localStorageService',
        'SharedDataService',
        'PostService',
        'CommentService',
        'PopupService',
        'UserService',
        'UtilityService',
        'LookupService',
        '$rootScope',
        '$routeParams',
        'AuthService',
        'TextModifierService',
        function (
        $location, $window,
        localStorageService,
        SharedDataService,
        PostService,
        CommentService,
        PopupService,
        UserService,
        UtilityService,
        LookupService,
        $rootScope,
        $routeParams,
        AuthService,
        TextModifierService) {

            vm.Util = UtilityService;
            vm.TextModifierService = TextModifierService;
            vm.BuyPosts = {};
            vm.BuyPosts.LookCss = "col-sm-3";
            vm.SellPosts = {};
            vm.SellPosts.LookCss = "col-sm-3";
            vm.BuyPagination = {
                PageSize: 12,
                Current: 1
            };
            vm.SellPagination = {
                PageSize: 12,
                Current: 1
            };

            vm.SearchSellPosts = function (newPageNumber, OldPageNumber) {
                vm.Processing = true;

                page = (newPageNumber ? newPageNumber : 0 - OldPageNumber ? OldPageNumber : 0) || 0;

                skip = (vm.SellPagination.Current + ((page === undefined || page == null || page == 0) ? 0 : page)) - 1;

                query = {
                    $top: vm.SellPagination.PageSize,
                    $orderby: "ModifiedOn desc",
                    $skip: skip ? skip * vm.SellPagination.PageSize : 0,
                    $inlinecount: "allpages"
                };

                query.$filter = "Owner/ID eq guid'" + vm.Profile.ID + "' and Selling eq true";

                PostService
                    .Search(query).$promise.then(
                        function (success) {
                            result = JSON.parse(success.result);
                            if (!vm.SellPosts.List)
                                vm.SellPosts.List = [];
                            UtilityService.UTCToLocalTime(result.Result);
                            vm.SellPosts.List = result.Result;
                            vm.SellPosts.Count = result.Count;
                            vm.Processing = false;
                        }, function (error) {
                            vm.Processing = false;
                        });
            }

            vm.SearchBuyPosts = function (newPageNumber, OldPageNumber) {
                vm.Processing = true;

                page = (newPageNumber ? newPageNumber : 0 - OldPageNumber ? OldPageNumber : 0) || 0;
                //(page === undefined || page == null || page == 0)
                skip = (vm.BuyPagination.Current + (page ? 0 : page)) - 1;
                //vm.BuyPagination.Current =skip;
                query = {
                    $top: vm.BuyPagination.PageSize,
                    $orderby: "ModifiedOn desc",
                    $skip: skip ? skip * vm.BuyPagination.PageSize : 0,
                    $inlinecount: "allpages"
                };

                query.$filter = "Owner/ID eq guid'" + vm.Profile.ID + "' and (Selling eq false or Selling eq null)";

                PostService
                    .Search(query).$promise.then(
                        function (success) {
                            result = JSON.parse(success.result);
                            if (!vm.BuyPosts.List)
                                vm.BuyPosts.List = [];
                            UtilityService.UTCToLocalTime(result.Result);
                            vm.BuyPosts.List = result.Result;
                            vm.BuyPosts.Count = result.Count;
                            vm.Processing = false;
                        }, function (error) {
                            vm.Processing = false;
                        });
            }

            vm.IsOwner = function (post) {
                if (post && auth.IsAuthenticated && (post.Owner.ID == auth.UserData.ID))
                    return true;
                return false;
            }

            function init() {
                vm.SearchBuyPosts();
                vm.SearchSellPosts();
            }

            init();


        }]);