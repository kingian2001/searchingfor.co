﻿app.controller('ProfileLogsController',
    ['$location', '$window',
        'localStorageService',
        'SharedDataService',
        'PostService',
        'CommentService',
        'PopupService',
        'UserService',
        'UtilityService',
        'LookupService',
        '$rootScope',
        '$routeParams',
        'AuthService',
        'TextModifierService',
        'LogService',
        function (
        $location, $window,
        localStorageService,
        SharedDataService,
        PostService,
        CommentService,
        PopupService,
        UserService,
        UtilityService,
        LookupService,
        $rootScope,
        $routeParams,
        AuthService,
        TextModifierService,
        LogService) {

            plc = this;
            vm.Util = UtilityService;
            plc.Pagination = {
                PageSize: 10,
                Current: 1
            };
            plc.Logs = {};
            plc.Filter = {};

            CreateFilter = function () {
                var filter = "";

                filterarray = [];

                if ($routeParams.userId)
                    filterarray.push("User/ID eq guid'" + $routeParams.userId + "'");

                return filter = filterarray.join(" and ");
            }

            plc.SearchAuditLogs = function (newPageNumber, OldPageNumber) {

                page = (newPageNumber ? newPageNumber : 0 - OldPageNumber ? OldPageNumber : 0) || 0;

                skip = (plc.Pagination.Current + (page ? 0 : page)) - 1;

                query = {
                    $top: plc.Pagination.PageSize,
                    $orderby: "DateTime desc",
                    $skip: skip ? skip * plc.Pagination.PageSize : 0,
                    $inlinecount: "allpages"
                };

                if (filter = CreateFilter())
                    query.$filter = filter;

                LogService
                .AuditLogSearch(query).$promise.then(
                    function (success) {
                        plc.Logs.List = success.result.Result;
                        plc.Logs.Count = success.result.Count;
                        UtilityService.UTCToLocalTime(plc.Logs.List);
                        vm.ShowAdminPanel = true;
                    }, function (error) {
                        PopupService.error("Unable to fetch from server");
                    });
            }

            function init() {
                plc.SearchAuditLogs();
            }

            init();
        }]);