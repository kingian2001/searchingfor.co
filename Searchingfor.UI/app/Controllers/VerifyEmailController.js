﻿app.controller('VerifyEmailController',
    ['$location', '$window', 'UserService', 'PopupService', '$routeParams',
        function (
        $location, $window, UserService, PopupService, $routeParams) {

            vm = this;
            vm.Email = "";
            vm.Name = "";
            var code = "";
            var profile;
            function init() {

                code = $routeParams.code;
                var user = { ID: code };

                UserService.GetProfileDetails(user).$promise.then(
                   function (success) {
                       profile = success.result.Profile;
                       vm.Email = profile.Email;
                       vm.Name = profile.Name;
                       vm.Processing = false;
                   }, function (error) {
                       $location.path("404");
                       vm.Processing = false;
                   });
            }

            vm.sendEmail = function () {                
                UserService.SendEmailVerification(code,vm.Email,vm.Name).$promise.then(
                  function (success) {
                      PopupService.success("Verification Email Sent");
                      vm.Processing = false;
                  }, function (error) {
                      $location.path("404");
                      vm.Processing = false;
                  });
               // $location.path("Login");
            }
            init();
        }]);
