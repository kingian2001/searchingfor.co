﻿app.controller('UserController',
    ['$location', '$window',
        'localStorageService',
        'SharedDataService',
        'PostService',
        'CommentService',
        'PopupService',
        'UserService',
        'UtilityService',
        'LookupService',
        '$rootScope',
        '$routeParams',
        'AuthService',
        '$q',
        'TabService',
        'DropdownService',
        'ReviewService',
        function (
            $location, $window,
            localStorageService,
            SharedDataService,
            PostService,
            CommentService,
            PopupService,
            UserService,
            UtilityService,
            LookupService,
            $rootScope,
            $routeParams,
            AuthService,
            $q,
            TabService,
            DropdownService,
            ReviewService) {

            vm = this;
            vm.tmpPost = tmpPost;
            vm.Auth = auth;
            vm.Util = UtilityService;
            $rootScope.ErrorKey = 0;
            vm.Countries = countries;
            vm.TabService = TabService;
            vm.Selected = {};
            vm.FBName = "";
            vm.tabs = [{
                title: 'Dashboard',
                url: 'ProfileDashboard.html',
                isAuthenticated: false,
            }, {
                title: 'My Ads',
                url: 'ProfileAds.html',
                isAuthenticated: true,//should be owner to view this
            },
            //{
            //    title: 'Favorite Ads',
            //    url: 'ProfileFavorites.html',
            //    isAuthenticated: true,//should be owner to view this
            //},
            {
                title: 'Reviews',
                url: 'ProfileReviews.html',
                isAuthenticated: false,
            },
            //{
            //    title: 'Pricing Plan',
            //    url: 'ProfilePricing.html',
            //    isAuthenticated: true,//should be owner to view this
            //},
            //{
            //    title: 'Payment Settings',
            //    url: 'ProfilePayments.html',
            //    isAuthenticated: true,//should be owner to view this
            //}, 
            {
                title: 'User Logs',
                url: 'ProfileLogs.html',
                isAuthenticated: true,//should be owner to view this
            }, {
                title: 'Settings',
                url: 'ProfileSettings.html',
                isAuthenticated: true,//should be owner to view this
            }];

            vm.ProfileHtmlLocation = "app/Views/Profile/";

            vm.Signup = function (data) {

                //validation here
                if (!data.Type)
                    data.Type = "individual";
                data.Country = vm.Selected.Country.Name;
                data.City = vm.Selected.City.Name;
                //data.Country = vm.Util.GetObjectWithIDOnly(vm.Selected.Country);
                //data.City = vm.Util.GetObjectWithIDOnly(vm.Selected.City);
                data.NewUser = true;
                vm.Processing = true;
                UserService.Create(data).$promise.then(
                    function (success) {
                        vm.Processing = false;
                        //if (tmpPost) {
                        //    PopupService.success('A verification email was sent to ' + success.result.Email);
                        //    vm.CreatePost(tmpPost);
                        //} else
                        $location.path("VerifyEmail/" + success.result.ID);
                    }, function (error) {
                        vm.Processing = false;
                    });
            }

            vm.UpdateUser = function (user) {
                if (!user.Email)
                    PopupService.error("Email is required and should be unique");

                object = {};
                object = vm.Util.Clone(user);
                object = user;
                object.Country = vm.Selected.Country.Name;
                object.City = vm.Selected.City.Name;
                //object.Country = { ID: vm.Selected.Country.ID, Name: vm.Selected.Country.Name };
                //object.City = { ID: vm.Selected.City.ID, Name: vm.Selected.City.Name };
                vm.Processing = true;
                UserService.Update(object).$promise.then(
                    function (success) {
                        vm.Processing = false;

                        //update profile
                        vm.Profile = vm.Util.Clone(user);
                        vm.Profile.Country = vm.Selected.Country.Name;
                        vm.Profile.City = vm.Selected.City.Name;

                        //update cached copy.
                        vm.Auth.me.StoreUserData(object);
                        PopupService.success("Successfully updated profile");
                    }, function (error) {
                        vm.Processing = false;
                    });
            }

            vm.ChangePassword = function (user, EmailToLower, callback) {
                if (!user.OldPassword)
                    PopupService.error("Old password is required");

                if (!user.Password)
                    PopupService.error("New password is required");

                if (user.Password != user.Password2)
                    PopupService.error("New password and confirm password should match");

                user.ID = vm.Auth.UserData.ID;
                vm.Processing = true;
                UserService.ChangePassword(user, EmailToLower).$promise.then(
                    function (success) {
                        if (success.result) {
                            if (callback)
                                callback();
                            PopupService.success("Successfully changed password!");
                            vm.ChangePasswordObject = null;
                        } else {
                            PopupService.success("Failed to change password, try again later");
                        }
                        vm.Processing = false;
                    }, function (error) {
                        vm.Processing = false;
                    });
            }

            vm.SubmitReview = function (review) {
                review.Reviewer = { ID: vm.Auth.UserData.ID, Name: vm.Auth.UserData.Name };
                review.Reviewee = { ID: vm.Profile.ID, Name: vm.Profile.Name };
                vm.Processing = true;
                ReviewService.Create(review).$promise.then(
                    function (success) {
                        if (success.result) {
                            vm.Reviewed = true;
                            PopupService.success("Thank you for your review!");
                        } else {
                            vm.Reviewed = false;
                            PopupService.success("Unable to submit review, try again later.");
                        }
                        vm.Processing = false;
                    }, function (error) {
                        vm.Processing = false;
                    });
            }

            vm.CanReview = function () {
                if (!vm.Auth.UserData)
                    return;

                query = {
                    $top: 1
                };

                query.$filter = "Reviewer/ID eq guid'" + vm.Auth.UserData.ID + "' and Reviewee eq guid'" + vm.Profile.ID + "'";

                vm.Processing = true;
                ReviewService.Search(query).$promise.then(
                    function (success) {
                        if (success.result.Result.length > 0) {
                            vm.Reviewed = true;
                        } else {
                            vm.Reviewed = false;
                        }
                        vm.Processing = false;
                    },
                    function (error) {
                        vm.Processing = false;
                    });
            }

            vm.Login = function (loginData) {

                //validation here.
                if (!loginData || !loginData.Email || !loginData.Password) {
                    PopupService.error("No email and/or password");
                    return;
                }
                if (loginData.Password.length < 6) {
                    PopupService.error("Password should be of 6 characters or more");
                    return;
                }


                string = "grant_type=password&username=" + loginData.Email + "&password=" + loginData.Password;
                vm.Processing = true;
                UserService.Token(string).$promise.then(
                    function (success) {
                        vm.Processing = false;
                        vm.Auth.me.StoreAuth(success, true);
                        vm.Auth = auth;
                        obj = JSON.parse(success.Value);
                        if (obj.EmailVerified) {
                            PopupService.success("Successfully logged in");
                            vm.CreatePost(tmpPost);
                        } else {
                            $location.path("VerifyEmail/" + obj.ID);
                        }
                    },
                    function (error) {
                        vm.Processing = false;
                        if (error.data.error_description)
                            PopupService.error(error.data.error_description);
                        else
                            PopupService.error("Username or Password might be wrong");
                    });
            }

            vm.LoginUsingFacebook = function () {
                window.fbAsyncInit();
                FB.login(function (response) {
                    vm.Processing = true;

                    if (response.authResponse) {
                        var accessToken = response.authResponse.accessToken;
                        var email;// = response.email;
                        var name;
                        vm.Processing = false;
                        //after getting user details from db. register to system.
                        FB.api('/me', { fields: 'id,name,email' }, function (response) {
                            vm.Processing = true;
                            console.log('Thanks for logging in, ' + response.name + ' ' + response.email + '!');
                            email = response.email;
                            name = response.name;

                            User = { Email: email, Name: name, FBAccessToken: accessToken };

                            //create account using fb credentials
                            UserService.LoginFB(User).$promise.then(
                                function (success) {
                                    vm.Processing = false;

                                    vm.Auth.me.StoreAuth(success.token, false);
                                    vm.Auth = auth;
                                    //remove later
                                    if (success.isNewUser) {
                                        vm.FBName = success.result.Name;
                                        $('#myModal').modal('show');
                                        isFirstLogin = true;
                                    } else {
                                        PopupService.success("Signed up successfully! Logging in...");
                                        //PopupService.success("Signed up successfully! Unable to login, try to login now...");
                                        if (tmpPost)
                                            vm.CreatePost(tmpPost);
                                        else
                                            $location.path("Dashboard");
                                    }


                                }, function (error) {
                                    vm.Processing = false;
                                });
                        });


                    }
                }, { scope: 'email' });

                return false;
            }

            vm.FBLoginChangePassword = function (user) {
                user.OldPassword = user.OldPassword.toLowerCase();
                vm.ChangePassword(user, true, vm.deleteModal);
                vm.CreatePost(tmpPost);
            }

            vm.deleteModal = function () {
                vm.Util.CloseModal('myModal');
                $location.path("/Dashboard");
            }

            vm.FBLoginSkip = function (user) {
                vm.Util.CloseModal('myModal');
                vm.CreatePost(tmpPost);
            }

            window.fbAsyncInit = function () {
                FB.init({
                    appId: '172467843173698',
                    cookie: true,
                    xfbml: true,
                    version: 'v2.7'
                });
            };


            vm.signOut = function () {
                var auth2 = gapi.auth2.getAuthInstance();
                auth2.signOut().then(function () {
                    console.log('User signed out.');
                });
            }

            var auth2; // The Sign-In object.
            var googleUser; // The current user.


            vm.LoginUsingGoogle = function () {

                //to continue
                // gapi.load('auth2', initSigninV2);   

            }

            /**
             * Initializes Signin v2 and sets up listeners.
             */
            var initSigninV2 = function () {
                auth2 = gapi.auth2.init({
                    client_id: '954115798607-5k0bn7s2dmo8g9a0aevc72834jfu11ct.apps.googleusercontent.com',
                    scope: 'profile'
                });

                // Listen for sign-in state changes.
                auth2.isSignedIn.listen(signinChanged);

                // Listen for changes to current user.
                auth2.currentUser.listen(userChanged);

                // Sign in the user if they are currently signed in.
                if (auth2.isSignedIn.get() == true) {
                    auth2.signIn();
                }

                // Start with the current live values.
                refreshValues();
            };

            /**
             * Listener method for sign-out live value.
             *
             * @param {boolean} val the updated signed out state.
             */
            var signinChanged = function (val) {
                console.log('Signin state changed to ', val);
                //alert(val);
            };

            /**
             * Listener method for when the user changes.
             *
             * @param {GoogleUser} user the updated user.
             */
            var userChanged = function (user) {
                console.log('User now: ', user);
                googleUser = user;
                updateGoogleUser();
                //alert(JSON.stringify(user, undefined, 2));
            };

            /**
             * Updates the properties in the Google User table using the current user.
             */
            var updateGoogleUser = function () {
                if (googleUser) {

                    alert(googleUser.getBasicProfile().getEmail());

                }
            };

            /**
             * Retrieves the current user and signed in states from the GoogleAuth
             * object.
             */
            var refreshValues = function () {
                if (auth2) {
                    console.log('Refreshing values...');

                    googleUser = auth2.currentUser.get();



                    updateGoogleUser();
                }
            }

            //retrieves information about the user.
            GetProfile = function () {
                //create user object using id in the url
                user = { ID: $routeParams.userId };

                UserService.GetProfileDetails(user).$promise.then(
                    function (success) {
                        vm.Profile = success.result.Profile;
                        vm.ActiveAdsCount = success.result.ActiveAdsCount;
                        vm.AverageRating = success.result.AverageRating;

                        //countries
                        if (vm.Profile.Country)
                            vm.Selected.Country = vm.Util.GetObjectByProperty("Name", vm.Profile.Country, vm.Countries);

                        //cities
                        if (vm.Selected.Country && (vm.Selected.Country.Subs.length > 0 || vm.Selected.Country.Subs))
                            vm.Selected.City = vm.Util.GetObjectByProperty("Name", vm.Profile.City, vm.Selected.Country.Subs);

                        //Set the tab
                        vm.currentTab = vm.TabService.GetTab($routeParams.tab, vm.IsTabViewable);

                        //check if the user can review the profile
                        vm.CanReview();

                        vm.ShowProfilePanel = true;
                    }, function (error) {
                    });
            }

            IsAllowedToAccessSignupPage = function () {

                //if authenticated and going to profile page.
                if (vm.Auth.IsAuthenticated &&
                    window.location.pathname.toUpperCase().includes("PROFILE")) {
                    return;
                }

                //if authenticated only.
                if (vm.Auth.IsAuthenticated) {
                    $location.path("/Dashboard");
                    PopupService.warning("Your already signed in!");
                    mc.init();
                    return;
                }
            }

            vm.CreatePost = function (posting) {

                if (!posting)
                    return;

                //use owner Id
                posting.Owner = {};
                posting.Owner.ID = vm.Auth.UserData.ID;
                posting.Owner.Name = vm.Auth.UserData.Name;

                vm.Processing = true;

                //retrieves all active categories with subcategories
                PostService.Create(posting).$promise.then(
                    function (success) {
                        vm.Processing = false;
                        tmpPost = null;
                        if (posting.Owner.NewUser) {
                            PopupService.success('A verficaiton email was sent to' + posting.Owner.Email);
                            vm.Auth.me.StoreAuth(success.token);
                        }
                        $location.path("/Post/" + (posting.LinkPostfix ? posting.LinkPostfix : JSON.parse(success.result).ID)).search({ FromDashboard: true });

                        PopupService.success('Sucessfully added an advertisment');
                    },
                    function (error) {
                        vm.Processing = false;
                    });
            }

            vm.GotoSignupPage = function () {
                if (tmpPost) {
                    if (confirm("Leaving this page will cancel creating a post. Proceed?")) {

                        $location.path("Signup");
                    }
                } else {
                    $location.path("Signup");
                }
            }

            function init() {

                //if from dashboard and tried to create post.
                if (tmpPost && vm.Auth.IsAuthenticated) {
                    vm.CreatePost(tmpPost);
                    return;
                }

                //set 
                vm.TabService.SetTabs(vm.tabs, vm.ProfileHtmlLocation);

                vm.Util.FocusBody();

                DropdownService.Set(vm).then(function (success) {
                    //if in profile page
                    if (window.location.href.toUpperCase().includes("PROFILE"))
                        GetProfile();
                }, function (error) {
                });

                IsAllowedToAccessSignupPage();
            }

            /**
            ** Utilities
            **/

            vm.UserHasUniqueEmail = function (email) {
                //check in database if unique
                return $q(function (resolve, reject) {
                    if (email) {
                        UserService.IsEmailUnique(email).$promise.then(
                            function (success) {
                                if (success.result)
                                    resolve();
                                else
                                    reject();
                            },
                            function (error) {
                                reject();
                            });
                    }
                });
            }

            vm.IfEmailIsUsedOtherThanThisUser = function (email) {
                return $q(function (resolve, reject) {
                    if (email) {
                        UserService.IfEmailIsUsedOtherThanThisUser(email, vm.Auth.UserData.ID).$promise.then(
                            function (success) {
                                if (success.result)
                                    reject();
                                else
                                    resolve();
                            },
                            function (error) {
                                reject();
                            });
                    }
                });
            }

            //for profile
            vm.onClickTab = function (tab) {
                $location.pathOverride("Profile/" + $routeParams.userId + "/" + tab.title, false).search({});
                vm.currentTab = vm.ProfileHtmlLocation + tab.url;
            }

            //for profile
            vm.isActiveTab = function (tab) {
                return vm.currentTab == vm.ProfileHtmlLocation + tab.url;
            }

            //if the user is able to see the tab
            vm.IsTabViewable = function (tab) {
                //if not logged in and is ok to view
                if (!tab.isAuthenticated)
                    return true;

                //if logged in and is the owner of the profile
                if (tab.isAuthenticated && vm.Auth.UserData && vm.Auth.UserData.ID == $routeParams.userId)
                    return true;

                return false;
            }

            //initialize
            init();
        }]);