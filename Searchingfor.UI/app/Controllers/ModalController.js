﻿app.controller('ModalController',
    ['$location', '$window', '$scope', '$uibModalInstance', 'obj',
        function (
        $location, $window, $scope, $uibModalInstance, obj) {

            $scope.obj = obj;

            $scope.ok = function () {
                $uibModalInstance.close();
            };

            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };
        }]);