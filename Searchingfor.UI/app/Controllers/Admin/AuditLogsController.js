﻿app.controller('LogsController',
    ['$location', '$window',
        'localStorageService',
        'SharedDataService',
        'PostService',
        'CommentService',
        'PopupService',
        'UserService',
        'UtilityService',
        'LookupService',
        '$rootScope',
        '$routeParams',
        'LogService',
        function (
        $location, $window,
        localStorageService,
        SharedDataService,
        PostService,
        CommentService,
        PopupService,
        UserService,
        UtilityService,
        LookupService,
        $rootScope,
        $routeParams,
        LogService) {

            alc = this;
            vm.Util = UtilityService;
            alc.Pagination = {
                PageSize: 20,
                Current: 1
            };
            alc.Logs = {};
            alc.Filter = {};

            CreateFilter = function () {
                var filter = "";

                filterarray = [];

                if ($routeParams.userID)
                    filterarray.push("User/ID eq guid'" + $routeParams.userID + "'");

                if (alc.Filter.Type)
                    filterarray.push("Type eq " + alc.Filter.Type + "");

                if (alc.Filter.FromDate) {
                    var date = new Date(alc.Filter.FromDate);
                    var s = date.getFullYear() + "-" + (date.getUTCMonth().length == 2 ? (date.getUTCMonth() + 1) : ("0" + (date.getUTCMonth() + 1))) + "-" + date.getUTCDate() + "T00:00:00.0000000";
                    filterarray.push("DateTime ge datetime'" + s + "'");
                }

                if (alc.Filter.ToDate) {
                    var date = new Date(alc.Filter.ToDate);
                    var s = date.getFullYear() + "-" + (date.getUTCMonth().length == 2 ? (date.getUTCMonth() + 1) : ("0" + (date.getUTCMonth() + 1))) + "-" + date.getUTCDate() + "T00:00:00.0000000";
                    filterarray.push("DateTime le datetime'" + s + "'");
                }
                
                return filter = filterarray.join(" and ");
            }

            alc.SearchAuditLogs = function (newPageNumber, OldPageNumber) {

                page = (newPageNumber ? newPageNumber : 0 - OldPageNumber ? OldPageNumber : 0) || 0;

                skip = (alc.Pagination.Current + (page ? 0 : page)) - 1;

                query = {
                    $top: alc.Pagination.PageSize,
                    $orderby: "DateTime desc",
                    $skip: skip ? (skip * alc.Pagination.PageSize) : 0,
                    $inlinecount: "allpages"
                };

                if (filter = CreateFilter())
                    query.$filter = filter;

                LogService
                .AuditLogSearch(query).$promise.then(
                    function (success) {
                        alc.Logs.List = success.result.Result;
                        vm.Util.UTCToLocalTime(alc.Logs.List);
                        alc.Logs.Count = success.result.Count;
                        vm.ShowAdminPanel = true;
                    }, function (error) {
                        PopupService.error("Unable to fetch from server");
                    });
            }

            function init() {
                alc.SearchAuditLogs();
            }

            init();
        }]);