﻿app.controller('AccountsController',
    ['$location', '$window',
        'localStorageService',
        'SharedDataService',
        'PostService',
        'CommentService',
        'PopupService',
        'UserService',
        'UtilityService',
        'LookupService',
        '$rootScope',
        '$routeParams',
        function (
        $location, $window,
        localStorageService,
        SharedDataService,
        PostService,
        CommentService,
        PopupService,
        UserService,
        UtilityService,
        LookupService,
        $rootScope,
        $routeParams) {

            ac = this;
            ac.Users = {};
            ac.Pagination = {
                PageSize: 20,
                Current: 1
            };
            ac.Filter = {};

            CreateFilter = function () {
                var filter = "";

                filterarray = [];

                if (ac.Filter.Email)
                    filterarray.push("substringof(tolower('" + ac.Filter.Email + "'),tolower(Email))");
                if (ac.Filter.Name)
                    filterarray.push("substringof(tolower('" + ac.Filter.Name + "'),tolower(Name))");
                if (ac.Filter.Country)
                    filterarray.push("Country eq '" + capitalizeFirstLetter(ac.Filter.Country.Name) + "'");
                if (ac.Filter.City)
                    filterarray.push("City eq '" + capitalizeFirstLetter(ac.Filter.City.Name) + "'");
                if (ac.Filter.Credits)
                    filterarray.push("Credits ge " + ac.Filter.Credits + "");

                return filter = filterarray.join(" and ");
            }

            ac.SearchUsers = function (newPageNumber, OldPageNumber) {

                page = (newPageNumber ? newPageNumber : 0 - OldPageNumber ? OldPageNumber : 0) || 0;

                skip = (ac.Pagination.Current + (page ? 0 : page)) - 1;

                query = {
                    $top: ac.Pagination.PageSize,
                    $skip: skip ? skip*ac.Pagination.PageSize : 0,
                    $inlinecount: "allpages"
                };

                if (filter = CreateFilter())
                    query.$filter = filter;

                vm.Processing = true;
                UserService
                .Search(query).$promise.then(
                    function (success) {
                        ac.Users.List = success.Result;
                        ac.Users.Count = success.Count;
                        vm.ShowAdminPanel = true;
                        vm.Processing = false;
                    }, function (error) {
                        vm.Processing = false;
                    });
            }

            ac.ShowUserLogs = function (user) {
                $location.path("Admin/Audit Logs").search({ userID: user.ID });
            }

            function init() {
                ac.SearchUsers();
            }

            init();


            function capitalizeFirstLetter(string) {
                return string.charAt(0).toUpperCase() + string.slice(1);
            }
        }]);