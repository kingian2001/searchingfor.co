﻿app.controller('PostController',
    ['$location', '$window',
        'localStorageService',
        'SharedDataService',
        'PostService',
        'CommentService',
        '$routeParams',
        'PopupService',
        'UtilityService',
        '$rootScope',
        'AuthService',
        'Upload',
        'TextModifierService',
        'Upload',
        'FileService',
        '$scope',
        'Lightbox',
        'DropdownService',
        'MetaService',
        function (
        $location, $window,
        localStorageService,
        SharedDataService,
        PostService,
        CommentService,
        $routeParams,
        PopupService,
        UtilityService,
        $rootScope,
        AuthService,
        Upload,
        TextModifierService,
        Upload,
        FileService,
        $scope,
        Lightbox,
        DropdownService,
        MetaService) {

            vm = this;
            $rootScope.ErrorKey = 0;
            vm.Util = UtilityService;
            vm.TextModifierService = TextModifierService;
            vm.Auth = AuthService;
            vm.Pagination = {
                PageSize: 5,
                Current: 1
            };
            vm.Selected = {};
            vm.Comment = {};
            vm.Comments = {};
            //used to index current image selection
            vm.index = 0;
            vm.TypeSelection = [
                {
                    Name: "Buy",
                    Selected: false,
                }, {
                    Name: "Sell",
                    Selected: false,
                }, {
                    Name: "Swap",
                    Selected: false,
                }, {
                    Name: "Rent",
                    Selected: false,
                },
            ];

            vm.OpenLightboxModal = function (index, list) {
                Lightbox.openModal(list, index);
            };

            vm.AddImage = function (file) {
                if (file) {
                    if (!vm.TempPost)
                        vm.TempPost = {};
                    if (!vm.TempPost.Images)
                        vm.TempPost.Images = [];
                    PopupService.info("Image rendering");
                    vm.TempPost.Images.push(file);
                    FileService.FilesToFileVMs(file).then(function (result) {
                        for (i in result)
                            vm.Post.Images.push(result[i]);
                    });
                }
            }

            vm.AddAttachment = function (file) {
                if (file) {
                    if (!vm.TempPost)
                        vm.TempPost = {};
                    if (!vm.TempPost.Attachments)
                        vm.TempPost.Attachments = [];
                    PopupService.info("Attaching file");
                    vm.TempPost.Attachments.push(file);

                    FileService.FilesToFileVMs(file).then(function (result) {
                        vm.Post.Attachments = [];
                        for (i in result)
                            vm.Post.Attachments.push(result[i]);
                    });
                }
            }

            vm.RemoveImage = function (file) {
                index = vm.TempPost.Images.indexOf(file);
                vm.TempPost.Images.splice(index, 1);
                vm.Post.Images.splice(index, 1);
            }

            vm.RemoveAttachment = function () {
                vm.TempPost.Attachments = vm.Post.Attachments = [];
            }

            _FillCommentFields = function () {

                if ($scope.commentForm) {
                    $scope.commentForm.$setPristine();
                    $scope.commentForm.$dirty = false;
                    angular.forEach($scope.commentForm.$error, function (field) {
                        field.$setPristine();
                    });
                }

                if (vm.TempComment && vm.TempComment.Images)
                    vm.TempComment.Images = [];

                //authenticated. fill in data.
                if (vm.Auth.IsAuthenticated) {
                    vm.Comment = {};
                    vm.Comment.User = {};
                    vm.Comment.User.ID = vm.Auth.UserData.ID;
                    vm.Comment.Name = vm.Auth.UserData.Name;
                    vm.Comment.Mobile = vm.Auth.UserData.Mobile;
                    vm.Comment.Email = vm.Auth.UserData.Email;
                } else {
                    vm.Comment = {};
                }
            }

            vm.AddCommentFiles = function (file, type) {
                if (file) {
                    if (!vm.TempComment)
                        vm.TempComment = {};
                    if (!vm.TempComment.Images)
                        vm.TempComment.Images = [];
                    vm.TempComment.Images.push(file);
                }
            }

            vm.RemoveCommentFile = function (file, type) {
                index = vm.TempComment.Images.indexOf(file);
                vm.TempComment.Images.splice(index, 1);
            }

            vm.HaveTypes = function () {
                if (vm.Post.TypeList) {
                    for (index in vm.Post.TypeList) {
                        if (vm.Post.TypeList[index])
                            return true;
                    }
                }
                return false;
            }

            //check if category has the categoryname
            vm.IfCategoryHas = function (categoryName) {
                //  try{
                if (vm.Post && vm.Post.Category && vm.Post.Category.MaterializedPath.includes(categoryName))
                    return true;
                //} catch (e) {
                //    return false;
                //}
                return false;
            }

            vm.Goto = function (id) {
                vm.Util.FocusToElement(id);
            }

            SetupJquery = function () {
                $('input[type="checkbox"]').change(function () {
                    if ($(this).is(':checked')) {
                        $(this).parent("label").addClass("checked");
                    } else {
                        $(this).parent("label").removeClass("checked");
                    }
                });

                $(document).ready(function () {
                    $(".fancybox").fancybox({
                        openEffect: "none",
                        closeEffect: "none"
                    });
                });
            }

            vm.IsOwner = function () {
                if (vm.Post && auth.IsAuthenticated && (vm.Post.Owner.ID == auth.UserData.ID))
                    return true;
                return false;
            }

            vm.EditPost = function (post) {
                $location.path("Post/Edit/" + post.ID).search({});
            }

            CreateFilter = function () {
                var filter = "";

                filterarray = [];

                filterarray.push("Post/ID eq guid'" + $routeParams.id + "'");

                if (!vm.IsOwner()) {
                    filterarray.push("PublicVisible eq true");
                }

                filter = filterarray.join(" and ");

                return filter;
            }

            vm.SearchComments = function (newPageNumber, OldPageNumber, fromInit) {
                if (vm.Filtering) {
                    vm.Pagination.Current = 1;
                }

                if (!fromInit)
                    vm.Util.FocusToElement('comments');

                page = (newPageNumber ? newPageNumber : 0 - OldPageNumber ? OldPageNumber : 0) || 0;

                skip = (vm.Pagination.Current + (page ? 0 : page)) - 1;

                query = {
                    $top: vm.Pagination.PageSize,
                    $orderby: "Featured desc,CreatedOn desc",
                    $skip: skip ? skip * vm.Pagination.PageSize : 0,
                    $inlinecount: "allpages"
                };

                if (skip > 0)
                    vm.Util.FocusToElement("inquires");

                if (filter = CreateFilter())
                    query.$filter = filter;

                CommentService
                    .Search(query).$promise.then(
                        function (success) {
                            result = success.result;
                            vm.Util.UTCToLocalTime(result.Result);
                            if (!vm.Comments)
                                vm.Comments = {};
                            if (!vm.Comments.List)
                                vm.Comments.List = [];
                            vm.Comments.List = result.Result;
                            vm.Comments.Count = result.Count;
                            vm.Processing = false;
                        }, function (error) {
                            vm.Processing = false;
                        });
            }

            function init() {

                if ($routeParams.FromDashboard) {
                    vm.FromDashboard = true;
                }

                //get url parameter
                var postId = $routeParams.id;

                _FillCommentFields();

                fbq('track', "PageView");

                //pass post id and user id
                PostService.Get(postId).$promise.then(
                    function (success) {
                        vm.Post = success.result;
                        SetupMeta(vm.Post);
                        vm.Util.UTCToLocalTime(vm.Post);

                        if (!vm.IsOwner() && $location.$$path.includes("Edit")) {
                            $location.path("Dashboard");
                            return;
                        }


                        if ($location.$$path.includes("Edit")) {

                            vm.Keywords = [];
                            for (index in vm.Post.TagList) {
                                vm.Keywords.push({ Word: vm.Post.TagList[index] });
                            }

                            if (!vm.TempPost)
                                vm.TempPost = {};
                            if (!vm.TempPost.Images)
                                vm.TempPost.Images = [];
                            if (!vm.TempPost.Attachments)
                                vm.TempPost.Attachments = [];
                            vm.Post.Images = [];
                            vm.Post.Attachments = [];

                            //convert into file object
                            if (vm.Post.ImageUrlList && vm.Post.ImageUrlList.length > 0) {
                                for (index in vm.Post.ImageUrlList) {
                                    //imageUrl = window.location.origin + vm.Post.ImageUrlList[index];
                                    imageUrl = vm.Post.ImageUrlList[index];
                                    FileService.UrlToFile(imageUrl, function (fileObject) {
                                        $scope.$apply(function () {
                                            fileObject.Old = true;
                                            vm.TempPost.Images.push(fileObject);
                                            vm.Post.Images.push(FileService.FileToFileVM(fileObject));
                                        });
                                    });
                                }
                            }

                            //convert into file object
                            if (vm.Post.AttachmentUrlList && vm.Post.AttachmentUrlList.length > 0) {
                                for (index in vm.Post.AttachmentUrlList) {
                                    fileUrl = vm.Post.AttachmentUrlList[index];
                                    FileService.UrlToFile(fileUrl, function (fileObject) {
                                        $scope.$apply(function () {
                                            fileObject.Old = true;
                                            vm.TempPost.Attachments.push(fileObject);
                                            vm.Post.Attachments.push(FileService.FileToFileVM(fileObject));
                                        });
                                    });
                                }
                            }

                            vm.Post.CategoryNames = vm.CategoryNamesArrayFromPath(vm.Post.Category.MaterializedPath);

                            DropdownService.Set(vm).then(function (success) {
                                if (vm.Post.Country) {
                                    vm.Selected.Country = vm.Util.GetObjectByProperty("Name", vm.Post.Country, success[0]);
                                }

                                if (vm.Post.City && vm.Selected.Country)
                                    vm.Selected.City = vm.Util.GetObjectByProperty("Name", vm.Post.City, vm.Selected.Country.Subs);

                            }, function (error) {
                                PopupService.error("Unable to retrieve countries and categories");
                            });

                            setTimeout(function () { SetupJquery(); }, 300);

                        }

                        //get primary image
                        vm.PrimaryImage = vm.Post.PrimaryImage;

                        vm.SearchComments(null, null, true);
                    },
                    function (error) {
                        if (error.status == 400) {
                            $location.path("Dashboard");
                            return;
                        }
                    });

                SetupJquery();

                focus = $routeParams.focus;

                if (focus)
                    setTimeout(function () { vm.Util.FocusToElement(focus); }, 500);
                else
                    vm.Util.FocusToElement('body');
            }

            SetupMeta = function (post) {
                obj = {};
                obj.ogurl = $location.$$absUrl;
                obj.ogtitle = vm.TextModifierService.RequestText(post) + " '" + post.Title + "' (" + post.DisplayLocation + ")";
                obj.ogdescription = vm.TextModifierService.ButtonText(post.Category) + " - Click this link for more details";
                obj.ogimage1 = UtilityService.GetPostPrimaryImageLink(post, true);
                MetaService.SetFBMeta(obj);
            }

            vm.ShareUsingFacebook = function () {
                window.fbAsyncInit();
                FB.ui({
                    method: 'share',
                    mobile_iframe: true,
                    href: window.location.href,
                }, function (response) { });

            }

            window.fbAsyncInit = function () {
                FB.init({
                    appId: '172467843173698',
                    cookie: true,
                    xfbml: true,
                    version: 'v2.7'
                });
            };
            vm.ReactivatePost = function (post) {
                if (confirm("Are you sure you want to reactivate?")) {
                    PostService.Reactivate(post.ID).$promise.then(function (success) {
                        if (success.result) {
                            vm.Post.Status = "active";
                            PopupService.success("Successfully reactivated your advertisment.");
                        }
                    }, function (error) { });
                }
            }

            vm.SubmitUpdatePost = function (post) {

                //post.Images = null;

                post.TagList = [];
                for (index in vm.Keywords) {
                    post.TagList.push(vm.Keywords[index].Word);
                }

                post.Country = vm.Selected.Country ? vm.Selected.Country.Name : "";
                post.City = vm.Selected.City ? vm.Selected.City.Name : "";

                UpdatePost(post);
            }

            UpdatePost = function (post) {
                PostService.Update(post).$promise.then(function (success) {
                    PopupService.success("Successfully modified your advertisment.");
                    $location.path("Post/" + post.ID).search({});
                }, function (error) { });
            }

            vm.SubmitComment = function () {
                //dito binubuo 
                vm.Comment.Post = {};
                vm.Comment.Post.ID = vm.Post.ID;
                vm.Comment.Post.Title = vm.Post.Title;
                vm.Comment.Post.Owner = { ID: vm.Post.Owner.ID };
                vm.Comment.Post.Owner.Email = vm.Post.Owner.Email;
                vm.Comment.Post.Owner.Name = vm.Post.Owner.Name;


                //convert file.
                if (vm.TempComment && vm.TempComment.Images && vm.TempComment.Images.length > 0) {
                    if (!vm.Comment.Images)
                        vm.Comment.Images = [];

                    FileService.FilesToFileVMs(vm.TempComment.Images).then(function (results) {
                        vm.Comment.Images = results;
                        CreateComment(vm.Comment);
                    });
                } else {
                    CreateComment(vm.Comment);
                }
            }

            CreateComment = function (comment) {
                vm.Processing = true;
                CommentService.Create(comment).$promise.then(
                    function (success) {
                        vm.Processing = false;
                        comment.ID = success.result.ID;
                        vm.Comment.ImageUrlList = success.result.ImageUrlList;
                        vm.Comment.Featured = success.result.Featured;
                        vm.Comment.CreatedOn = new Date();
                        PopupService.success("Successfully commented to this post");
                        if (comment.User && comment.User.ID)
                            comment.User = vm.Auth.UserData;

                        if (comment.PublicVisible) {
                            vm.Comments.List.unshift(comment);
                            vm.Comments.Count++;
                        }

                        _FillCommentFields();
                        vm.Goto('comments');
                    },
                    function (error) {
                        vm.Processing = false;
                    });
            }

            init();

            vm.AddTag = function (keyword) {
                if (!vm.Keywords)
                    vm.Keywords = [];

                if (vm.keyword) {
                    //check if already existing
                    for (index in vm.Keywords) {
                        if (vm.Keywords[index].Word.toLowerCase() == keyword.toLowerCase()) {
                            PopupService.error("Keyword already used");
                            return;
                        }
                    }

                    vm.Keywords.push({ Word: keyword });
                    vm.keyword = "";
                }
            }

            vm.RemoveTag = function (keyword) {
                temp = vm.Keywords.indexOf(keyword);
                vm.Keywords.splice(temp, 1);
            }

            vm.SelectImage = function (image) {
                vm.PrimaryImage = image;
                for (i in vm.Post.ImageUrlList) {
                    if (vm.Post.ImageUrlList[i] == vm.PrimaryImage) {
                        vm.index = i;
                    }
                }
            }

            vm.PrevImage = function () {
                for (i in vm.Post.ImageUrlList) {
                    if (vm.Post.ImageUrlList[i] == vm.PrimaryImage) {
                        if (i - 1 == -1)
                            vm.PrimaryImage = vm.Post.ImageUrlList[vm.index = vm.Post.ImageUrlList.length - 1];
                        else
                            vm.PrimaryImage = vm.Post.ImageUrlList[vm.index = --i];
                        return;
                    }
                }
            }

            vm.NextImage = function () {
                for (i in vm.Post.ImageUrlList) {
                    if (vm.Post.ImageUrlList[i] == vm.PrimaryImage) {
                        if (i == vm.Post.ImageUrlList.length - 1)
                            vm.PrimaryImage = vm.Post.ImageUrlList[vm.index = 0];
                        else
                            vm.PrimaryImage = vm.Post.ImageUrlList[vm.index = ++i];
                        return;
                    }
                }
            }

            vm.GetImage = function (data) {
                v = 'data:' + data.Type + ';base64,' + data.Base64;
                //console.log(v);
                return v;
            }

            vm.dataURItoBlob = function (file) {
                var byteString = atob(file.Base64);
                var ab = new ArrayBuffer(byteString.length);
                var ia = new Uint8Array(ab);
                for (var i = 0; i < byteString.length; i++) {
                    ia[i] = byteString.charCodeAt(i);
                }
                return new Blob([ab], { type: file.Type });
            }

            vm.toBlob = function (file) {
                var canvas = document.createElement('canvas');
                canvas.width = 100;
                canvas.height = 100;
                var img_b64 = canvas.toDataURL(file.Type);
                var png = file.Base64;

                var the_file = new Blob([window.atob(png)],
                    { type: file.Type, encoding: 'utf-8' });

                var fr = new FileReader();

                fr.onload = function (oFREvent) {
                    var v = oFREvent.target.result.split(',')[1]; // encoding is messed up here, so we fix it
                    v = atob(v);
                    var good_b64 = btoa(decodeURIComponent(escape(v)));
                    //= document.getElementById("uploadPreview").src 
                    temp = document.createElement('temp');
                    file = temp.src = "data:" + file.Type + ";base64," + good_b64;

                    if (!vm.Images)
                        vm.Images = [];

                    vm.Images.push(file);
                };

                fr.readAsDataURL(the_file);
            }

            vm.CategoryNamesArrayFromPath = function (materializedPath) {
                array = [];
                var names = materializedPath.split(".");
                for (index in names) {
                    array.push(names[index]);
                }
                return array;
            }

            vm.TypeListContains = function (type) {
                return vm.Post.TypeList.includes(type);
            }

            vm.ModifyType = function (type) {
                if (vm.Post.TypeList.includes(type)) {
                    remove(vm.Post.TypeList, type);
                } else {
                    vm.Post.TypeList.push(type);
                }
            }

            function remove(arr, what) {
                var found = arr.indexOf(what);

                while (found !== -1) {
                    arr.splice(found, 1);
                    found = arr.indexOf(what);
                }
            }

        }]);

