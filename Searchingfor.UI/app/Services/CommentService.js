﻿app.factory('CommentService',
    ['$resource',
        function ($resource) {

            return {
                Create: function (comment) {
                    return $resource('/api/Comment/Create').save(comment);
                },
                Search: function (query) {
                    return $resource('/api/Comment/Search').get(query);
                }
            };
        }]);
