﻿app.factory('PopupService', [
    function () {

    return {
        success: function (text) {
            toastr.success(text, "Success");
        },
        error: function (text) {
            toastr.error(text, "Error");
        },
        warning: function (text) {
            toastr.warning(text, "Warning");
        },
        info: function (text) {
            toastr.info(text, "Info");
        }
    };
}]);
