﻿app.factory('AuthService',
    ['$q',
        '$location',
        'UtilityService',
        'PopupService',
        'SharedDataService',
        function ($q,
            $location,
            UtilityService,
            PopupService,
            SharedDataService) {

            vm = this;
            vm.IsAuthenticated = false;
            vm.UserData = null;
            vm.Token = null;
            //window.location.origin + 
            vm.CookieName = "AuthData";

            //refreshes information each new route
            vm.FillAuthData = function () {
                authData = JSON.parse(SharedDataService.get(this.CookieName) ? SharedDataService.get(this.CookieName) : null);

                while (typeof authData == 'string')
                    authData = JSON.parse(authData);
                if (authData) {
                    this.IsAuthenticated = true;
                    //request userData

                    while (typeof authData.Value == 'string')
                        this.UserData = authData.Value = JSON.parse(authData.Value);

                    if (typeof authData.Value == 'object')
                        this.UserData = authData.Value;

                    if (authData.access_token) {
                        this.Token = authData;
                        this.Token.Value = "";
                    }
                } else {
                    this.IsAuthenticated = false;
                    this.UserData = this.Token = null;
                }

                return {
                    IsAuthenticated: this.IsAuthenticated,
                    UserData: this.UserData,
                    me: this
                };
            };

            vm.StoreAuth = function (data, redirect) {
                if (typeof (data) == 'string')
                    SharedDataService.set(this.CookieName, data);
                else
                    SharedDataService.set(this.CookieName, JSON.stringify(data));

                //initialize main controller to refresh header
                mc.init();

                if (redirect)
                    $location.path("/Dashboard");
            };

            vm.StoreUserData = function (user) {
                get = SharedDataService.get(this.CookieName);
                if (get) {
                    while (typeof get == 'string')
                        get = JSON.parse(get);

                    get.Value = user;
                    SharedDataService.set(this.CookieName, JSON.stringify(get));

                    //initialize main controller
                    mc.init();
                }
            };

            vm.Logout = function () {
                mc.LoggedOutManually = true;
                mc.notifyCount = 0;
                SharedDataService.set(this.CookieName, "");

                tmpUserId = auth.UserData.ID;

                auth = null;
                //initialize main controller to refresh header
                mc.init();

                $location.path("/Dashboard");

                notificationHub.server.logOut(tmpUserId);

                PopupService.success("Logout sucessfully!");
            };

            vm.request = function (config) {
                //window.location.origin +
                get = SharedDataService.get("AuthData");

                config.headers = config.headers || {};

                var authData = JSON.parse(get || null) || null;

                if (authData) {
                    config.headers.Authorization = "Bearer " + authData.access_token;
                }

                activateOverlay();

                return config;
            };

            //enable overlay.
            activateOverlay = function () {
                document.getElementById("overlay").style.display = "block";
            };

            //disable overlay.
            deactivateOverlay = function () {
                document.getElementById("overlay").style.display = "none";
            };

            vm.response = function (config) {
                deactivateOverlay();
                return config;
            };

            vm.responseError = function (rejection) {
                deactivateOverlay();
                //bad request and has message
                if (rejection.status == 400 && rejection.data.Message) {
                    PopupService.error(rejection.data.Message);
                }
                //not found
                if (rejection.status == 404) {
                    $location.path("404");
                }
                //servererror
                if (rejection.status === 500) {
                    //$location.path("500");
                    console.log(rejection.config.url);
                    if (rejection.data.ExceptionMessage)
                        console.log(rejection.data.ExceptionMessage.substring(0, 600) + " ....");
                }
                //un-authorized
                if (rejection.status === 401) {
                    $location.path("Dashboard");
                    SharedDataService.set("AuthData", "");
                    //refresh main controller
                    if (mc)
                        mc.init();
                }

                return $q.reject(rejection);
            };


            return vm;
        }]);
