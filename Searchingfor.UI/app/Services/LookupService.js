﻿app.factory('LookupService',
    ['$resource',
        function ($resource) {

            return {
                Get: function (groupName) {
                    return $resource('/api/Lookup/Get?groupName=' + groupName).query();
                },
                GetActive: function (groupName) {
                    return $resource('/api/Lookup/GetAllActive?groupName=' + groupName).query();
                },
                GetCountries: function () {
                    return $resource('/api/Lookup/GetCountries').query();
                },
                GetCategories: function () {
                    return $resource('/api/Lookup/GetCategories').query();
                },
                GetCategoriesMetaData: function (identifier) {
                    return $resource('/api/Lookup/GetCategoriesMetaData').query();
                },
                GetSiblingCategoryMetaData: function (type, groupName) {
                    return $resource('/api/Lookup/GetSiblingCategoryMetaData?type=' + type + "&groupName=" + groupName).query();
                },
                Save: function (category) {
                    return $resource('/api/Lookup/Save').save(category);
                }
            };
        }]);
