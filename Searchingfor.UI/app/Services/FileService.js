﻿app.factory('FileService',
    ['$resource', 'Upload', '$q',
        function ($resource, Upload, $q) {

            url = "";
            var extensionRegex = /(?:\.([^.]+))?$/;

            getFileBlob = function (url, cb) {
                var xhr = new XMLHttpRequest();
                xhr.open("GET", url);

                xhr.responseType = "blob";
                xhr.addEventListener('load', function () {
                    cb(xhr.response, url);
                });
                xhr.send();
            };

            blobToFile = function (blob, name) {
                blob.lastModifiedDate = new Date();
                blob.name = name;
                blob.ext = extensionRegex.exec(name)[1];
                return blob;
            };

            getFileObject = function (filePathOrUrl, cb) {
                this.getFileBlob(filePathOrUrl, function (blob, url) {
                    urlSplit = url.split('/');
                    nameSplit = urlSplit[urlSplit.length - 1].split('.');
                    name = nameSplit.join(".");
                    cb(blobToFile(blob, name));
                });
            };

            return {
                //creates a file object from a file
                UrlToFile: function (url, callback) {
                    url = url;
                    getFileObject(url, callback);
                },
                FilesToFileVMs: function (files) {
                    var deferred = $q.defer();

                    if (files) {

                        fileVMs = [];

                        if (files.constructor == Array) {
                            for (index in files) {
                                file = files[index];
                                fileVM = this.FileToFileVM(file);
                                fileVMs.push(fileVM);
                            }
                        }
                        else {
                            file = files;
                            fileVM = this.FileToFileVM(file);
                            fileVMs.push(fileVM);
                        }

                        Upload.base64DataUrl(files).then(
                        function (urls) {
                            if (typeof (urls) == 'string') {
                                fileVMs[0].Type = urls.split(",")[0];
                                fileVMs[0].Base64 = urls.split(",")[1];
                            }
                            else {
                                for (index in urls) {
                                    fileVMs[index].Type = urls[index].split(",")[0];
                                    fileVMs[index].Base64 = urls[index].split(",")[1];
                                }
                            }
                            deferred.resolve(fileVMs);
                        });

                        return deferred.promise;
                    }
                },
                //converts a file to a fileobject to be consumed serverside
                FileToFileVM: function (file) {

                    return fileVM = {
                        LastModified: file.lastModified,
                        LastModifiedDate: file.lastModifiedDate,
                        Name: file.name,
                        Type: file.type,
                        Ext: extensionRegex.exec(file.name)[1],
                        Old: file.Old,
                        Raw: '',
                        Type: '',
                        Base64: ''
                    };

                }

            };
        }]);
