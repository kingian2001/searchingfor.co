﻿app.factory('MetaService',
    ['$resource',
        '$rootScope',
        function ($resource,
            $rootScope) {

            return {
                SetMeta: function (meta) {

                },
                SetFBMeta: function (obj) {
                    if(obj.ogurl)
                        $rootScope.ogurl = obj.ogurl;

                    if(obj.ogtitle)
                        $rootScope.ogtitle = obj.ogtitle;

                    if(obj.ogdescription)
                        $rootScope.ogdescription = obj.ogdescription;

                    if(obj.ogimage1)
                        $rootScope.ogimage1 = obj.ogimage1;

                    if(obj.ogimage2)
                        $rootScope.ogimage2 = obj.ogimage2;
                },

                SetFBMetaDefault: function () {
                    $rootScope.ogurl = "http://www.searchingfor.co";
                    $rootScope.ogtype = "article";
                    $rootScope.ogtitle = "Searchingfor.co - 'Hanap mo? Post mo!";
                    $rootScope.ogdescription = "Where you just post your demand, and sellers/providers provide you great offers at the best price!";
                    $rootScope.ogimage1 = "/assets/img/logo-green300x300.jpg";
                    $rootScope.ogimage2 = "/assets/img/2DescriptionImage2048x1088.jpg";
                }
            };
        }]);
