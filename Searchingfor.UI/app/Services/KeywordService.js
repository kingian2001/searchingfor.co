﻿app.factory('KeywordService',
    ['$resource',
        function ($resource) {
            return {
                GetKeywords: function (userId) {
                    return $resource('/api/Keyword/GetKeywords?userID=' + userId).get();
                },
                AssignKeywords: function (keywords, userId) {
                    return $resource('/api/Keyword/AssignKeywords?userID=' + userId).save(keywords);
                },
                KeywordsToSlots: function (keywords, userId) {
                    return $resource('/api/Keyword/KeywordsToSlots?userID=' + userId).save(keywords);
                }
            };
        }]);
