﻿app.factory('LogService',
    ['$resource',
        function ($resource) {
            return {
                AuditLogSearch: function (query) {
                    return $resource('/api/AuditLog/Search').get(query);
                },
                AuditLogStatistics: function (id) {
                    return $resource('/api/AuditLog/Statistics?userId=' + id).get();
                },
                SystemLogSearch: function (query) {
                    return $resource('/api/SystemLog/Search').get(query);
                },
            };
        }]);
