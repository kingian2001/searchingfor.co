﻿app.factory('TextModifierService',
    [function () {

        return {
            //modifies button text in advertise list
            ButtonText: function (category, selling) {
                try {
                    if (!selling) {
                        if (category.MaterializedPath.includes("Job")) {
                            return "Hire Me";
                        } else if (category.MaterializedPath.includes("Buyable/Swappable/Rentable") || category.MaterializedPath.includes("Service")) {
                            return "Make an offer";
                        } else {
                            return "Reach out";
                        }
                    } else {
                        return "Reach out";
                    }
                } catch (e) { }
            },
            //modifies request text in advertise list
            RequestText: function (post) {
                try {
                    if (post.Selling) {
                        //if (post.Category.MaterializedPath.includes("Buyable/Swappable/Rentable")) {
                        //    if (post.Types) {
                        //        return "Want to " + post.Types;
                        //    } else {
                        return "I have";
                        //}
                    } else {
                        if (post.Category.MaterializedPath.includes("Job")) {
                            return "Position Desired";
                        } else if (post.Category.MaterializedPath.includes("Buyable/Swappable/Rentable")) {
                            if (post.Types) {
                                return "Want to " + post.Types;
                            } else {
                                return "I want to Buy";
                            }
                        } else {
                            return "Searching for";
                        }
                    }
                } catch (e) { }
            },
            //modifies budget text in advertise list
            BudgetText: function (category, selling) {
                try {
                    if (!selling) {
                        if (category.MaterializedPath.includes("Job")) {
                            return "Salary Range";
                        } else if (category.MaterializedPath.includes("Buyable/Swappable/Rentable")) {
                            return "Willing to Pay";
                        } else {
                            return "Budget";
                        }
                    } else {
                        return "Price";
                    }
                } catch (e) { }
            },

            //In PostingDetails Page.
            TitleLabel: function (category, selling) {
                try {
                    if (!selling) {
                        if (category.MaterializedPath.includes("Job")) {
                            return "What kind of job your searching for?";
                        } else {
                            return "What are you searching for?";
                        }
                    } else {
                        return "What do you have?";
                    }
                } catch (e) { }
            },
            AmountLabel: function (category, selling) {
                try {
                    if (!selling) {
                        if (category.MaterializedPath.includes("Job")) {
                            return "Desired Salary Range";
                        } else {
                            return "Budget";
                        }
                    } else {
                        return "Price";
                    }
                } catch (e) { }
            },
            TitleToolTip: function (category, selling) {
                try {
                    if (!selling) {
                        if (category.MaterializedPath.includes("Job")) {
                            return "ex: c# Software engineering position";
                        } else if (category.MaterializedPath.includes("Service")) {
                            return "ex: Plumbing near malabon area ASAP";
                        } else if (category.MaterializedPath.includes("Buy")) {
                            return "ex: Samsung Galaxy S3";
                        } else {
                            return "Brief description about what your searching for";
                        }
                    } else {
                        return "Brief description about the product/service";
                    }
                } catch (e) { }
            },
            TagLabel: function (category) {
                try {
                    if (category.MaterializedPath.includes("Job")) {
                        return "Areas of Expertise";
                    } else {
                        return "Tags describing the post";
                    }
                } catch (e) { }
            },
            DescriptionLabel: function (category) {
                try {
                    if (category.MaterializedPath.includes("Job")) {
                        return "Describe yourself";
                    } else {
                        return "Description";
                    }
                } catch (e) { }
            }
        }
    }]);
