﻿app.factory('UserService',
    ['$resource',
        function ($resource) {

            return {
                Login: function (data) {
                    return $resource('/api/User/Login').save(data);
                },
                Token: function (data) {
                    return $resource('/api/Token').save(data);
                },
                //unused
                AdminLogin: function (data) {
                    return $resource('/api/User/AdminLogin').save(data);
                },

                Create: function (data) {
                    return $resource('/api/User/Create').save(data);
                },
                LoginFB: function (data) {
                    return $resource('/api/User/LoginFB').save(data);
                },
                LoginGoogle: function (data) {
                    return $resource('/api/User/LoginGoogle').save(data);
                },
                Update: function (data) {
                    return $resource('/api/User/Update').save(data);
                },
                ChangePassword: function (data,EmailToLower) {
                    return $resource('/api/User/ChangePassword?EmailToLower=' + EmailToLower).save(data);
                },
                UploadProfilePic: function (user) {
                    return $resource('/api/User/UploadProfilePic').save(user);
                },
                GetProfileDetails: function (data) {
                    return $resource('/api/User/GetProfileDetails').save(data);
                },
                GetProfileDetailsViaEmail: function (email) {
                    return $resource('/api/User/GetProfileDetailsViaEmail?email=' + email).get();
                },
                Search: function (query) {
                    return $resource('/api/User/Search').get(query);
                },
                RetrieveCreditBalance: function (userId) {
                    return $resource('/api/User/RetrieveCreditBalance?userId=' + userId).get();
                },
                IsEmailUnique: function (email) {
                    return $resource('/api/User/IsEmailUnique?email=' + email).get();
                },
                IfEmailIsUsedOtherThanThisUser: function (email, userId) {
                    return $resource('/api/User/IfEmailIsUsedOtherThanThisUser?email=' + email + "&userId=" + userId).get();
                },
                Subscribe: function (email) {
                    return $resource('/api/User/Subscribe?email=' + email).get();
                },
                VerifyEmail: function (code) {
                    return $resource('/api/User/VerifyEmail').save(code);
                },
                SendEmailVerification: function (userId,email,name) {
                    return $resource('/api/User/SendEmailVerification?userId=' + userId + '&email=' + email + '&name=' + name).get();
                },
                SendEmailDefaultPassword: function (email) {
                    return $resource('/api/User/SendEmailDefaultPassword?email=' + email).get();
                },
            };
        }]);
