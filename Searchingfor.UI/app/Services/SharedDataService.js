﻿
app.factory('SharedDataService',
    ['$cookies',
        function ($cookies) {
            return {
                //get and set cookies
                get: function (key) {

                    return $cookies.get(key);
                },

                set: function (key, value) {

                    $cookies.put(key, value);
                },
                //remove cookie by name
                remove: function (key) {

                    $cookies.remove(key);
                }
            };
        }]);
