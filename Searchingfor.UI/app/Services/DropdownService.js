﻿app.factory('DropdownService',
    ['SharedDataService', 'localStorageService', '$q', 'LookupService',
    function (SharedDataService, localStorageService, $q, LookupService) {

        return {
            Set: function (vm) {

                var defer1 = $q.defer();
                var defer2 = $q.defer();

                //if (!localStorageService.get("Countries"))
                    LookupService.GetCountries().$promise.then(
                        function (success) {
                            vm.Countries = countries = success;
                            localStorageService.set("Countries", JSON.stringify(success));
                            defer1.resolve(countries);
                        },
                        function (error) {
                            defer1.reject();
                        });
                //else {
                //    vm.Countries = countries = JSON.parse(localStorageService.get("Countries"));
                //    defer1.resolve(countries);
                //}
                //get categories
                //if (!localStorageService.get("Categories"))
                    LookupService.GetCategories().$promise.then(
                        function (success) {
                            vm.Categories = categories = success;
                            localStorageService.set("Categories", JSON.stringify(success));
                            defer2.resolve(categories);
                        },
                        function (error) {
                            defer2.reject();
                        });
                //else {
                //    vm.Categories = categories = JSON.parse(localStorageService.get("Categories"));
                //    defer2.resolve(categories);
                //}

                return $q.all([defer1.promise, defer2.promise]);
            },
        };
    }]);
