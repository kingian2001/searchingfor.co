﻿//app.directive('postList', function() {
//  return {
//      restrict: 'AE',
//      replace: 'false',
//      //require: '^ngModel',
//      scope: {
//          posts: '=',
//      },
//      //template:"<h1>asd</h1>",
//      templateUrl: "app/Views/Shared/test.html",
//      link: function (scope, elem, attrs) {
//      //elem.bind('click', function() {
//      //  elem.css('background-color', 'white');
//      //  scope.$apply(function() {
//      //    scope.color = "white";
//      //  });
//      //});
//      //elem.bind('mouseover', function() {
//      //  elem.css('cursor', 'pointer');
//      //});
//    }
//  };
//});

app.directive('postList', function (UtilityService, TextModifierService) {
    return {
        restrict: 'EA',
        replace: true,
        scope: {
            posts: '=',
            paginationSetting: '=',
            func: '&',
            paginationId:'=',
            //Pagination: '='
        },
        templateUrl: "app/Views/Shared/PostList.html",
        link: function (scope, element, attrs) {
            scope.UtilityService = UtilityService;
            scope.TextModifierService = TextModifierService;
            scope.IsOwner = function (post) {
                if (post && auth.IsAuthenticated && (post.Owner.ID == auth.UserData.ID))
                    return true;
                return false;
            }
        }
    };
});


app.directive('dashboardPostList', function (UtilityService, TextModifierService) {
    return {
        restrict: 'EA',
        replace: true,
        scope: {
            posts: '=',
            options: '='
            //Pagination: '='
        },
        templateUrl: "app/Views/Shared/DashboardPostList.html",
        link: function (scope, element, attrs) {
            scope.UtilityService = UtilityService;
            scope.TextModifierService = TextModifierService;
            scope.IsOwner = function (post) {
                if (post && auth.IsAuthenticated && (post.Owner.ID == auth.UserData.ID))
                    return true;
                return false;
            }
        }
    };
});