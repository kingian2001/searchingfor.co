﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.OData.Query;
using Utilities.Container;

namespace Searchingfor.UI.Utilities
{
    public static class ODataQueryOptionsExtensions
    {
        /// <summary>
        /// Gets the filter expression.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="filter">The filter.</param>
        /// <returns>Expression&lt;Func&lt;T, System.Boolean&gt;&gt;.</returns>
        public static Expression<Func<T, bool>> GetFilterExpression<T>(this FilterQueryOption filter) where T : class
        {
            var enumerable = Enumerable.Empty<T>().AsQueryable();
            var param = Expression.Parameter(typeof(T));
            if (filter != null)
            {
                enumerable = (IQueryable<T>)filter.ApplyTo(enumerable, new ODataQuerySettings()
                {
                    HandleNullPropagation = HandleNullPropagationOption.False,
                    EnsureStableOrdering = false,
                });

                var mce = enumerable.Expression as MethodCallExpression;
                if (mce != null)
                {
                    var quote = mce.Arguments[1] as UnaryExpression;
                    if (quote != null)
                    {
                        return quote.Operand as Expression<Func<T, bool>>;
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// Gets the order by expression.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="orderBy">The order by.</param>
        /// <returns>SortExpression&lt;T&gt;[].</returns>
        public static List<SortExpression<T>> 
            GetOrderByExpression<T>(this OrderByQueryOption orderBy)
            where T : class
        {
            if (orderBy == null)
                return null;

            var param = Expression.Parameter(typeof(T));
            var sortExpressions = new List<SortExpression<T>>();

            List<Expression<Func<T, object>>> expressions = new List<Expression<Func<T, object>>>();

            var parameters = orderBy.RawValue.Split(',');

            Expression field = null;
            var nodeCtr = 0;
            foreach (var p in parameters)
            {
                var raw = p.Replace(" asc", "").Replace(" desc", "");
                var props = raw.Split('/');

                if (props.Length < 2)
                {
                    field = Expression.Property(param, raw);
                }
                else
                {
                    field = param;
                    foreach (var sub in props)
                    {
                        field = Expression.Property(field, sub);
                    }
                }

                var converted = Expression.Lambda<Func<T, object>>(Expression.Convert(field, typeof(object)), param);

                expressions.Add(converted);
                sortExpressions.Add(new SortExpression<T>(converted, true));
                nodeCtr++;
            }

            //return expressions.ToList();

            return sortExpressions;
        }
    }
}