﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Searchingfor.UI
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            //web api is now hosted by OWIN
            //WebApiConfig.Register(GlobalConfiguration.Configuration);

            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            RegisterFolders();

            ///starts log4net.
            log4net.Config.XmlConfigurator.Configure();
 }

        /// <summary>
        /// Registers the folders needed for operation.
        /// </summary>
        private void RegisterFolders()
        {
            if (!Directory.Exists(HttpContext.Current.Server.MapPath("/App_Data")))
                Directory.CreateDirectory(HttpContext.Current.Server.MapPath("/App_Data"));
            
            //Create by default the necessary folder.
            var rootFolder = HttpContext.Current.Server.MapPath(
                ConfigurationManager.AppSettings["UploadFilePath"].ToString());

            var imagesFolder = HttpContext.Current.Server.MapPath(
             ConfigurationManager.AppSettings["ImagesFilePath"].ToString());

            var attachmentFolder = HttpContext.Current.Server.MapPath(
             ConfigurationManager.AppSettings["AttachmentsFilePath"].ToString());

            //Create by default the necessary folder.
            var screenShotsFolder = HttpContext.Current.Server.MapPath(
                ConfigurationManager.AppSettings["CrawlerScreenshots"].ToString());

            GlobalVariables.EmailPath = HostingEnvironment.ApplicationPhysicalPath + "\\App_Data\\Subscribers.txt";

            var exists = System.IO.File.Exists(GlobalVariables.EmailPath);

            if (!exists)
                System.IO.File.Create(GlobalVariables.EmailPath);

            if (!Directory.Exists(rootFolder))
                Directory.CreateDirectory(rootFolder);

            if (!Directory.Exists(imagesFolder))
                Directory.CreateDirectory(imagesFolder);

            if (!Directory.Exists(attachmentFolder))
                Directory.CreateDirectory(attachmentFolder);

            if (!Directory.Exists(screenShotsFolder))
                Directory.CreateDirectory(screenShotsFolder);
        }
    }

    //set global variables
    public class GlobalVariables
    {
        /// <summary>
        /// where subscriber emails are stored ';' separated
        /// </summary>
        public static string EmailPath { get; set; }

        /// <summary>
        /// get list of emails on email file
        /// </summary>
        public static List<string> Emails { get { return System.IO.File.ReadAllText(GlobalVariables.EmailPath).Split(';').ToList(); } }

    }
}